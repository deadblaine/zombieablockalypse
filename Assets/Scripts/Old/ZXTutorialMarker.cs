﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXTutorialMarker : MonoBehaviour, IComparer<ZXTutorialMarker>,  IComparable<ZXTutorialMarker>
{
    [SerializeField] private int index;

    //Types
    public enum Series
    {
        alpha = 0,
        bravo = 1,
        capa = 2,
        delta = 3,
        echo = 4,
        fox = 5,
        gama = 6
    }

    //Inspector variables
    [SerializeField] private Series series;

    //Private statics
    private static List<ZXTutorialMarker> markers;

    //Public Acessors
    public static bool Initalized
    {
        get
        {
            return markers != null;
        }
    }

    private void Awake()
    {
        if (markers == null)
            markers = new List<ZXTutorialMarker>(20);

        markers.Add(this);
    }

    public static List<ZXTutorialMarker> getGroup(Series group)
    {
        List < ZXTutorialMarker > temp = markers.FindAll(delegate (ZXTutorialMarker obj) { return obj.series == group; });

        temp.Sort();

        return temp;
    }

    public int Compare(ZXTutorialMarker A, ZXTutorialMarker B)
    {
        return A.index - B.index;
    }

    public int CompareTo(ZXTutorialMarker other)
    {
        if (other == null)
            return 1;
        else
            return index - other.index;
    }

    public void OnDestroy()
    {
        markers = null;
    }
}
