﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ZXCore;
using DG.Tweening;
using TMPro;

public class ZXHealthBar : MonoBehaviour 
{
	private IZXDamageable damageable;
    private IZXInstructable instructable;
    private ZXToon toon;
	private Image healthFill;
	private Image healthBackground;

	private Transform location;
	private Transform transform;
	private Transform healthBarTrans;

    private TextMeshProUGUI levelText;
    private RectTransform levelRect;
    private RectTransform baseRect;

	public Vector3 PosOffset;

	private GameObject go;
	private Color initColor;

    void Start()
    {
        transform = GetComponent<Transform>();
        toon = GetComponent<ZXToon>();

        damageable = transform.GetComponent<IZXDamageable>();
        instructable = transform.GetComponent<IZXInstructable>();
        damageable.e_HealthChanged += new System.Action<float, float>(updateBar);

        if (toon)
        {
            toon.e_LevelChanged += new System.Action<int>(SetLevel);
            toon.e_Dead += new System.Action<IZXTargetable>(DisableBar);
        }
 
        ZXObjective obj = GetComponent<ZXObjective>();

		if(toon && ZXCombatUI.ActivePlayers.Count > 0 && GetComponent<IZXInstructable>() == ZXCombatUI.ActivePlayers[0])
		{
			toon.e_CanMove += new System.Action<bool>(canMove);
		}

        if (GetComponent<IZXDamageable>().Race != ToonRace.Human && !GetComponent<ZXSpawnManager>() && !GetComponent<ZXObjective>())
        {
            if (GetComponent<ZXZombie>() is ZXSpitter || GetComponent<ZXZombie>() is ZXTankZombie)
            {
                go = Instantiate(Resources.Load("HealthBarZombie")) as GameObject;
                go.name = "HealthBar" + gameObject.name;
                levelRect = go.transform.Find("LevelImage").GetComponent<RectTransform>();
                levelText = levelRect.Find("Level").GetComponent<TextMeshProUGUI>();
                levelRect.anchoredPosition = new Vector2(-0f, 0f);
            }
            else if (GetComponent<ZXZombie>() is ZXExploder)
            {
                go = Instantiate(Resources.Load("HealthBarExploderZombie")) as GameObject;
                go.name = "HealthBar" + gameObject.name;
                levelRect = go.transform.Find("LevelImage").GetComponent<RectTransform>();
                levelText = levelRect.Find("Level").GetComponent<TextMeshProUGUI>();
                levelRect.anchoredPosition = new Vector2(-0f, 0f);
                go.transform.DOPunchScale(new Vector3(0.5f, 0.5f, 0.5f), 1, 1).SetLoops(-1);
            }
            else if (GetComponent<IZXDamageable>().Race == ToonRace.Raider)
            {
                go = Instantiate(Resources.Load("HealthBarRaider")) as GameObject;
                go.name = "HealthBar" + gameObject.name;
                levelRect = go.transform.Find("LevelImage").GetComponent<RectTransform>();
                levelText = levelRect.Find("Level").GetComponent<TextMeshProUGUI>();
                levelRect.anchoredPosition = new Vector2(-0f, 0f);
            }
            else
            {
                go = Instantiate(Resources.Load("HealthBarZombie")) as GameObject;
                go.name = "HealthBar" + gameObject.name;
                levelRect = go.transform.Find("LevelImage").GetComponent<RectTransform>();
                levelText = levelRect.Find("Level").GetComponent<TextMeshProUGUI>();
                levelRect.anchoredPosition = new Vector2(-0f, 0.0f);
            }
        }
        else if (obj)
        {
            switch (obj.Type)
            {
                case ZXObjective.ObjectiveType.KOTH:
                    go = Instantiate(Resources.Load("HealthBarBuildingNormal")) as GameObject;
                    break;

                case ZXObjective.ObjectiveType.KOTH_Rescue:
                    go = Instantiate(Resources.Load("HealthBarBuildingRescue")) as GameObject;
                    break;

                case ZXObjective.ObjectiveType.KOTH_Vehicle:
                    go = Instantiate(Resources.Load("HealthBarBuildingVehicle")) as GameObject;
                    break;

                default:
                    go = null;
                    break;
            }
        }
        else if (ZXCombatUI.ActivePlayers.Contains(GetComponent<IZXInstructable>()))
        {
            go = Instantiate(Resources.Load("HealthBar")) as GameObject;
            levelRect = go.transform.Find("LevelImage").GetComponent<RectTransform>();
            levelText = levelRect.Find("Level").GetComponent<TextMeshProUGUI>();
            levelRect.anchoredPosition = new Vector2(-0f, 0f);
            baseRect = go.transform.Find("ImageRef").GetComponent<RectTransform>();

            levelRect.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f, 1, 1).SetUpdate(true).SetLoops(-1);
            baseRect.DOPunchScale(new Vector3(0.1f, 0.1f, 0.1f), 0.5f, 1, 1).SetUpdate(true).SetLoops(-1);
        }
        else if (GetComponent<IZXDamageable>().Race == ToonRace.Human && !ZXCombatUI.ActivePlayers.Contains(GetComponent<IZXInstructable>()))
        {
            go = Instantiate(Resources.Load("HealthBarCompanion")) as GameObject;
            levelRect = go.transform.Find("LevelImage").GetComponent<RectTransform>();
            levelText = levelRect.Find("Level").GetComponent<TextMeshProUGUI>();
            levelRect.anchoredPosition = new Vector2(-0f, 0f);
        }
        else
        {
            Debug.Log("HEALTH BAR NOT ON VALID OBJECT");
            return;
        }

        healthBarTrans = go.transform;

		if(healthBarTrans.Find("HPBarFill"))
		{
			healthFill	=	healthBarTrans.Find("HPBarFill").GetComponent<Image>();
			initColor = healthFill.color;
		}

		if(healthBarTrans.Find("HPBarBackground"))
		{
			healthBackground	=	healthBarTrans.Find("HPBarBackground").GetComponent<Image>();
		}

		healthBarTrans.SetParent(ZXCombatUI.ObjectMask);

		healthBarTrans.position = transform.position + new Vector3(0, 4, 0) + PosOffset;

        healthFill.gameObject.SetActive(false);
        healthBackground.gameObject.SetActive(false);

		if (toon)
			SetLevel(toon.ToonLevel);
    }

	void OnEnable()
	{
		if(go)
		{
			go.SetActive(true);

			healthFill.gameObject.SetActive(false);
			healthBackground.gameObject.SetActive(false);

			healthBackground.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
			healthFill.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;

			if(levelRect)
			{
				levelRect.gameObject.SetActive(true);
				levelRect.anchoredPosition = new Vector2(0f, 0f);
			}

			if(GetComponent<ZXZombie>() is ZXExploder)
			{
				go.transform.DOPunchScale(new Vector3(0.5f, 0.5f, 0.5f), 1, 1).SetLoops(-1);
			}
		}
    }

    void canMove(bool value)
	{
        /*
        string levelid = "canMoveLevelTweenID";
        string baseid = "canMoveBaseTweenID";

        if (value)
        {
            if (!DOTween.IsTweening(levelid))
            {
                levelRect.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f, 1, 1).SetId(levelid).SetUpdate(true).SetLoops(-1);
            }
            if (!DOTween.IsTweening(baseid))
            {
                baseRect.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), 0.5f, 1, 1).SetId(baseid).SetUpdate(true).SetLoops(-1);
            }
        }
        else
        {
            //Stub
        }
        */
	}

	void killTween()
	{
		if (DOTween.IsTweening(levelRect))
		{
			DOTween.Kill(levelRect);
		}
		levelRect.localScale = new Vector3(5.844f, 5.844f, 5.844f);
	}
		
	void LateUpdate()
	{
		if (healthBarTrans == null) return;
		healthBarTrans.transform.rotation = Quaternion.Euler(45, 225, 0);
		healthBarTrans.position = transform.position + new Vector3(0, -.25f, 0) + PosOffset;
	}

    public void SetLevel(int level)
    {
        if (levelRect && levelText)
        {
            levelRect.gameObject.SetActive(true);
            levelText.text = level.ToString();
        }
    }

	void updateBar(float healthPercentage, float damageAmount)
	{
        if(healthPercentage == 1)
        {
			if(levelRect)
            levelRect.anchoredPosition = new Vector2(0f, 0f);
        }

		if(healthFill)
		{
			healthBarTrans.gameObject.SetActive(true);
			healthBackground.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
			healthFill.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
			healthFill.gameObject.SetActive(true);
			healthBackground.gameObject.SetActive(true);

			healthFill.fillAmount = healthPercentage;

			if(!DOTween.IsTweening(healthFill))
			{
				healthFill.DOFillAmount(healthPercentage, 0.2f);
					healthFill.DOColor(Color.white, 0.05f).OnComplete(delegate() 
					{
						healthFill.DOColor(initColor, 0.05f);
					});
			}

			if(healthFill.fillAmount <= 0)
			{
				healthFill.gameObject.SetActive(false);
				healthBackground.gameObject.SetActive(false);
				healthBarTrans.gameObject.SetActive(false);
                if(levelRect)
                levelRect.gameObject.SetActive(false);

				DOTween.Kill(go.transform);
				go.transform.localScale = Vector3.one;
			}

			if(levelRect)
                levelRect.anchoredPosition = new Vector2(-1.53f, 0f);
		}
	}

    private void DisableBar(IZXTargetable Damageable)
    {
        if (go != null)
            go.SetActive(false);
    }
}
