﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXCore;

public class ZXTutorialSpawner : ZXSpawnLocation
{
    public enum SpawnType
    {
        Zombie = 0,
        Raider = 1,
    }

    //Types
    public enum Series
    {
        alpha = 0,
        bravo = 1,
        capa = 2,
        delta = 3,
        echo = 4,
        fox = 5,
        gama = 6
    }

    //Inspector variables
    [SerializeField] private SpawnType type;
    [SerializeField] private Series series;

    //Private statics
    private static List<ZXTutorialSpawner> spawners;

    //Public Acessors
    public static bool Initalized
    {
        get
        {
            return spawners != null;
        }
    }

    private void Awake()
    {
        if (spawners == null)
            spawners = new List<ZXTutorialSpawner>(20);

        spawners.Add(this);
    }

    public static void spawnGroup(Series groupToSpawn)
    {
        List<ZXTutorialSpawner> targetSpawners = spawners.FindAll(delegate (ZXTutorialSpawner obj) { return obj.series == groupToSpawn; });
        ZXToon temp = null;

        foreach (ZXTutorialSpawner spawner in targetSpawners)
        {
            if (spawner.type == SpawnType.Zombie)
                temp = ZXSpawnManager.PoolRequest<ZXToon>(ZXSpawnData.Spawn.Zombie);
            else if (spawner.type == SpawnType.Raider)
                temp = ZXSpawnManager.PoolRequest<ZXToon>(ZXSpawnData.Spawn.RaiderMelee);

            if (temp != null)
                spawner.Spawn(temp, Random.Range(0.5f, 2f));
        }
    }

    private void OnDestroy()
    {
        spawners = null;
    }
}
