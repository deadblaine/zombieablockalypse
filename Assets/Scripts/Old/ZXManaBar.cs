﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXManaBar : MonoBehaviour
{
    //Inspector variables
    [SerializeField] private int startingMana;
    [SerializeField] private int maxMana;
    [SerializeField] private float manaCollectionRate;

    //Private transforms
    [SerializeField] private RectTransform manaBar;
    [SerializeField] private RectTransform manaBarBackground;
    [SerializeField] private TextMeshProUGUI manaLabel;
    [SerializeField] private Image manaBarFill;

    //Public accessors
    public static float Mana
    {
        get
        {
            return currentMana;
        }
        set
        {
            currentMana = value;
        }
    }

    //Private tracking variables
    private static ZXManaBar singleton;
    private static float currentMana;

    private void Awake()
    {
        singleton = this;

        //Disable game object display
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    private void Update ()
    {
        if (ZXCombatUI.CurrentState == ZXCombatUI.State.Combat)
        {
            if (currentMana < maxMana)
            {
                //Count mana
                currentMana += Time.deltaTime * manaCollectionRate;

                //Update primary fill
                manaBarFill.fillAmount = Mathf.Clamp01(currentMana / maxMana);
                //Update label
                manaLabel.text = Mathf.FloorToInt(currentMana).ToString("0");
            }
        }
    }

    public static void Show()
    {
        if (singleton != null)
        {
            singleton.gameObject.SetActive(true);

            //Set starting mana
            currentMana = singleton.startingMana;
        }
    }

    public static void Hide()
    {
        if (singleton != null)
        {
            singleton.gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        singleton = null;
    }
}
