﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXTutorialSpellMarker : MonoBehaviour
{
    //Types
    public enum Series
    {
        alpha = 0,
        bravo = 1,
        capa = 2,
        delta = 3,
        echo = 4,
        fox = 5,
        gama = 6
    }

    //Inspector variables
    [SerializeField] private Series series;

    //Private statics
    private static List<ZXTutorialSpellMarker> markers;

    //Public Acessors
    public static bool Initalized
    {
        get
        {
            return markers != null;
        }
    }

    private void Awake()
    {
        if (markers == null)
            markers = new List<ZXTutorialSpellMarker>(20);

        markers.Add(this);
    }

    public static ZXTutorialSpellMarker getMarker(Series group)
    {
        return markers.Find(delegate (ZXTutorialSpellMarker obj) { return obj.series == group; });
    }

    public void OnDestroy()
    {
        markers = null;
    }
}
