﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class ZXColor : MonoBehaviour
{
    [SerializeField] private string key;

    private Image image;

    private Color color;

	// Use this for initialization
	private void Awake()
    {
        image = GetComponent<Image>();
	}

    private void OnEnable()
    {
        color = ZXDataManager.GetColor(key);

        if (image != null)
            image.color = color;
    }
}
