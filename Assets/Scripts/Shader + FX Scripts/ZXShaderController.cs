﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXShaderController : MonoBehaviour
{
    /* Constants */
    private const float _blinkTime = 0.25f;
    private const float _fadeTickSpeed = 0.05f;
    private const bool _shadersBlink = true;

    private const float _burnTime = 5f;
    private const float _burnTick = 0.1f;

    //Shader data
    private const string _transProp = "_Transparency";
    private const int _transPropIndex = 0;
    private const string _colorProp = "_Color";
    private const int _colorPropIndex = 1;
    private const string _emissionProp = "_EmissionColor";
    private const int _emissionPropIndex = 2;
    private static readonly string[] r_PropIDs = { _transProp, _colorProp, _emissionProp };
    private static readonly float[] r_PropReset = { 1f, 0f, 0f };
    private static readonly float[] r_PropResetTrans = { 0f, 0f, 0f };

    /* Inspector Fields */
    protected Color color = Color.white;
    [SerializeField] protected Color emissionColor = Color.white; 
    [SerializeField] bool children = true;

    /* Private Variables */
    protected Material[] r_MasterMaterials;
    protected bool[] r_MasterControls;
    protected bool[,] r_MasterProperties;

    protected SkinnedMeshRenderer[] r_SkinnedMeshRenderers;
    protected MeshRenderer[] r_MeshRenderers;

    //Shader tween IDS
    private int uid;

    private string r_BlinkEmissionID;
    private string r_ColorID;

    //Parent ZXShaderController
    private ZXShaderController parentController;

    //Callback containers
    private bool fading = false;
    private System.Action fadeCallback = null;

    /* Initalization */
    private void Awake()
    {
        //Get UID
        uid = gameObject.GetInstanceID();

        //Configure IDs
        r_BlinkEmissionID = uid + "_blinkID";
        r_ColorID = uid + "_colorID";

        //Get all renderers
        if (children)
        {
            r_SkinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
            r_MeshRenderers = GetComponentsInChildren<MeshRenderer>();
        }
        else
        {
            r_SkinnedMeshRenderers = GetComponents<SkinnedMeshRenderer>();
            r_MeshRenderers = GetComponents<MeshRenderer>();
        }

        BuildMasterMaterials();
    }

    private void Start()
    {
        Reset();

        if (transform.parent != null)
            parentController = transform.parent.GetComponentInParent<ZXShaderController>();
    }

    private void BuildMasterMaterials()
    {
        int total = 0;
        int index = 0;

        //Calculate the total number of renderers
        if (r_SkinnedMeshRenderers != null)
            total += r_SkinnedMeshRenderers.Length;

        if (r_MeshRenderers != null)
            total += r_MeshRenderers.Length;

        //Create arrays
        r_MasterMaterials = new Material[total];
        r_MasterControls = new bool[total];
        r_MasterProperties = new bool[total, r_PropIDs.Length];

        //Add skinned mesh renderers
        if (r_SkinnedMeshRenderers != null && r_SkinnedMeshRenderers.Length > 0)
            for (int i = 0; i < r_SkinnedMeshRenderers.Length; i++, index++)
            {
                r_MasterMaterials[index] = r_SkinnedMeshRenderers[i].material;
                r_MasterControls[index] = true;

                //Validate properties
                for (int x = 0; x < r_PropIDs.Length; x++)
                {
                    r_MasterProperties[index, x] = r_MasterMaterials[index].HasProperty(r_PropIDs[x]);
                }
            }

        //Add mesh renderers
        if (r_MeshRenderers != null && r_MeshRenderers.Length > 0)
            for (int i = 0; i < r_MeshRenderers.Length; i++, index++)
            {
                r_MasterMaterials[index] = r_MeshRenderers[i].material;
                r_MasterControls[index] = true;

                //Validate properties
                for (int x = 0; x < r_PropIDs.Length; x++)
                {
                    r_MasterProperties[index, x] = r_MasterMaterials[index].HasProperty(r_PropIDs[x]);
                }
            }
    }

    /* Shader Functions */
    private void Reset()
    {
        Reset(false);
    }    

    public void Reset(bool Transparent)
    {
        float[] resetVals = Transparent ? r_PropResetTrans : r_PropReset;

        //Reset all properties to default values
        for (int i = 0; i < r_MasterMaterials.Length; i++)
        {
            for (int x = 0; x < r_PropIDs.Length; x++)
            {
                if (r_MasterControls[i] && r_MasterProperties[i, x])
                    r_MasterMaterials[i].SetFloat(r_PropIDs[x], resetVals[x]);
            }
        }
    }

    public void BlinkEmission()
    {
        if (DOTween.IsTweening(r_BlinkEmissionID) || !_shadersBlink)
            return;

        //Fade skinned mesh renderers
        DOTween.To(
            delegate () /*Getter*/
            {
                return emissionColor.a;
            },

            delegate (float val) /*Setter*/
            {
                emissionColor.a = val;

                for (int i = 0; i < r_MasterMaterials.Length; i++)
                {
                    if (r_MasterControls[i] && r_MasterProperties[i, _emissionPropIndex])
                        r_MasterMaterials[i].SetColor(_emissionProp, emissionColor);
                }

            }, 1f, _blinkTime).SetId(r_BlinkEmissionID).SetLoops(2, LoopType.Yoyo);
    }

    public void EndBlinks()
    {
        //Fade skinned mesh renderers
        DOTween.To(
            delegate () /*Getter*/
            {
                return emissionColor.a;
            },

            delegate (float val) /*Setter*/
            {
                emissionColor.a = val;

                for (int i = 0; i < r_MasterMaterials.Length; i++)
                {
                    if (r_MasterControls[i] && r_MasterProperties[i, _emissionPropIndex])
                        r_MasterMaterials[i].SetColor(_emissionProp, emissionColor);
                }

            }, 0f, _blinkTime / 2).SetId(r_BlinkEmissionID);
    }

    public void Fade(float Delay, float Duration)
    {
        Fade(Delay, Duration, 0f, null);
    }

    public void Fade(bool FadeIn, float Delay, float Duration)
    {
        Fade(FadeIn, Delay, Duration, 0f, null);
    }

    public void Fade(float Delay, float Duration, float CallbackDelay, System.Action Callback)
    {
        Fade(false, Delay, Duration, CallbackDelay, Callback);
    }

    public void Fade(bool FadeIn, float Delay, float Duration, float CallbackDelay, System.Action Callback)
    {
        if (!fading)
        {
            fading = true;
            fadeCallback = Callback;

            if (!gameObject.activeSelf)
                gameObject.SetActive(true);

            StartCoroutine(FadeRoutine(FadeIn, Delay, Duration, CallbackDelay));
        }
    }

    /* Fade Routine */
    private IEnumerator FadeRoutine(bool FadeIn, float Delay, float Duration, float CallbackDelay)
    {
        float fadeTickAmount = (1 / Duration) * _fadeTickSpeed;
        float fadeCounter = FadeIn ? 0f : 1f;
        float fadeEndValue = FadeIn ? 1f : 0f;

        //Adjust to count up or down
        fadeTickAmount = FadeIn ? fadeTickAmount : fadeTickAmount * -1;

        //Set shaders to be transparent
        if (FadeIn)
            Reset(true);

        //Wait to begin fading
        yield return new WaitForSeconds(Delay);

        //While still visible fade shaders out
        while (fadeCounter != fadeEndValue)
        {
            fadeCounter = Mathf.Clamp01(fadeCounter + fadeTickAmount);

            for (int i = 0; i < r_MasterMaterials.Length; i++)
            {
                if (r_MasterControls[i] && r_MasterProperties[i, _transPropIndex])
                    r_MasterMaterials[i].SetFloat(_transProp, fadeCounter);
            }

            yield return new WaitForSeconds(_fadeTickSpeed);
        }

        //Wait for callback delay time
        if (CallbackDelay > 0f)
            yield return new WaitForSeconds(CallbackDelay);

        //Callback
        if (fadeCallback != null)
        {
            fadeCallback();
            fadeCallback = null;
        }

        fading = false;
    }

    /* Public Renderer Access */
    public void UseParentController(bool Control)
    {
        if (parentController != null)
        {
            for (int i = 0; i < r_MasterMaterials.Length; i++)
            {
                parentController.ControlRenderer(r_MasterMaterials[i], Control);
            }

            if (!Control)
                EndBlinks();
        }
    }

    public void ControlRenderer(Material Material, bool Control)
    {
        for (int i = 0; i < r_MasterMaterials.Length; i++)
        {
            if (r_MasterMaterials[i] == Material)
                r_MasterControls[i] = Control;
        }
    }

    /* Destruction */
    private void OnDisable()
    {
        Reset();
    }
}


