﻿using UnityEngine;
using System.Collections;
using ZXCore;

public class ZXNavigationNexus : ZXInterestPoint
{
    protected override void Awake()
    {
        base.Awake();

        pointType = Interest.Nexus;
        maxInteractions = 0;

        ZXReflectingPool.Register<ZXNavigationNexus>(this, transform);
    }

    public override int Interact(IZXTargetable Actor)
    {
        return 0;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ZXReflectingPool.DeRegister<ZXNavigationNexus>(transform);
    }
}
