﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXStarCounter : MonoBehaviour
{
    private const string pp_Star_Base_UID = "PP_Star_ID_";
    private const string pp_Star_Reward_Base_UID = "PP_Star_Reward_ID_";

    [SerializeField] private ZXMissionManager.Type missionType;
    [SerializeField] private ZXMissionManager.Difficulty missionDifficulty;
    [SerializeField] private int maxCount;

    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] List<ZXStarReward> rewards;

    private static Dictionary<string, ZXStarCounter> starTable;

    private int Stars
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Star_UID))
                return PlayerPrefs.GetInt(pp_Star_UID);
            else
                return 0;
        }
        set
        {
            if (value < maxCount)
            {
                PlayerPrefs.SetInt(pp_Star_UID, value);
            }
            else
            {
                PlayerPrefs.SetInt(pp_Star_UID, maxCount);

                GenerateReward();
            }

            UpdateDisplay();
        }
    }

    private int RewardsRecieved
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Star_Reward_UID))
                return PlayerPrefs.GetInt(pp_Star_Reward_UID);
            else
                return 0;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Star_Reward_UID, value);
        }
    }

    private float StarsProgress
    {
        get
        {
            return Mathf.Clamp01((float)Stars / (float)maxCount);
        }
    }

    private string pp_Star_UID;
    private string pp_Star_Reward_UID;
    private string dic_Star_UID;

    // Use this for initialization
    private void Awake()
    {
        pp_Star_UID = pp_Star_Base_UID + missionType.ToString() + "_" + missionDifficulty.ToString();
        pp_Star_Reward_UID = pp_Star_Reward_Base_UID + missionType.ToString() + "_" + missionDifficulty.ToString();
        dic_Star_UID = missionType.ToString() + "_" + missionDifficulty.ToString();

        if (starTable == null)
            starTable = new Dictionary<string, ZXStarCounter>(5);

        starTable.Add(dic_Star_UID, this);
    }

    private void OnEnable()
    {
        UpdateDisplay();
    }

    //Update functions
    public static void AddStars(ZXMissionManager.Type MissionType, ZXMissionManager.Difficulty MissionDifficulty, int Amount)
    {
        string key = MissionType.ToString() + "_" + MissionDifficulty.ToString();

        if (starTable != null && starTable.ContainsKey(key))
        {
            starTable[key].Stars += Amount;
        }
    }

    public static bool CanPlay(ZXMissionManager.Type MissionType, ZXMissionManager.Difficulty MissionDifficulty)
    {
        string key = MissionType.ToString() + "_" + MissionDifficulty.ToString();

        return starTable[key].StarsProgress < 1f;
    }

    public static void ResetAllStars()
    {
        List<ZXStarCounter> starCounters = new List<ZXStarCounter>(starTable.Values);

        foreach (ZXStarCounter counter in starCounters)
        {
            counter.Stars = 0;
            counter.RewardsRecieved = 0;
        }
    }

    public static void ResetStars(ZXMissionManager.Type MissionType)
    {
        if (starTable != null)
        {
            List<ZXStarCounter> starCounters = new List<ZXStarCounter>(starTable.Values);

            foreach (ZXStarCounter counter in starCounters)
            {
                if (counter.missionType == MissionType)
                    counter.Stars = 0;
            }
        }
    }

    private void ResetRewards()
    {
        RewardsRecieved = rewards.Count;
    }

    private void UpdateDisplay()
    {
       fill.fillAmount = StarsProgress;

       text.text = Stars.ToString() + " / " + maxCount.ToString();

        for (int i = 0; i < rewards.Count; i++)
        {
            float threshold = (float)(i + 1) / (float)rewards.Count;

            rewards[i].Completed(StarsProgress >= threshold);

            if (StarsProgress >= threshold)
            {
                if (RewardsRecieved <= i)
                {
                    RewardsRecieved++;
                    StartCoroutine(GiveReward());
                }
            }
        }
    }

    private IEnumerator GiveReward()
    {
        yield return new WaitUntil(delegate () { return ZXChestRewards.Loaded; });

        if (missionType == ZXMissionManager.Type.BasicKit || missionType == ZXMissionManager.Type.ClassKit)
        {
            ZXDataManager.Kits += (int)missionDifficulty + 1;
        }

        ZXChestRewards.GrantChestRewards();
    }

    private void GenerateReward()
    {

    }

    private void OnDestroy()
    {
        starTable.Remove(dic_Star_UID);
    }
}
