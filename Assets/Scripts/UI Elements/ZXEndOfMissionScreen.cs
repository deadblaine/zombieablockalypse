﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ZXEndOfMissionScreen : ZXCore.ZXPopup
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private List<GameObject> stars;

    [SerializeField] private GameObject zombieBounty;
    [SerializeField] private TextMeshProUGUI zombieBountyText;
    [SerializeField] private GameObject lockerReward;
    [SerializeField] private GameObject experienceReward;
    [SerializeField] private TextMeshProUGUI experienceRewardText;
    [SerializeField] private GameObject virilityReward;
    [SerializeField] private TextMeshProUGUI virilityRewardText;

    private static ZXEndOfMissionScreen singleton;

    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
    }

    public override void Show()
    {
        int mod = (int)ZXMissionManager.CurrentMissionDifficulty + 1;
        bool result = ZXCombatUI.CurrentState == ZXCombatUI.State.Victory ? true : false;
        int bounty = ZXMissionClock.ZombiesKilled * mod;
        int experience = 10 * (ZXMissionClock.StarCount + 1) * mod;
        int virility = result ? 5 * mod : 20 * mod;

        base.Show();

        text.text = result ? "Victory" /*ZXLocal.LocalText("Victory")*/ : "Defeat" /*ZXLocal.LocalText("Defeat")*/;
        
        //Enable stars
        for (int i = 0; i < stars.Count; i++)
        {
            if (i <= ZXMissionClock.StarCount - 1)
                stars[i].SetActive(true);
            else
                stars[i].SetActive(false);
        }

        //Report Results
        ZXMissionManager.CurrentMissionVictory = result;
        ZXMissionManager.CurrentMissionStars = ZXMissionClock.StarCount;

        //Silver Reward
        zombieBountyText.text = bounty.ToString();
        ZXDataManager.Silver += bounty;

        //Locker Reward
        lockerReward.SetActive(result && ZXMissionManager.CurrentMissionType == ZXMissionManager.Type.Battle);

        //Experience
        experienceRewardText.text = experience.ToString();
        ZXDataManager.Experience += experience;

        //Virility
        virilityRewardText.text = virility.ToString();
    }

    public override void Hide()
    {
        base.Hide();

        SceneManager.LoadScene((int)ZXDataManager.Scenes.Main); //Load the main scene
    }
}
