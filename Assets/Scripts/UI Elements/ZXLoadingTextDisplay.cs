﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZXLoadingTextDisplay : MonoBehaviour
{
    private static ZXLoadingTextDisplay singleton;
    private static TextMeshProUGUI display;

	// Use this for initialization
	private void Awake()
    {
        singleton = this;

        display = GetComponent<TextMeshProUGUI>();
	}

    public static void ShowText(string DisplayText)
    {
        if (singleton != null)
        {
            display.text = DisplayText;
        }
    }
}
