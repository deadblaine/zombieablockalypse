﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZXConnectionPopup : ZXCore.ZXPopup
{
    private static ZXConnectionPopup singleton;

    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
    }

    public static void ShowPopup()
    {
        singleton.Show();
    }

    public void Restart()
    {
        SceneManager.LoadScene((int)ZXDataManager.Scenes.Loading);
    }
}
