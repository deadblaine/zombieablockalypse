﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class ZXLoadingProgress : MonoBehaviour
{
    //Editor variables
    [SerializeField] private TextMeshProUGUI loadingTitleText;
    [SerializeField] private TextMeshProUGUI loadingDetailsText;
    [SerializeField] private TextMeshProUGUI loadingFlavorText;
    [SerializeField] private Image loadingBarFillImage;
    [SerializeField] private TextMeshProUGUI loadingPercentText;

    //Deleages
    private System.Action d_LangaugeLoaded;
    private System.Action<string, float> d_UpdateLoading;

    private static ZXLoadingProgress singleton;

    private float CurrentPercentage
    {
        get
        {
            return loadingBarFillImage.fillAmount;
        }
        set
        {
            loadingPercentText.text = string.Format("{0:0%}", Mathf.Clamp01(value));
            loadingBarFillImage.fillAmount = Mathf.Clamp01(value);
        }
    }

    //Initalization
    private void Awake()
    {
        singleton = this;

        d_LangaugeLoaded = new System.Action(LoadText);
        d_UpdateLoading = new System.Action<string, float>(UpdateLoadingProgress);

        CurrentPercentage = 0f;
    }

    private void OnEnable()
    {
        ZXLocal.LanguageChanged += d_LangaugeLoaded;
        ZXBootStrap.e_StartedLoading += d_UpdateLoading;
    }

    //LoadText
    private void LoadText()
    {
        loadingTitleText.text = ZXLocal.LocalText("GameTitle");
        loadingDetailsText.text = string.Empty;
        loadingFlavorText.text = string.Empty; //loadingFlavorText.text = ZXLocal.FlavorText();
    }

    private void UpdateLoadingProgress(string DisplayText, float Percent)
    {
        loadingDetailsText.text = DisplayText;
        CurrentPercentage = Percent;
    }

    private void OnDisable()
    {
        ZXLocal.LanguageChanged -= d_LangaugeLoaded;
        ZXBootStrap.e_StartedLoading -= d_UpdateLoading;
    }
}
