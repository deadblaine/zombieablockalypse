﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ZXSoundButton : ZXButton, IPointerClickHandler
{
    //Inspector Variables
    [SerializeField] private ZXAudioController.Channel channel;
    [SerializeField] private TextMeshProUGUI text;

    //Private variables
    private string key;
    private Image image;

    //Accessors
    private float Volume
    {
        get
        {
            switch (channel)
            {
                case ZXAudioController.Channel.Master:
                    return ZXAudioController.MasterVolume;

                case ZXAudioController.Channel.Music:
                    return ZXAudioController.MusicVolume;

                case ZXAudioController.Channel.SFX:
                    return ZXAudioController.SFXVolume;

                default:
                    return ZXAudioController.MasterVolume;
            }
        }
        set
        {
            switch (channel)
            {
                case ZXAudioController.Channel.Master:
                    ZXAudioController.MasterVolume = value;
                    break;

                case ZXAudioController.Channel.Music:
                    ZXAudioController.MusicVolume = value;
                    break;

                case ZXAudioController.Channel.SFX:
                    ZXAudioController.SFXVolume = value;
                    break;

                default:
                    ZXAudioController.MasterVolume = value;
                    break;
            }

            text.text = value == 0f ? "On" : "Off";
            image.color = value == 0f ? ZXDataManager.GetColor("GoGreen") : ZXDataManager.GetColor("StopRed");
        }
    }

    //Initalization
    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        Volume = Volume;
    }

    //Button function
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        Volume = Volume == 0f ? 1f : 0f;
    }
}
