﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ZXNewsButton : ZXButton
{
    [SerializeField] private GameObject notification;

    protected override void OnEnable()
    {
        base.OnEnable();

        notification.SetActive(ZXGiftButton.HasGift);
    }

    //Event handlers
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        notification.SetActive(false);

        ZXNewsPopup.ShowNews();
    }
}
