﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ZXFloatingVirus : MonoBehaviour
{
    private Image image;
    private RectTransform rTrans;
	
    // Use this for initialization
	private void Awake()
    {
        image = GetComponent<Image>();
        rTrans = GetComponent<RectTransform>();
	}

    public void StartFalling()
    {
        int duration = Random.Range(3, 6);

        image.DOFade(0, duration);
        rTrans.DOAnchorPos(new Vector2(Random.Range(-400, 400), Random.Range(-400, 0)), duration).OnComplete(delegate () { Destroy(gameObject); });
    }

    private void OnDisable()
    {
        DOTween.Kill(this);
    }
}
