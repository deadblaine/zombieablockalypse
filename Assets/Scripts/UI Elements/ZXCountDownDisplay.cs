﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ZXCountDownDisplay : MonoBehaviour
{
    [SerializeField] private ZXMissionManager.Type missionType;

    private TextMeshProUGUI display;
    private Action<TimeSpan> d_Update;

    private void Awake()
    {
        display = GetComponent<TextMeshProUGUI>();

        d_Update = new Action<TimeSpan>(UpdateClock);
    }

    private void OnEnable()
    {
        switch (missionType)
        {
            case ZXMissionManager.Type.Campaign:
                ZXMissionManager.e_Campaign_Clock += d_Update;
                break;

            case ZXMissionManager.Type.ClassKit:
                ZXMissionManager.e_Kit_Clock += d_Update;
                break;

            case ZXMissionManager.Type.BasicKit:
                ZXMissionManager.e_Kit_Clock += d_Update;
                break;
        }
    }

    private void UpdateClock(TimeSpan Time)
    {
        string text = string.Empty;

        if (Time.Days > 0)
            text = text + " " + Time.Days.ToString() + "D ";

        if (Time.Hours > 0 || Time.TotalHours > Time.Hours)
            text = text + " " + Time.Hours.ToString() + "H ";

        if (Time.Minutes > 0 || Time.TotalMinutes > Time.Minutes)
            text = text + " " + Time.Minutes.ToString() + "M ";

        if (Time.Seconds > 0 || Time.TotalSeconds > Time.Seconds)
            text = text + " " + Time.Seconds.ToString() + "S";

        display.text = text;
    }

    private void OnDisable()
    {
        switch (missionType)
        {
            case ZXMissionManager.Type.Campaign:
                ZXMissionManager.e_Campaign_Clock -= d_Update;
                break;

            case ZXMissionManager.Type.ClassKit:
                ZXMissionManager.e_Kit_Clock -= d_Update;
                break;

            case ZXMissionManager.Type.BasicKit:
                ZXMissionManager.e_Kit_Clock -= d_Update;
                break;
        }
    }
}
