﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using TMPro;
using DarkTonic.MasterAudio;

[RequireComponent(typeof(Button))]
public class ZXButton : MonoBehaviour, IPointerDownHandler, IPointerClickHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    public enum State
    {
        Down = 0,
        Clicked = 1,
        Up = 2
    }

    private const string tw_Base_Click_ID = "tw_click_click_";
    private const string tw_Base_Down_ID = "tw_button_down_";
    private const string tw_Base_Up_ID = "tw_button_up_";
    private const float tween_duration = 0.15f;
    private const float tween_downscale = 0.95f;

    //Inspector variables
    [SerializeField] private string buttonGroup;
    [SerializeField] private bool animate = true;
    [SerializeField] private bool validate = false;
    [SerializeField] private ZXDataManager.Currency currency;
    [SerializeField] protected TextMeshProUGUI costText;

    //Accessors
    public Button Button
    {
        get
        {
            return button;
        }
    }

    //Private variables
    protected Button button;
    private string tw_Click_ID;
    private string tw_Down_ID;
    private string tw_Up_ID;
    private bool valid;
    private int cost;
    private System.Action d_Validator;
    private State state = State.Up;

    private static Dictionary<string, List<ZXButton>> buttonGroups;
    private System.Action d_CurrencyHandler;

    //Initalization
    protected virtual void Awake()
    {
        button = GetComponent<Button>();
        tw_Click_ID = tw_Base_Click_ID + gameObject.GetInstanceID().ToString();
        tw_Down_ID = tw_Base_Down_ID + gameObject.GetInstanceID().ToString();
        tw_Up_ID = tw_Base_Up_ID + gameObject.GetInstanceID().ToString();

        d_CurrencyHandler = new System.Action(Validation);
    }

    protected virtual void OnEnable()
    {
        if (buttonGroups == null)
            buttonGroups = new Dictionary<string, List<ZXButton>>(20);

        if (!buttonGroups.ContainsKey(buttonGroup))
            buttonGroups.Add(buttonGroup, new List<ZXButton>(10));

        buttonGroups[buttonGroup].Add(this);

        if (validate)
            ZXDataManager.e_Currency_Updated += d_CurrencyHandler;

        if (animate)
            transform.localScale = Vector3.one;
    }

    //Validation
    private void Validation()
    {
        switch (currency)
        {
            case ZXDataManager.Currency.Silver:
                valid = cost <= ZXDataManager.Silver;
                break;

            case ZXDataManager.Currency.Gems:
                valid = cost <= ZXDataManager.Gems;
                break;

            case ZXDataManager.Currency.Fuel:
                valid = cost <= ZXDataManager.Fuel;
                break;
        }

        button.enabled = valid;
    }

    //Event handlers
    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (state != State.Clicked)
        {
            state = State.Clicked;
            UpdateAnimation();

            MasterAudio.PlaySound("UI_Single_Click");
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (state == State.Up)
        {
            state = State.Down;
            UpdateAnimation();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (state == State.Down && !eventData.eligibleForClick)
        {
            state = State.Up;
            UpdateAnimation();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        /*
        #if UNITY_EDITOR || UNITY_WEBGL
        if (state == State.Up && eventData.button == PointerEventData.InputButton.Left)
        #endif
        #if UNITY_IOS || UNITY_ANDROID
            if (state == State.Up)
        #endif
        {
            state = State.Down;
            UpdateAnimation();
        }
        */
    }
        public void OnPointerExit(PointerEventData eventData)
    {
        if (state == State.Down)
        {
            state = State.Up;
            UpdateAnimation();
        }
    }

    //Interaction functions
    public void UpdateCost(int NewCost)
    {
        costText.text = NewCost.ToString();

        Validation();
    }

    public void UpdateAnimation()
    {
        float speed = (1 - tween_downscale) / tween_duration;
        float distance;

        if (animate)
        {
            if (DOTween.IsTweening(this))
                DOTween.Kill(this);

            switch (state)
            {
                case State.Clicked:

                    distance = Mathf.Clamp(transform.localScale.x - tween_downscale, 0, int.MaxValue);

                    transform.DOScale(tween_downscale, distance / speed).SetEase(Ease.OutQuad).SetId(tw_Down_ID).OnComplete(delegate ()
                    {
                        transform.DOScale(1, tween_duration).SetEase(Ease.OutBack).SetId(tw_Up_ID).OnComplete(delegate ()
                        {
                            state = State.Up;
                        });
                    });
                    break;

                case State.Down:
                    distance = Mathf.Clamp(transform.localScale.x - tween_downscale, 0, int.MaxValue);
 
                    transform.DOScale(tween_downscale, distance / speed).SetEase(Ease.OutQuad).SetId(tw_Down_ID);
                    break;

                case State.Up:
                    distance = Mathf.Clamp(1 - transform.localScale.x, 0, int.MaxValue);

                    transform.DOScale(1, distance / speed).SetEase(Ease.OutQuad).SetId(tw_Up_ID);
                    break;
            }
        }
    }

    public static void Set(string ButtonGroup, bool Active)
    {
        if (buttonGroups.ContainsKey(ButtonGroup))
        {
            foreach (ZXButton button in buttonGroups[ButtonGroup])
            {
                button.Button.enabled = Active;
            }
        }
    }

    //Disable
    private void OnDisable()
    {
        buttonGroups[buttonGroup].Remove(this);

        if (validate)
            ZXDataManager.e_Currency_Updated -= d_CurrencyHandler;

        if (animate)
        {
            if (DOTween.IsTweening(this))
                DOTween.Kill(this);

            transform.localScale = Vector3.one;
        }
    }
}
