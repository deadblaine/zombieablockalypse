﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ZXLaunchButton : ZXButton, IPointerClickHandler
{
    [SerializeField] private ZXMissionManager.Type missionType;
    [SerializeField] private ZXMissionManager.Difficulty missionDifficulty;
    [SerializeField] private int missionCost;
    [SerializeField] private GameObject costDisplay;

    private string ID = string.Empty;

    protected override void OnEnable()
    {
        base.OnEnable();

        if (costDisplay != null)
        {
            costDisplay.SetActive(missionCost > 0);
        }

        if (costText != null)
        {
            costText.text = missionCost.ToString();
        }
    }

    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        if (missionType == ZXMissionManager.Type.BasicKit || missionType == ZXMissionManager.Type.ClassKit)
        { 
            if (ZXStarCounter.CanPlay(missionType, missionDifficulty))
                ZXMissionManager.StartMission(missionType, missionDifficulty, missionCost, ID);
            else
                ZXMessageScreen.ShowMessage("These missions are complete! Wait for them to refresh!", 2f);
        }
        else
        {
            ZXMissionManager.StartMission(missionType, missionDifficulty, missionCost, ID);
        }
    }

    public void SetID(string ID)
    {
        this.ID = ID;
    }
}
