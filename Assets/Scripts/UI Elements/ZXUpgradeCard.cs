﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXUpgradeCard : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI level;

    [SerializeField] private Image cardsFill;
    [SerializeField] private TextMeshProUGUI cardsText;

    public void ShowUpgrade(ZXCard Card)
    {
        image.sprite = Card.Card_Image;
        level.text = Card.LevelText;

        cardsFill.fillAmount = Card.CardsProgress;
        cardsText.text = Card.CardsProgressText;
    }
}
