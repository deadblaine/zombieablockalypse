﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZXCampaignPath : MonoBehaviour
{
    public bool Complete
    {
        get
        {
            return complete;
        }
    }

    private List<Image> images;
    private bool complete = false;

    private void Awake()
    {
        images = new List<Image>(GetComponentsInChildren<Image>());
    }

    public void CompletePath()
    {
        if (!complete)
        {            
            foreach (Image image in images)
            {
                image.color = Color.white;
            }

            complete = true;
        }
    }
}
