﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXPlayerLevel : MonoBehaviour
{
    //[SerializeField] private Image icon;
    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI levelText;

    private int level = 0;
    private float progress = 0;
    private System.Action<int> d_update;
    private System.Action<float> d_updateFill;

    // Use this for initialization
    private void Awake()
    {
        d_update = new System.Action<int>(UpdateAmount);
        d_updateFill = new System.Action<float>(UpdateFill);
    }

    private void OnEnable()
    {
        level = ZXDataManager.Level;
        progress = ZXDataManager.LevelProgress;

        ZXDataManager.e_LevelChanged += d_update;
        ZXDataManager.e_LevelProgressChanged += d_updateFill;

        UpdateAmount(level);
        UpdateFill(progress);
    }

    //Update functions
    protected virtual void UpdateAmount(int Level)
    {
        this.level = Level;

        levelText.text = level.ToString();
    }

    protected virtual void UpdateFill(float Percentage)
    {
        this.progress = Percentage;

        fill.fillAmount = progress;
    }

    private void OnDisable()
    {
        ZXDataManager.e_LevelChanged -= d_update;
        ZXDataManager.e_LevelProgressChanged -= d_updateFill;
    }
}
