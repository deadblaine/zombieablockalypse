﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXFuelBar : MonoBehaviour
{
    //[SerializeField] private Image icon;
    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI text;

    private int amount = 0;
    private float percentage = 0;
    private System.Action d_Update;

    // Use this for initialization
    private void Awake()
    {
        d_Update = new System.Action(UpdateFuel);
    }

    private void OnEnable()
    {
        amount = ZXDataManager.Fuel;
        percentage = ZXDataManager.FuelPercent;

        ZXDataManager.e_Currency_Updated += d_Update;

        UpdateFuel();
    }

    //Update functions
    protected virtual void UpdateFuel()
    {
        this.amount = ZXDataManager.Fuel;
        this.percentage = ZXDataManager.FuelPercent;

        fill.fillAmount = percentage;
        text.text = ZXDataManager.FuelAmountString;
    }

    private void OnDisable()
    {
        d_Update -= new System.Action(UpdateFuel);
    }
}
