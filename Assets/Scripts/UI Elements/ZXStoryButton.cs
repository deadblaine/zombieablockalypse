﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ZXStoryButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private Button button;
    private ZXCardInfoPopup cardInfo;

    // Use this for initialization
    private void Awake()
    {
        button = GetComponent<Button>();
        cardInfo = GetComponentInParent<ZXCardInfoPopup>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        cardInfo.SetStory(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        cardInfo.SetStory(false);
    }
}
