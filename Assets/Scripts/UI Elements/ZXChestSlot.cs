﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ZXChestSlot : MonoBehaviour, IPointerClickHandler
{
    private const string pp_Base_ID = "pp_Chest_Slot_";
    private const string pp_Chest_Opening_UID = "pp_Opening_Chest";
    private const int hours = 3;

    [SerializeField] private int slot;

    private Button button;
    private GameObject chest_Image;
    private GameObject time_Display;
    private TextMeshProUGUI time_Text;

    public static List<ZXChestSlot> slots;
    private string pp_Chest_Slot_ID;

    private static bool OpenChest
    {
        get
        {
            return PlayerPrefs.HasKey(pp_Chest_Opening_UID);
        }
        set
        {
            if (value)
                ChestUnlockDate = System.DateTime.Now.Add(new TimeSpan(0, hours, 0, 0));
            else
                PlayerPrefs.DeleteKey(pp_Chest_Opening_UID);
        }
    }

    private static TimeSpan ChestUnlockTime
    {
        get
        {
            return ChestUnlockDate.Subtract(DateTime.Now);
        }
    }

    private static DateTime ChestUnlockDate
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Chest_Opening_UID))
                return DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString(pp_Chest_Opening_UID)));
            else
                return DateTime.Now;
        }
        set
        {
            PlayerPrefs.SetString(pp_Chest_Opening_UID, value.ToBinary().ToString());
        }
    }

    public bool Chest
    {
        get
        {
            return PlayerPrefs.HasKey(pp_Chest_Slot_ID);
        }
        set
        {
            if (value)
                PlayerPrefs.SetInt(pp_Chest_Slot_ID, 0);
            else
                PlayerPrefs.DeleteKey(pp_Chest_Slot_ID);
        }
    }

    public bool Opening
    {
        get
        {
            if (PlayerPrefs.HasKey(pp_Chest_Slot_ID))
            {
                return PlayerPrefs.GetInt(pp_Chest_Slot_ID) == 1;
            }
            else
                return false;
        }
        set
        {
            PlayerPrefs.SetInt(pp_Chest_Slot_ID, value ? 1 : 0);
        }
    }

    public bool Unlocked
    {
        get
        {
            return ChestUnlockTime.TotalSeconds <= 0;
        }
    }

	// Use this for initialization
	private void Awake ()
    {
        pp_Chest_Slot_ID = pp_Base_ID + slot.ToString();

        if (slots == null)
            slots = new List<ZXChestSlot>(5);

        slots.Add(this);

        button = GetComponent<Button>();
        chest_Image = transform.Find("Chest_Image").gameObject;
        time_Display = transform.Find("Timer_Back").gameObject;
        time_Text = time_Display.transform.Find("Text").GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        string text = string.Empty;
        TimeSpan Time = ChestUnlockTime;

        if (Time.TotalSeconds > 0)
        {
            if (Time.Hours > 0)
                text = text + " " + Time.Hours.ToString() + "H ";

            if (Time.Minutes > 0 || Time.TotalMinutes > Time.Minutes)
                text = text + " " + Time.Minutes.ToString() + "M ";

            if (Time.Seconds > 0 || Time.TotalSeconds > Time.Seconds)
                text = text + " " + Time.Seconds.ToString() + "S";

            time_Text.text = text;
        }
        else
            text = "Now!";

        time_Text.text = text;
    }

    private void OnEnable()
    {
        if (Chest)
        {
            ShowChest();
        }
    }

    public static void PlaceChestInSlot()
    {
        foreach (ZXChestSlot slot in ZXChestSlot.slots)
        {
            if (!slot.Chest)
            {
                slot.AddChest();
                break;
            }
        }
    }

    public void AddChest()
    {
        Chest = true;
        ShowChest();
    }

    public void ShowChest()
    {
        chest_Image.SetActive(true);

        if (Opening)
        {
            time_Display.gameObject.SetActive(true);
            chest_Image.SetActive(true);
        }
    }

    private void TakeChest()
    {
        if (Opening)
            OpenChest = false;

        Chest = false;

        time_Display.gameObject.SetActive(false);
        chest_Image.SetActive(false);
    }

    private void OpenNewChest()
    {
        Opening = true;
        OpenChest = true;

        time_Display.gameObject.SetActive(true);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Chest && !OpenChest)
        {
            OpenNewChest();
        }
        else if (Chest && Opening && Unlocked)
        {
            TakeChest();
            ZXChestRewards.GrantChestRewards();
        }
        else if (Chest && ZXDataManager.Gems > 36)
        {
            ZXDataManager.Gems -= 36;
            TakeChest();
            ZXChestRewards.GrantChestRewards();
        }
    }

    private void OnDisable()
    {
        slots.Remove(this);
    }
}
