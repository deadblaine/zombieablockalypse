﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXStarReward : MonoBehaviour
{
    [SerializeField] private GameObject check;
    [SerializeField] private GameObject chest;

    public void Completed(bool Active)
    {
        check.SetActive(Active);
        chest.SetActive(!Active);
    }
}
