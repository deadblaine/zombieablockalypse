﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ZXCampaignButton : ZXButton, IPointerClickHandler
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private ZXCampaignPopup popup;

    [SerializeField] private bool open;

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    //Event handlers
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        ZXCampaignPopup.ShowPopup(open);
    }
}
