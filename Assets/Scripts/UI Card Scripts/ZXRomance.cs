﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class ZXRomance : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Image image;
    [SerializeField] private Image locked;
    [SerializeField] private Button button;
    [SerializeField] private TextMeshProUGUI rankText;
    [SerializeField] private ZXCardInfoPopup cardInfo;

    private ZXCard.Romance romance;

    private void Awake()
    {
        image = GetComponent<Image>();
        button = GetComponent<Button>();
        cardInfo = GetComponentInParent<ZXCardInfoPopup>();
    }

    public void Set(ZXCard.Romance Romance)
    {
        romance = Romance;
        image.sprite = romance.Thumb;

        if (Romance.Locked)
        {
            locked.gameObject.SetActive(true);
            button.interactable = false;

            rankText.text = Romance.Requiered_Rank.ToString();
        }
        else
        {
            locked.gameObject.SetActive(false);
            button.interactable = true;
        }

        gameObject.SetActive(true);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (button.interactable)
            cardInfo.SetImage(true, romance.Image);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        cardInfo.SetImage(false, null);
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
