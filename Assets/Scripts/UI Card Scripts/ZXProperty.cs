﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class ZXProperty : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI value;

    public void Set(ZXCard.Property NewProperty)
    {
        icon.sprite = NewProperty.Icon;
        title.text = NewProperty.Name;

        value.text = NewProperty.LevelAdjustedValue;

        gameObject.SetActive(true);
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }
}
