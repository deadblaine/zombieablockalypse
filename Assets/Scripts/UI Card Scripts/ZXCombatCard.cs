﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;
using DG.Tweening;
using ZXCore;
using DarkTonic.MasterAudio;
using TMPro;

public class ZXCombatCard : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    private Image image;
    private Button button;
    private int cost;
    private TextMeshProUGUI costText;
    private int pool;

    IZXInstructable dragObject;
    ZXToon toon;
    ZXSpell spell;

    public static event System.Action e_DragStarted;
    public static event System.Action<bool> e_DragEnded;

    void Awake()
    {
		button = GetComponent<Button>();
        image = GetComponent<Image>();
        costText = GetComponentInChildren<TextMeshProUGUI>();
    }

	void Update()
	{
        if (ZXManaBar.Mana >= cost)
            button.interactable = true;
        else
            button.interactable = false;

		if(toon)
		{
			toon.GetComponent<ZXHuman>().FindObjective();
			toon.GetComponent<ZXHuman>().PingObjective();
		}
	}

    public void NextCard(Sprite CardImage, int CardCost, int CardPool)
    {
        image.sprite = CardImage;
        cost = CardCost;
        costText.text = CardCost.ToString();
        pool = CardPool;
    }

    private void GetCardObject()
    {
        ResetDragObject();

        switch(pool)
        {
            case 1:
                toon = ZXCombatDeck.p_PlayerToon1.Request();
                break;
            case 2:
                toon = ZXCombatDeck.p_PlayerToon2.Request();
                break;
            case 3:
                toon = ZXCombatDeck.p_PlayerToon3.Request();
                break;
            case 4:
                spell = ZXCombatDeck.p_PlayerSpell1.Request();
                break;
            case 5:
                spell = ZXCombatDeck.p_PlayerSpell2.Request();
                break;
            case 6:
                spell = ZXCombatDeck.p_PlayerSpell3.Request();
                break;
        }

        if (toon != null)
            dragObject = toon.GetComponent<IZXInstructable>();

        if (spell != null)
            dragObject = spell.GetComponent<IZXInstructable>();
    }

    private void ResetDragObject()
    {
        dragObject = null;
        toon = null;
        spell = null;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
		if(!button.interactable) return;

        image.color = Color.green;

        if (dragObject == null)
            GetCardObject();

        if (toon)
        {
            dragObject.Place(eventData.pointerCurrentRaycast.worldPosition + new Vector3(0, 2, 0), Quaternion.LookRotation(Vector3.left, Vector3.up), ZXToon.Action.DragFromUI);

            ZXSpawnArea.SetActive(true);

            toon.gameObject.SetActive(true);

            foreach (Collider co in toon.GetComponentsInChildren<Collider>())
            {
                co.enabled = false;
            }
        }

        if (e_DragStarted != null)
            e_DragStarted();
    }

    public void OnDrag(PointerEventData eventData)
    {
		if (toon)
        {
            toon.transform.position = ZXSpawnArea.BoxCollider.ClosestPointOnBounds(eventData.pointerCurrentRaycast.worldPosition) + new Vector3(0, 2.2f, 0);

            toon.transform.rotation = Quaternion.LookRotation(Vector3.left, Vector3.up);
        }

        if (spell)
        {
            List<ZXToon> enemies;

            spell.transform.position = eventData.pointerCurrentRaycast.worldPosition + new Vector3(0, 0.2f, 0);

            enemies = ZXReflectingPool.FindInSphere<ZXToon>(eventData.pointerCurrentRaycast.worldPosition, 8, delegate (ZXToon enemy) { return enemy.Race != ToonRace.Human; });

            if (enemies != null)
            {
                enemies.ForEach(delegate (ZXToon obj)
                {
                    //obj.Shader.BlinkEmission();
                });
            }

            spell.Place(eventData.pointerCurrentRaycast.worldPosition, transform.rotation, false);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        RaycastHit hit;
        bool result = false;

		if(toon)
		{
            if (Physics.Raycast(toon.transform.position, Vector3.down, out hit, 1000, ZXCombatDeck.IgnoreMaskToons) && eventData.pointerCurrentRaycast.gameObject == ZXGroundEvents.instance.gameObject)
	        {
                foreach (Collider co in toon.GetComponentsInChildren<Collider>())
                {
                    co.enabled = true;
                }

                dragObject.Place(toon.transform.position, Quaternion.LookRotation(Vector3.left, Vector3.up), ZXToon.Action.LandFromFalling);

                result = true;
	        }	   
		}
		
        if (spell)
		{
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, ZXCombatDeck.IgnoreMaskSpells) && eventData.pointerCurrentRaycast.gameObject == ZXGroundEvents.instance.gameObject)
			{
                spell.Place(eventData.pointerCurrentRaycast.worldPosition, transform.rotation, true);

                result = true;
            }
            else
            {              
                spell.gameObject.SetActive(false);
            }
        }

        image.color = Color.white;
        ZXSpawnArea.SetActive(false);

        ResetDragObject();

        if (e_DragEnded != null)
            e_DragEnded(result);

        if (result)
        {
            ZXManaBar.Mana -= cost;
            ZXCombatDeck.DealCard(this);
        }
    }
}
