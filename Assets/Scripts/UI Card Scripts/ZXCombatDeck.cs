﻿using System.Collections;
using System.Collections.Generic;
using ZXCore;
using UnityEngine;
using UnityEngine.UI;

public class ZXCombatDeck : MonoBehaviour
{
    //Accessors
    [SerializeField] private LayerMask spawnAreaMask;

    public static LayerMask SpawnAreaMask
    {
        get
        {
            return singleton.spawnAreaMask;
        }
    }

    [SerializeField] private LayerMask ignoreMaskToons;

    public static LayerMask IgnoreMaskToons
    {
        get
        {
            return singleton.ignoreMaskToons;
        }
    }

    [SerializeField] private LayerMask ignoreMaskSpells;

    public static LayerMask IgnoreMaskSpells
    {
        get
        {
            return singleton.ignoreMaskSpells;
        }
    }

    private List<ZXCombatCard> cardspots;

    /* Char Pools */
    public static ZXPoolLite<ZXToon> p_Hero;
    private int p_HeroSize = 3;

    public static ZXPoolLite<ZXToon> p_PlayerToon1;
    private int p_PlayerToon1Size = 5;

    public static ZXPoolLite<ZXToon> p_PlayerToon2;
    private int p_PlayerToon2Size = 5;

    public static ZXPoolLite<ZXToon> p_PlayerToon3;
    private int p_PlayerToon3Size = 5;

    public static ZXPoolLite<ZXSpell> p_PlayerSpell1;
    private int p_PlayerSpell1Size = 3;

    public static ZXPoolLite<ZXSpell> p_PlayerSpell2;
    private int p_PlayerSpell2Size = 3;

    public static ZXPoolLite<ZXSpell> p_PlayerSpell3;
    private int p_PlayerSpell3Size = 3;

    //Private variables
    private static ZXCombatDeck singleton;
    private static List<ZXCard> deck;
    private static int index = 1;

    private void Awake()
    {
        singleton = this;

        cardspots = new List<ZXCombatCard>(GetComponentsInChildren<ZXCombatCard>());
    }

    private void Start()
    {
        deck = ZXDeckManager.FetchCards();

        LoadCardPools(deck[0].PreFab.GetComponent<ZXToon>(), deck[1].PreFab.GetComponent<ZXToon>(), deck[2].PreFab.GetComponent<ZXToon>(), deck[3].PreFab.GetComponent<ZXToon>(), deck[4].PreFab.GetComponent<ZXSpell>(), deck[5].PreFab.GetComponent<ZXSpell>(), deck[6].PreFab.GetComponent<ZXSpell>());

        foreach (ZXCombatCard card in cardspots)
        {
            DealCard(card);
        }
    }

    private void LoadCardPools(ZXToon Hero, ZXToon Toon1, ZXToon Toon2, ZXToon Toon3, ZXSpell Spell1, ZXSpell Spell2, ZXSpell Spell3)
    {
        p_Hero = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, Hero, p_HeroSize);
        p_PlayerToon1 = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, Toon1, p_PlayerToon1Size);
        p_PlayerToon2 = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, Toon2, p_PlayerToon2Size);
        p_PlayerToon3 = new ZXPoolLite<ZXToon>(ZXCombatUI.ObjectMask, Toon3, p_PlayerToon3Size);
        p_PlayerSpell1 = new ZXPoolLite<ZXSpell>(ZXCombatUI.ObjectMask, Spell1, p_PlayerSpell1Size);
        p_PlayerSpell2 = new ZXPoolLite<ZXSpell>(ZXCombatUI.ObjectMask, Spell2, p_PlayerSpell2Size);
        p_PlayerSpell3 = new ZXPoolLite<ZXSpell>(ZXCombatUI.ObjectMask, Spell3, p_PlayerSpell3Size);
    }

    public static void DealCard(ZXCombatCard Card)
    {
        Card.NextCard(deck[index].Card_Image, deck[index].Cost, index);

        index = index + 1 < deck.Count ? index + 1 : 1;
    }
}
