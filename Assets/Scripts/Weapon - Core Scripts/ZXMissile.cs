﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using Vectrosity;
using DarkTonic.MasterAudio;

public class ZXMissile : ZXExplosive
{
    //Consts
    private const float _missileFlightDrag = 10f;
    private const float _navTolerance = 0.7f;
    private const float _apexMissiletime = 1.5f;

    [SoundGroupAttribute]
    [SerializeField]
    protected string s_Engine;

    //Support types
    public enum TrajectoryType
    {
        DirectFire = 0,
        TwoStage = 1
    }

    //Inspector
    [SerializeField] private float missileVelocity;
    [SerializeField] private TrajectoryType trajectoryType;
    [SerializeField] private int trajPointFreq;
    [SerializeField] private float trajVar;
    [SerializeField] private float engineLifetime;
    [SerializeField] protected ParticleSystem apexParticlePrefab;

    //Private variables
    private bool engineOn = false;
    private float engineTimer = 0f;

    private PlaySoundResult soundResult;
    private AudioSource engineSound;

    private List<Vector3> waypoints;

    private float timeSinceLaunch;
    private Transform currentTarget;
    private Vector3 initLaunchPosition;
    protected ParticleSystem apex;

    private Sequence twoStageSequence;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        waypoints = new List<Vector3>(10);

        if (apexParticlePrefab != null)
        {
            apex = Instantiate<ParticleSystem>(apexParticlePrefab);
            apex.transform.SetParent(transform);
            apex.transform.localPosition = Vector3.zero;
            apex.transform.localRotation = Quaternion.identity;
            apex.gameObject.SetActive(false);
        }
    }

    protected override void Start()
    {
        base.Start();

        if (useWeaponValues && weapon is ZXLauncher)
            missileVelocity = (weapon as ZXLauncher).MissileVelocity;
    }

    //Runtime
    protected override void FixedUpdate()
    {
        base.FixedUpdate();

        if (engineOn)
        {
            timeSinceLaunch += Time.fixedDeltaTime * 3;

            float currentVelocity = rigidbody.velocity.magnitude;

            missileVelocity *= 1.02f;
            float difference = missileVelocity - currentVelocity;

            rigidbody.AddRelativeForce(Vector3.forward * difference * rigidbody.mass, ForceMode.Impulse);
            rigidbody.AddForce(new Vector3(0, 1, 0) * Mathf.Sin(Time.time * 10) * 6, ForceMode.Impulse);
        }
    }

    protected void Update()
    {
        if (engineTimer > 0f)
            engineTimer -= Time.deltaTime;
        else
            SetEngine(false);

        if (waypoints.Count > 0 && Vector3.Distance(transform.position, waypoints[0]) < _navTolerance)
            waypoints.RemoveAt(0);

        if (waypoints.Count <= 0)
            SetEngine(false);
    }

    //Main functions
    protected override void EnableRigidbody(bool Active)
    {
        base.EnableRigidbody(Active);

        if (Active)
            rigidbody.drag = _missileFlightDrag;
    }

    public void Launch(Vector3 Velocity, Vector3 TargetPosition, IZXTargetable Target)
    {
        initLaunchPosition = transform.position;

        if (Target != null)
            currentTarget = Target.LookAtTransform;

        PlotCourse(TargetPosition);

        timeSinceLaunch = 0;
         
        if (trajectoryType == TrajectoryType.DirectFire)
        {
            base.Launch(Velocity);
            SetEngine(true);
        }
        if (trajectoryType == TrajectoryType.TwoStage)
        {
            Vector3 firstStagePos = new Vector3();
            Vector3 secondStagePos = currentTarget.position;
            twoStageSequence = DOTween.Sequence();

            base.Launch(Vector3.zero);

            firstStagePos.x = initLaunchPosition.x + (currentTarget.position.x - initLaunchPosition.x) / 4.0f;
            firstStagePos.y = transform.position.y + 5;
            firstStagePos.z = initLaunchPosition.z + (currentTarget.position.z - initLaunchPosition.z) / 4.0f;

            twoStageSequence.Append(transform.DOMove(firstStagePos, _apexMissiletime).SetEase(Ease.OutQuad));
            twoStageSequence.AppendCallback(playApexParticle);
            twoStageSequence.Append(transform.DOMove(secondStagePos, _apexMissiletime / 5).SetEase(Ease.InQuad));
        }
    }

    private void SetEngine(bool Active)
    {
        engineOn = Active;

        if (Active)
        {
            soundResult = MasterAudio.PlaySound3DAtTransform(s_Engine, transform);

            if (soundResult != null && soundResult.SoundPlayed)
            {
                engineSound = soundResult.ActingVariation.VarAudio;
                engineSound.loop = true;
            }

            engineTimer = engineLifetime;
        }
        else
        {
            rigidbody.useGravity = true;
            rigidbody.drag = _dragFactor;

            if (soundResult != null && soundResult.SoundPlayed)
            {
                engineSound.Stop();
            }
        }
    }

    protected override void RotateTranslation()
    {
            Vector3 direction = Vector3.zero;
            float amount = _turningSpeed * Time.fixedDeltaTime;

            if (waypoints.Count > 0)
            {
                direction = (waypoints[0] - transform.position).normalized;
            }
            else
                direction = rigidbody.velocity.normalized;

            direction = Vector3.RotateTowards(transform.forward, direction, amount, 0.0F);

            if (direction != Vector3.zero)
                transform.rotation = Quaternion.LookRotation(direction);
    }

    protected void PlotCourse(Vector3 Target)
    {
        float distance = Vector3.Distance(transform.position, Target);
        int pointCount = Mathf.RoundToInt(distance / trajPointFreq) - 1;
        Ray ray = new Ray(transform.position, Target - transform.position);
        Vector3 currentPoint;

        //Clear existing waypoints
        waypoints.Clear();

        if (trajectoryType == TrajectoryType.DirectFire)
        {
            //Calculate the randomized waypoints
            for (int i = 1; i < pointCount; i++)
            {
                currentPoint = ray.GetPoint(trajPointFreq * i);
                currentPoint += new Vector3(Random.Range(-trajVar, trajVar), Random.Range(-trajVar, trajVar), Random.Range(-trajVar, trajVar));
                waypoints.Add(currentPoint);
            }
        }
        else if (trajectoryType == TrajectoryType.TwoStage)
        {
            waypoints.Add(new Vector3(
            initLaunchPosition.x + (currentTarget.position.x - initLaunchPosition.x) / 4.0f, 
            transform.position.y + 5, 
            initLaunchPosition.z + (currentTarget.position.z - initLaunchPosition.z) / 4.0f));
        }

        //Add final waypoint
        waypoints.Add(Target);

        //Debugging
        for (int i = 1; i < waypoints.Count; i++)
        {
            Debug.DrawLine(waypoints[i - 1], waypoints[i], Color.red, 5f);
        }
    }

    private void playApexParticle()
    {
        if (apex != null)
        {
            apex.gameObject.SetActive(true);
            apex.transform.SetParent(null);
            apex.Play();
        }
    }

    protected override void Disable()
    {
        base.Disable();

        if (apex != null)
        {
            apex.transform.SetParent(transform);
            apex.transform.localPosition = Vector3.zero;
            apex.transform.localRotation = Quaternion.identity;
            apex.gameObject.SetActive(false);
        }
    }
}
