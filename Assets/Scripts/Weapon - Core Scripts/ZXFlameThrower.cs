﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXFlameThrower : ZXFirearm
{
    [SerializeField] ToonRace ignoreRace;

    //Constant variables
    private const int _muzzleEmissionRate = 10;

    //Private variables
    private ZXFlameThrowerParticle p_FlameSystem;

    protected override void Awake()
    {
        base.Awake();

        de_FirePattern = d_ConicFire;
    }

    protected override void Start()
    {
        base.Start();

        p_FlameSystem = p_muzzleFlash.GetComponent<ZXFlameThrowerParticle>();
        p_FlameSystem.e_DamagableCollision += new System.Action<IZXDamageable>(ParticleDamage);

        if (p_FlameSystem == null)
            throw new System.Exception("Flame throw was not set up correctly");
    }

    protected override void DischargeWeapon()
    {
        if (DamageableList != null && DamageableList.Count > 0)
        {
            ZXDamage damage;
            int adjWeaponDamage = weaponDamage;
            float lastDensity = 0;
            Ray trajectory;

            for (int i = 0; i < DamageableList.Count; i++)
            {
                //Look on the objects parent
                if (DamageableList[i] != null)
                {
                    adjWeaponDamage -= Mathf.RoundToInt(weaponDamage * Mathf.Clamp(lastDensity - penetration, 0f, 0.9f));
                    lastDensity = DamageableList[i].Density;

                    if (adjWeaponDamage <= 0)
                        return;

                    trajectory = new Ray(AimTransform.position, (DamageableList[i].Center - AimTransform.position).normalized);
                }
            }
        }
    }

    protected void ParticleDamage(IZXDamageable Damageable)
    {
        if (t_Muzzle == null)
            return;

        //Create damage object
        Vector3 direction = (Damageable.Center - t_Muzzle.position).normalized;
        Ray ray = new Ray(transform.position, direction);
        ZXDamage damage = new ZXDamage(weaponDamage, 0f, 0.25f, 0f, Race, ZXDamage.Type.Fire, ray, ZXDamage.SpecialEffect.ForceFire);

        //Send damage object
        if (Damageable.Race != Race)
        {
            if (ignoreRace != ToonRace.None && Damageable.Race != ignoreRace)
                Damageable.TakeDamage(damage);
        }
    }
}
