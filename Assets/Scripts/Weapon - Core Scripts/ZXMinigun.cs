﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using System;
using DarkTonic.MasterAudio;

public class ZXMinigun : ZXFirearm
{
    //Inspector variables
    [SerializeField] private float rateOfFireMax;

    //Private variables
    private List<ZXWeaponPoint> t_Barrels;
    private float currentBarrel;
    private ZXMinigunBarrel masterBarrel;

    protected override void Awake()
    {
        base.Awake();

        de_FirePattern = d_LinearFire;

        t_Barrels = weaponPoints.FindAll(delegate (ZXWeaponPoint Object) { return Object.PartType == ZXWeaponPoint.Part.Barrel; });
        masterBarrel = GetComponentInChildren<ZXMinigunBarrel>();
    }

    protected override void DischargeWeapon()
    {
        if (DamageableList != null && DamageableList.Count > 0)
        {
            ZXDamage damage;
            int adjWeaponDamage = weaponDamage;
            float lastDensity = 0;
            Ray trajectory;

            for (int i = 0; i < DamageableList.Count; i++)
            {
                //Look on the objects parent
                if (DamageableList[i] != null)
                {
                    adjWeaponDamage -= Mathf.RoundToInt(weaponDamage * Mathf.Clamp(lastDensity - penetration, 0f, 0.9f));
                    lastDensity = DamageableList[i].Density;

                    if (adjWeaponDamage <= 0)
                        return;

                    trajectory = new Ray(AimTransform.position, (DamageableList[i].Center - AimTransform.position).normalized);

                    damage = new ZXDamage(adjWeaponDamage, penetration, forceMod, 0f, Race, ZXDamage.Type.Projectile, trajectory, ZXDamage.SpecialEffect.None);
                    DamageableList[i].TakeDamage(damage);
                }
            }
        }
    }
}
