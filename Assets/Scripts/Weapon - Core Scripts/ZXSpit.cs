﻿using UnityEngine;
using System.Collections;
using ZXCore;

public sealed class ZXSpit : ZXExplosive
{
    //Initalization
    protected override void Awake()
    {
        base.Awake();

        triggerCondition = new System.Predicate<Collider>(delegate (Collider Other) { return Other.GetComponentInParent<ZXZombie>() == null; });
    }

    protected override void Start()
    {
        base.Start();

        gameObject.layer = 21;
    }
}
