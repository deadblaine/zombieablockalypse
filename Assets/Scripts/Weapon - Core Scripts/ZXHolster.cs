﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZXHolster : MonoBehaviour
{
    public enum Holster
    {
        Left_Hand,
        Right_Hand,
        Back_Holster,
        Back_Alt_Holster,
        Backpack_Holster,
        Left_Front_Hip_Holster,
        Left_Back_Hip_Holster,
        Left_Shield_Holster,
        Right_Hip_Holster
    }

    //Accessors
    public Holster HolsterType
    {
        get
        {
            return holsterType;
        }
    }

    //Variables
    private static List<Holster> PartsList = new List<Holster>(System.Enum.GetValues(typeof(Holster)) as Holster[]);
    private Holster holsterType;

    public void SetType(Transform Parent, Holster HolsterType)
    {
        holsterType = HolsterType;
        transform.SetParent(Parent);

        gameObject.name = HolsterType.ToString();

        switch (HolsterType)
        {
            case Holster.Left_Hand:
                transform.localRotation = Quaternion.Euler(new Vector3(0f, -90f, 180f));
                transform.localPosition = new Vector3(-0.1f, 0f, 0f);
                break;

            case Holster.Right_Hand:
                transform.localRotation = Quaternion.Euler(new Vector3(0f, 90f, 90f));
                transform.localPosition = new Vector3(0.1f, 0f, 0f);
                break;

            case Holster.Back_Holster:
                transform.localRotation = Quaternion.Euler(new Vector3(45f, 90f, -180f));
                transform.localPosition = new Vector3(-0.9f, 0.45f, -0.35f);
                break;

            case Holster.Back_Alt_Holster:
                break;

            case Holster.Backpack_Holster:
                transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 90f));
                transform.localPosition = new Vector3(0f, 0f, -0.335f);
                break;

            case Holster.Left_Front_Hip_Holster:
                transform.localRotation = Quaternion.Euler(new Vector3(38f, -25f, -220.1f));
                transform.localPosition = new Vector3(-0.38f, 0.36f, 0.14f);
                break;

            case Holster.Left_Back_Hip_Holster:
                transform.localRotation = Quaternion.Euler(new Vector3(-30.1f, -90f, -90.1f));
                transform.localPosition = new Vector3(-0.36f, 0.4f, -0.17f);
                break;

            case Holster.Left_Shield_Holster:
                transform.localRotation = Quaternion.Euler(new Vector3(90f, 0f, 0f));
                transform.localPosition = new Vector3(0.25f, -0.1f, 0f);
                break;

            case Holster.Right_Hip_Holster:
                transform.localRotation = Quaternion.Euler(new Vector3(55f, -125f, -120f));
                transform.localPosition = new Vector3(0.5f, -0.07f, 0.055f);
                break;
        }
    }
}
