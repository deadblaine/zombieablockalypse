﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXCrossbow : ZXLauncher
{
    public override void SnapToTransform(ZXHolster holster)
    {
        base.SnapToTransform(holster);

        if (holster.HolsterType == ZXHolster.Holster.Back_Holster || holster.HolsterType == ZXHolster.Holster.Back_Alt_Holster)
            t_Grip.transform.localRotation *= Quaternion.Euler(0f, 0f, 90f);
    }
}
