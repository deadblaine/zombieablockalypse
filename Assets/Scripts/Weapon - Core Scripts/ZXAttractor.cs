﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;


public class ZXAttractor : ZXInterestPoint
{
    private float effectPulseSpeed = 2f;

    private bool active = false;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        pointType = Interest.Attractor;

        ZXReflectingPool.Register<ZXAttractor>(this, transform);
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        active = true;
        StartCoroutine(Pulse());
    }

    public override int Interact(IZXTargetable Actor)
    {
        return 0;
    }

    //Main function
    private IEnumerator Pulse()
    {
        List<ZXToon> targets = null;

        while (active)
        {
            targets = ZXReflectingPool.FindInSphere<ZXToon>(transform.position, interactionRadius, targetingParams);

            if (targets != null && targets.Count > 0)
            {
                targets.ForEach(delegate (ZXToon target)
                {
                    if (target.canMove)
                        target.Move(transform.position);
                });
            }

            targets = null;

            yield return new WaitForSeconds(effectPulseSpeed);
        }
    }

    private bool targetingParams(ZXToon target)
    {
        return target.Race == race;
    }

    protected override void OnDisable()
    {
        base.OnDisable();

        ZXReflectingPool.DeRegister<ZXAttractor>(transform);

        active = false;
        StopAllCoroutines();
    }
}
