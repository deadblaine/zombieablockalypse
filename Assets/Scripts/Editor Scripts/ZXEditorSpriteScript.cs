﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXEditorSpriteScript : MonoBehaviour
{
    [SerializeField] private bool placeOnNavMesh;

    private new SpriteRenderer renderer;

	void Awake ()
    {
        UnityEngine.AI.NavMeshHit hit;

        //Place on nav mesh
        if (placeOnNavMesh)
        {
            if (UnityEngine.AI.NavMesh.SamplePosition(transform.position, out hit, 5f, UnityEngine.AI.NavMesh.AllAreas))
            {
                transform.position = hit.position;
            }
            else
                Debug.Log(gameObject.name + " could not be placed on NavMesh");
        }

        //Get sprite renderer
        renderer = GetComponent<SpriteRenderer>();

        //Disable sprite renderer
        if (renderer != null)
            renderer.enabled = false;
    }
}
