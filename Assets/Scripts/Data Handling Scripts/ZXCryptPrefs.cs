﻿using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Threading;
 

public class ZXCryptPrefs : MonoBehaviour
{
    //Constants
    private const int Iterations = 255;

    //Providers
    private static DESCryptoServiceProvider des = new DESCryptoServiceProvider();

    //Keys
    private const string encryptKey = "hwh63g0KxUXE384900pF9N1246l1666P";
    private static byte[] key;
    private static byte[] salt;
    private static byte[] IV;

    delegate bool encryptDel(string inStr, out string outStr);

    private static encryptDel d_Encrypt = new encryptDel(Encrypt);

    private static Queue<Encryptor> vault = null;

    private static ZXCryptPrefs singleton;

    //Initalization
    private void Awake()
    {
        if (singleton == null)
            singleton = this;
        else
            DestroyImmediate(this);

        Initialize();
    }

    private void Initialize()
    {
        //Create salt
        #if UNITY_IOS || UNITY_ANDROID || UNITY_EDITOR
        salt = Encoding.UTF8.GetBytes(SystemInfo.deviceUniqueIdentifier);
        #endif

        #if UNITY_WEBGL
        salt = Encoding.UTF8.GetBytes("f49c77HRAphfWE6rHwRSBoFXs4mF3bd8");
        #endif

        //Create the primary key
        if (PlayerPrefs.HasKey(encryptKey))
            key = new Rfc2898DeriveBytes(PlayerPrefs.GetString(encryptKey), salt, Iterations).GetBytes(8);
        else
        {
            string newKey;

            //Generate new key
            des.GenerateKey();
            newKey = Convert.ToBase64String(des.Key);

            //Save new key
            PlayerPrefs.SetString(encryptKey, newKey);

            key = new Rfc2898DeriveBytes(newKey, salt, Iterations).GetBytes(8);
        }

        //Create IV
        IV = Encoding.UTF8.GetBytes("aomy6ew6BE920Y2viK6qm1rY3pc0e9d5");

        vault = new Queue<Encryptor>(5);
    }

    //Runtime
    private void Update()
    {
        Encryptor completedEncryptor;

        while (vault != null && vault.Count > 0 && vault.Peek().Completed)
        {
            completedEncryptor = vault.Dequeue();

            if (!completedEncryptor.Voided)
                PlayerPrefs.SetString(completedEncryptor.Key, completedEncryptor.Value);
        }
    }

    //Player prefs interface
    public static void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

    public static bool HasKey(string key)
    {
        string eString;
        bool flag = Encrypt(key, out eString);

        if (flag)
            return PlayerPrefs.HasKey(eString);
        else
            return false;
    }

    public static void DeleteKey(string key)
    {
        string eString;
        bool flag = Encrypt(key, out eString);

        if (flag && PlayerPrefs.HasKey(eString))
            PlayerPrefs.DeleteKey(eString);
    }

    public static void Save()
    {
        PlayerPrefs.Save();
    }

    //Encryption get functions
    public static float GetFloat(string key)
    {
        float value;

        if (float.TryParse(GetString(key), out value))
            return value;
        else
            return 0f;
    }

    public static int GetInt(string key)
    {
        int value;

        if (int.TryParse(GetString(key), out value))
            return value;
        else
            return 0;
    }

    public static string GetString(string key)
    {
        string eString;
        bool flag = Encrypt(key, out eString);
        string value;

        if (flag)
        {
            value = Decrypt(PlayerPrefs.GetString(eString));

            if (singleton != null && ZXErrorHandler.Logging)
                Debug.Log("Loading... " + key + ": " + value);

            return value;
        }
        else
            return "";
    }

    //Encryption set functions
    public static void SetFloat(string key, float value)
    {
         SetString(key, System.Convert.ToString(value));
    }

    public static void SetInt(string key, int value)
    {
        SetString(key, System.Convert.ToString(value));
    }

    public static void SetString(string key, string value)
    {
        vault.Enqueue(new Encryptor(key, value, d_Encrypt));

        if (singleton != null && ZXErrorHandler.Logging)
            Debug.Log("Saving... " + key + ": " + value);
    }

    //Private encryption functions
    private static bool Encrypt(string strPlain, out string strEncrypted)
    {
        try
        {
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);

            //Write to stream 
            memoryStream.Write(IV, 0, IV.Length);

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(strPlain);

            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();

            //Return the encrypted value
            strEncrypted = Convert.ToBase64String(memoryStream.ToArray());
            return true;
        }
        catch (Exception e)
        {
            Debug.LogWarning("Encrypt Exception: " + e);

            strEncrypted = strPlain;
            return false;
        }
    }

    private static string Decrypt(string strEncrypt)
    {
        try
        {
            byte[] cipherBytes = Convert.FromBase64String(strEncrypt);
            MemoryStream memoryStream = new MemoryStream(cipherBytes);
            memoryStream.Read(IV, 0, IV.Length);

            // use derive bytes to generate key from password and IV
            CryptoStream cryptoStream = new CryptoStream(memoryStream, des.CreateDecryptor(key, IV), CryptoStreamMode.Read);
            StreamReader streamReader = new StreamReader(cryptoStream);

            return streamReader.ReadToEnd();
        }
        catch (Exception e)
        {
            Debug.LogWarning("Decrypt Exception: " + e);
            return strEncrypt;
        }
    }

    //Encryption threader
    private class Encryptor
    {
        Thread cryptWorker;

        //Accessors
        public bool Completed
        {
            get
            {
                return completed;
            }
        }
        public bool Voided
        {
            get
            {
                return voided;
            }
        }
        public string Key
        {
            get
            {
                return key;
            }
        }
        public string Value
        {
            get
            {
                return value;
            }
        }
        
        //Private variables
        private bool completed = false;
        private bool voided = false;
        private string inKey;
        private string inValue;
        private string key;
        private string value;

        //Delegate
        encryptDel d_Encryption;

        public Encryptor(string Key, string Value, encryptDel Encryption)
        {
            this.inKey = Key;
            this.inValue = Value;

            d_Encryption = Encryption;

            #if UNITY_IOS || UNITY_ANDOIRD || UNITY_EDITOR
            cryptWorker = new Thread(new ThreadStart(CryptThread));
            cryptWorker.Priority = System.Threading.ThreadPriority.Lowest;
            cryptWorker.Start();
            #endif

            #if UNITY_WEBGL
            CryptThread();
            #endif
        }

        private void CryptThread()
        {
            bool keyFlag = d_Encryption(inKey, out key);
            bool valueFlag = d_Encryption(inValue, out value);

            if (keyFlag && valueFlag)
                completed = true;
            else
            {
                completed = true;
                voided = true;
            }
        }
    }
}


