﻿using System.Collections;
using System.Collections.Generic;
using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

public class ZXPermissionManager : MonoBehaviour, ZXCore.IZXLoadable
{
    //Constants
    private const string PP_ID_Credentials = "PP_Cred_ID";
    private const string PP_ID_GDRP = "PP_GDRP_ID";

    //IZXLoadable Implimentation
    public event System.Action e_LoadingComplete;

    public static ZXCore.IZXLoadable Loadable
    {
        get
        {
            return singleton;
        }
    }
    public List<System.Action> LoadingList
    {
        get
        {
            return new List<System.Action>(new System.Action[] { Login, LoadPlayFabAccountInfo, LoadPlayFabProfile });
        }
    }

    //Accessors
    public static string PlayerName
    {
        get
        {
            return playFabAccount.Username;
        }
    }
    public static string PlayFabID
    {
        get
        {
            return playFabLogin.PlayFabId;
        }
    }

    //Private Accessors
    private static string credentials = null;

    private static string Credentials
    {
        get
        {
            if (credentials == null && ZXCryptPrefs.HasKey(PP_ID_Credentials))
                credentials = ZXCryptPrefs.GetString(PP_ID_Credentials);

            return credentials;
        }
        set
        {
            credentials = value;

            if (value != null)
                ZXCryptPrefs.SetString(PP_ID_Credentials, credentials);
            else
                ZXCryptPrefs.DeleteKey(PP_ID_Credentials);
        }
    }
    private static bool GDRP
    {
        get
        {
            return PlayerPrefs.HasKey(PP_ID_GDRP);
        }
        set
        {
            if (value)
                PlayerPrefs.SetString(PP_ID_GDRP, System.DateTime.Now.ToBinary().ToString());
            else
                PlayerPrefs.DeleteKey(PP_ID_GDRP);
        }
    }


    //Private variables
    private static ZXPermissionManager singleton;

    //Login Variables
    private static LoginResult playFabLogin;
    private static PlayerProfileModel playFabProfile;
    private static UserAccountInfo playFabAccount;

    /* Initalization */
    private void Awake()
    {
        singleton = this;
    }

    /* Login and Authentication Functions */
    private void Login()
    {
        if (Credentials != null)
        {
            Authenticate(true); /*false*/
        }
        else
        {
            Authenticate(true);
        }

        /*
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest();

        request.CustomId = SystemInfo.deviceUniqueIdentifier;
        request.CreateAccount = true;

        PlayFabClientAPI.LoginWithCustomID(request, OnAuthenticate, ZXErrorHandler.WebLoadingErrorHandler);
        */
    }

    private void Logout()
    {
        Credentials = null;
        GDRP = false;
    }

    /// <summary>
    /// Used to authenticate user credentials
    /// </summary>
    private void Authenticate(bool CreateAccount = false)
    {
#if UNITY_ANDROID
        LoginWithAndroidDeviceIDRequest request = new LoginWithAndroidDeviceIDRequest();

        request.AndroidDevice = SystemInfo.deviceModel;
        request.AndroidDeviceId = SystemInfo.deviceUniqueIdentifier;
        request.OS = SystemInfo.operatingSystem;

#elif UNITY_IOS
        LoginWithIOSDeviceIDRequest request = new LoginWithIOSDeviceIDRequest();

        request.DeviceModel = SystemInfo.deviceModel;
        request.DeviceId = SystemInfo.deviceUniqueIdentifier;
        request.OS = SystemInfo.operatingSystem;

#elif UNITY_EDITOR || UNITY_WEBGL
        LoginWithCustomIDRequest request = new LoginWithCustomIDRequest();

        request.CustomId = SystemInfo.deviceUniqueIdentifier;
#endif

        /*Account Creation Logic*/
        request.TitleId = PlayFabSettings.TitleId;
        request.CreateAccount = CreateAccount;

#if UNITY_ANDROID
        PlayFabClientAPI.LoginWithAndroidDeviceID(request, OnAuthenticate, ZXErrorHandler.WebLoadingErrorHandler);

#elif UNITY_IOS
        PlayFabClientAPI.LoginWithIOSDeviceID(request, OnAuthenticate, ZXErrorHandler.WebLoadingErrorHandler);

#elif UNITY_EDITOR || UNITY_WEBGL
        PlayFabClientAPI.LoginWithCustomID(request, OnAuthenticate, ZXErrorHandler.WebLoadingErrorHandler);
#endif
    }

    private void OnAuthenticate(LoginResult Result)
    {
        ZXErrorHandler.ReportMessage(singleton.gameObject, "Logged in as: " + Result.PlayFabId);

        playFabLogin = Result;
        Credentials = Result.PlayFabId;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /*Load PlayFab Account Info*/
    private void LoadPlayFabAccountInfo()
    {
        GetAccountInfoRequest request = new GetAccountInfoRequest();

        request.PlayFabId = playFabLogin.PlayFabId;

        PlayFabClientAPI.GetAccountInfo(request, OnAccountInfoLoaded, ZXErrorHandler.WebLoadingErrorHandler);
    }

    private void OnAccountInfoLoaded(GetAccountInfoResult Result)
    {
        playFabAccount = Result.AccountInfo;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /*Load PlayFab Profile*/
    private void LoadPlayFabProfile()
    {
        GetPlayerProfileRequest request = new GetPlayerProfileRequest();

        request.PlayFabId = playFabLogin.PlayFabId;

        PlayFabClientAPI.GetPlayerProfile(request, OnUserProfileLoaded, ZXErrorHandler.WebLoadingErrorHandler);
    }

    private void OnUserProfileLoaded(GetPlayerProfileResult Result)
    {
        playFabProfile = Result.PlayerProfile;

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }
}
