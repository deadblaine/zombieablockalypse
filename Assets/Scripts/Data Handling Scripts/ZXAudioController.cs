﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using DarkTonic.MasterAudio;

public class ZXAudioController : MonoBehaviour
{
    //Constants
    private const string pp_Master_Key = "MasterVolume";
    private const string pp_SFX_Key = "SFXVolume";
    private const string pp_Music_Key = "MusicVolume";

    //Types
    public enum Channel
    {
        Master = 0,
        SFX = 1,
        Music = 2
    }

    //Inspector Variables
    [SerializeField] private AudioMixer mixer;

    //Accessors
    public static float MasterVolume
    {
        get
        {
            float value = 1;

            if (PlayerPrefs.HasKey(pp_Master_Key))
                value = PlayerPrefs.GetFloat(pp_Master_Key);

            return value;
        }
        set
        {
            PlayerPrefs.SetFloat(pp_Master_Key, value);
            singleton.mixer.SetFloat(pp_Master_Key, value);
        }
    }
    public static float SFXVolume
    {
        get
        {
            float value = 1;

            if (PlayerPrefs.HasKey(pp_SFX_Key))
                value = PlayerPrefs.GetFloat(pp_SFX_Key);

            return value;
        }
        set
        {
            PlayerPrefs.SetFloat(pp_SFX_Key, value);
            singleton.mixer.SetFloat(pp_SFX_Key, value);
        }
    }
    public static float MusicVolume
    {
        get
        {
            float value = 1;

            if (PlayerPrefs.HasKey(pp_Music_Key))
                value = PlayerPrefs.GetFloat(pp_Music_Key);

            return value;
        }
        set
        {
            PlayerPrefs.SetFloat(pp_Music_Key, value);
            singleton.mixer.SetFloat(pp_Music_Key, value);
        }
    }

    //Delegates
    private UnityAction<Scene, LoadSceneMode> d_SceneChanged;

    //Private vraibles
    private static ZXAudioController singleton;
    private PlaySoundResult playing;

	// Use this for initialization
	private void Awake()
    {
        singleton = this;

        d_SceneChanged = new UnityAction<Scene, LoadSceneMode>(SceneChanged);

        mixer.SetFloat(pp_Master_Key, MasterVolume);
        mixer.SetFloat(pp_SFX_Key, SFXVolume);
        mixer.SetFloat(pp_Music_Key, MusicVolume);

        SceneManager.sceneLoaded += d_SceneChanged;
	}

    private void SceneChanged(Scene scene, LoadSceneMode Mode)
    {
        string NextSong = string.Empty;

        switch (scene.buildIndex)
        {
            case 0:
                break;

            case 1:
                NextSong = "Title_Intro_01";
                break;

            case 2:
                NextSong = "Title_Music_01";
                break;

            default:
                NextSong = "Combat_Music_0" + UnityEngine.Random.Range(1, 3 + 1).ToString();
                break;
        }

        if (playing != null)
        {
            playing.ActingVariation.FadeOutNow(2f);
            playing = MasterAudio.PlaySound(NextSong);
        }
        else
        {
            playing = MasterAudio.PlaySound(NextSong);
        }
    }

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= d_SceneChanged;
    }
}
