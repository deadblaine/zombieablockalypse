﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Xml;

public class ZXImageServer : MonoBehaviour, ZXCore.IZXLoadable
{
    //Inspector Variables
    [SerializeField] private TextAsset imagemanifest;
    [SerializeField] private bool Log;

    //IZXLoadable Implimentation
    public event System.Action e_LoadingComplete;

    public static ZXCore.IZXLoadable Loadable
    {
        get
        {
            return singleton;
        }
    }
    public List<System.Action> LoadingList
    {
        get
        {
            return new List<System.Action>(new System.Action[] { LoadImages });
        }
    }

    //Private variables
    private static ZXImageServer singleton;
    [SerializeField] private List<Sprite> sprites;
    //private static Dictionary<string, Sprite> sprites;
    private string imagesFolderPath;

    private void Awake()
    {
        singleton = this;
    }

    public static Sprite FetchSprite(string Key)
    {
        /*
        if (sprites.ContainsKey(Key))
        {
            return sprites[Key];
        }
        else
        {
            return Resources.Load<Sprite>(Path.Combine("Images", Path.GetFileNameWithoutExtension(Key)));
        }
        */

        Sprite sprite = singleton.sprites.Find(delegate (Sprite S) { return S.name + ".png" == Key; });

        if (sprite != null)
        {
            return sprite;
        }
        else
        {
            return Resources.Load<Sprite>(Path.Combine("Images", Path.GetFileNameWithoutExtension(Key)));
        }
    }

    private void LoadImages()
    {
        StartCoroutine(LoadImageFiles());
    }

    private IEnumerator LoadImageFiles()
    {
        /*
        XmlDocument manifest = new XmlDocument();
        string manifestPath;
        XmlNodeList collectionNodes;
        string imageFilePath;
        WWW imageFile;
        WWW manifestFile;

        //File path specific to device
        #if UNITY_ANDROID
        imagesFolderPath = "jar:file://" + Application.dataPath + "!/assets/Images/";
        manifestPath = "jar:file://" + Application.dataPath + "!/assets/ImagesManifest.xml";
        #endif

        #if UNITY_WEBGL
        imagesFolderPath = Path.Combine("https://s3.us-east-2.amazonaws.com/blitzproject.com/StreamingAssets", "Images"); 
        manifestPath = Path.Combine("https://s3.us-east-2.amazonaws.com/blitzproject.com/StreamingAssets", "ImagesManifest.xml");
        #endif

        #if UNITY_EDITOR || UNITY_IOS
        imagesFolderPath = Path.Combine(Application.streamingAssetsPath, "Images");
        manifestPath = Path.Combine(Application.streamingAssetsPath, "ImagesManifest.xml");
        #endif

        //Read file
        //manifestFile = new WWW(manifestPath);

        //Wait until the file is done reading
        //yield return new WaitUntil(delegate { return manifestFile.isDone; });

        manifest.LoadXml(imagemanifest.text);

        //Parse XML Data

        //Manifest Data
        collectionNodes = manifest.DocumentElement.SelectNodes("image");

        sprites = new Dictionary<string, Sprite>(collectionNodes.Count);

        foreach (XmlNode node in collectionNodes)
        {
            imageFilePath = Path.Combine(imagesFolderPath, node.Attributes["file"].Value);

            if (Log)
                Debug.Log("Loading: " + imageFilePath);

            if (!imageFilePath.Contains(".meta")) //Ignore meta files
            {
                imageFile = new WWW(imageFilePath);

                yield return new WaitUntil(delegate { return imageFile.isDone; });

                sprites.Add(Path.GetFileName(imageFilePath), Sprite.Create(imageFile.texture as Texture2D, new Rect(0, 0, imageFile.texture.width, imageFile.texture.height), Vector2.zero));
            }
            else
            {
                ZXErrorHandler.ReportError(singleton.gameObject, "Could not load image: " + imageFilePath);
            }
        }
    */
        yield return new WaitForSeconds(0.01f);

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }
}
