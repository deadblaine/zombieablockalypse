﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;
using TMPro;

public class ZXLocal : MonoBehaviour, ZXCore.IZXLoadable
{
    //Constants
    private const string errStr = "STR 404";
    private const string langKey = "Local_Lang_";
    private const string langVerKey = "Local_Lang_Version";

    //Inspector
    [SerializeField] private TextAsset language;
    [SerializeField] private TMP_FontAsset asciiFont;
    [SerializeField] private TMP_FontAsset cjkFont;

    //Public events
    public static event System.Action LanguageChanged;

    //Public accessors
    public static SystemLanguage Language
    {
        get
        {
            return (SystemLanguage)PlayerPrefs.GetInt(langKey); ;
        }
        set
        {
            //Save new value
            PlayerPrefs.SetInt(langKey, (int)value);

            //Update the font file
            font = LangToFont(Language);
        }
    }
    private static string Version
    {
        get
        {
            return PlayerPrefs.GetString(langVerKey);
        }
        set
        {
            PlayerPrefs.SetString(langVerKey, value);
        }
    }
    public static TMP_FontAsset Font
    {
        get
        {
            return font;
        }
    }

    //IZXLoadable Implimentation
    public event System.Action e_LoadingComplete;

    public static ZXCore.IZXLoadable Loadable
    {
        get
        {
            return singleton;
        }
    }
    public List<System.Action> LoadingList
    {
        get
        {
            return new List<System.Action>(new System.Action[] { LoadDictionary });
        }
    }

    /*Master Languages List*/
    private static List<SystemLanguage> supportedLanguages = new List<SystemLanguage>(new SystemLanguage[] { SystemLanguage.English, SystemLanguage.German, SystemLanguage.French, SystemLanguage.Spanish, SystemLanguage.Portuguese, SystemLanguage.Russian, SystemLanguage.Chinese, SystemLanguage.Japanese, SystemLanguage.Korean });

    //Private variables
    private static ZXLocal singleton;
    private static TMP_FontAsset font;
    private static Dictionary<string, string> dictionary;
    private static List<string> flavor;

    // Use this for initialization
    private void Awake()
    {
        singleton = this;

        if (!PlayerPrefs.HasKey(langKey))
        {
            if (supportedLanguages.Contains(Application.systemLanguage))
            {
                Language = Application.systemLanguage;
            }
            else
                Language = supportedLanguages[0];
        }
    }


    /* Loading Functions */ 
    private void LoadDictionary()
    {
        //Open the XML file
        singleton.StartCoroutine(singleton.OpenXmlFile());
    }

    /// <summary>
    /// Opens the XML file
    /// </summary>
    /// <returns></returns>
    private IEnumerator OpenXmlFile()
    {
        XmlDocument xml = new XmlDocument();
        /*
        string path;

        //File path specific to device
        #if UNITY_ANDROID
        path = "jar:file://" + Application.dataPath + "!/assets/Language.xml";
        #endif

        #if UNITY_WEBGL
        path = Path.Combine("https://s3.us-east-2.amazonaws.com/blitzproject.com/StreamingAssets", "Language.xml");
        #endif

        #if UNITY_EDITOR || UNITY_IOS
        path = Path.Combine(Application.streamingAssetsPath, "Language.xml");
        #endif

        WWW languageFile = new WWW(path);

        yield return new WaitUntil(delegate { return languageFile.isDone; });
        */
        yield return new WaitForSeconds(0.01f);

        xml.LoadXml(language.text);

        //ZXLoadingTextDisplay.ShowText(path);

        Version = xml.DocumentElement.Attributes["version"].Value;

        //Parse dictionary
        XmlNodeList nodes = xml.GetElementsByTagName("entry");

        dictionary = new Dictionary<string, string>(nodes.Count);
        flavor = new List<string>(10);

        foreach (XmlNode node in nodes)
        {
            dictionary.Add(node.Attributes["key"].Value, node.SelectSingleNode(Language.ToString()).InnerText);

            if (node.Attributes["category"].Value == "Flavor")
                flavor.Add(node.SelectSingleNode(Language.ToString()).InnerText);
        }

        //Raise events
        if (LanguageChanged != null)
            LanguageChanged();

        if (e_LoadingComplete != null)
            e_LoadingComplete();
    }

    /*Public Functions */
    public static string LocalText(string Key)
    {
        string returnString;

        if (dictionary.TryGetValue(Key, out returnString))
            return returnString;
        else
        {
            ZXErrorHandler.ReportWarning(singleton.gameObject, Key + " was not found in the dictionary");
            return errStr;
        }
    }

    public static string FlavorText()
    {
        return flavor[Random.Range(0, flavor.Count)];
    }

    /* Support Functions*/
    /// <summary>
    /// Gets the appropriate font based on the selected language
    /// </summary>
    /// <param name="Language"></param>
    /// <returns></returns>
    private static TMP_FontAsset LangToFont(SystemLanguage Language)
    {
        switch (Language)
        {
            case SystemLanguage.Chinese:
                return singleton.cjkFont;
            case SystemLanguage.Japanese:
                return singleton.cjkFont;
            case SystemLanguage.Korean:
                return singleton.cjkFont;
            default:
                return singleton.asciiFont;
        }
    }
}
