﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXMineField : ZXSpell
{
    [SerializeField]
    private ZXExplosive p_explosivePrefab;
    [SerializeField]
    private int p_MortarsSize;
    private ZXPoolLite<ZXProjectile> p_Explosives;

    [SerializeField]
    private float dropHeight;
    [SerializeField]
    private float hitRadius;
    [SerializeField]
    private float spawnSpeed;
    [SerializeField]
    private float timeOnTarget;

    protected override void Awake()
    {
        base.Awake();

        p_Explosives = new ZXPoolLite<ZXProjectile>(ZXCombatUI.ObjectMask, p_explosivePrefab, p_MortarsSize);
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        base.Place(location, rotation, args);

        if (args is bool && (bool)args)
        {
            StartCoroutine(PlaceMortars(location));
        }
    }

    private IEnumerator PlaceMortars(Vector3 location)
    {
        Vector3 currentLocation;
        Vector3 origin;
        ZXExplosive currentExplosive;

        origin = ZXReflectingPool.FindClosest<ZXInterestPoint>(transform, delegate (ZXInterestPoint point) { return point.Race == ToonRace.Human; }).transform.position;
        origin.y = dropHeight;

        for (int i = 0; i < p_MortarsSize; i++)
        {
            currentLocation = location + new Vector3(Random.Range(-hitRadius, hitRadius), 0f, Random.Range(-hitRadius, hitRadius));

            currentExplosive = p_Explosives.Request() as ZXExplosive;

            if (currentExplosive != null)
            {
                currentExplosive.Place(origin, Quaternion.identity, null);
                currentExplosive.Arm();
                currentExplosive.Launch(currentLocation, timeOnTarget);
            }

            yield return new WaitForSeconds(spawnSpeed);
        }

        yield return null;
    }

    public override void AdjustValues(int index, int level)
    {
    }
}
