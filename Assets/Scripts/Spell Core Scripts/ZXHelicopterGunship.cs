﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXHelicopterGunship : ZXSpell
{
    //Pools
    [SerializeField] private float deployTime;
    [SerializeField] private ZXParticleEffect p_20mmTracerPrefab;

    [SoundGroupAttribute][SerializeField] protected string s_MinigunShot;
    [SoundGroupAttribute][SerializeField] protected string s_MinigunRotation;
    [SoundGroupAttribute][SerializeField] protected string s_HelicopterRotars;

    private ZXPoolLite<ZXParticleEffect> p_20mmTracers;
    private int p_20mmTracersSize = 10;

    //Firing variables
    [SerializeField] private float Speed;
    [SerializeField] private float Height;
    [SerializeField] private float Distance;
    [SerializeField] private float Duration;

    [SerializeField] private float rateOfFire20mm;
    [SerializeField] private int volleySize20mm;
    [SerializeField] private int tracerFreq20mm;
    [SerializeField] private int volleyRange20mm;

    [SerializeField] private LayerMask mask;

    //Private variables
    private new Transform transform;
    private bool rotate = false;
    private float timer = 0f;
    private ZXHelicopterOrigin helicopterOrigin;
    private ZXHelicopterTarget helicopterTarget;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        transform = GetComponent<Transform>();

        helicopterOrigin = GetComponentInChildren<ZXHelicopterOrigin>();
        helicopterTarget = GetComponentInChildren<ZXHelicopterTarget>();

        p_20mmTracers = new ZXPoolLite<ZXParticleEffect>(transform, p_20mmTracerPrefab, p_20mmTracersSize);
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        base.Place(location, Quaternion.LookRotation(new Vector3(-1f, 0f, 0f), Vector3.up), args);

        if (args is bool && (bool)args)
        {
            ZXHeroSpawn spawn = ZXReflectingPool.FindClosest<ZXHeroSpawn>(transform);

            if (spawn == null)
                spawn = GameObject.Find("HeroSpawn").GetComponent<ZXHeroSpawn>();

            //Set location and rotation
            helicopterTarget.transform.position = new Vector3(spawn.transform.position.x, transform.position.y, transform.position.z);
            helicopterOrigin.transform.localPosition = new Vector3(0f, Height, -Distance);
            
            //Start playing rotar sound
            MasterAudio.PlaySound3DAtTransform(s_HelicopterRotars, helicopterOrigin.transform);

            StartCoroutine(Fire20mmGun());
        }
    }

    private void Update()
    {
        if (rotate)
        {
            timer += Time.deltaTime;

            helicopterTarget.transform.Translate(-Speed * Time.deltaTime, 0f, 0f, Space.World);

            if (timer > Duration)
                rotate = false;
        }
    }

    private IEnumerator Fire20mmGun()
    {
        Vector3 targetPosition;
        Vector3 firePosition;
        RaycastHit[] hits;
        List<IZXDamageable> damageables;

        damageables = new List<IZXDamageable>(10);

        yield return new WaitForSeconds(deployTime);

        rotate = true;

        if (image != null)
            image.FadeOutPartial();

        //Play minigun rotation buzz sound
        MasterAudio.PlaySound3DAtTransformAndForget(s_MinigunRotation, helicopterOrigin.transform);

        while (timer < Duration)
        {
            ZXParticleEffect tracer;

            for (int i = 0; i < volleySize20mm; i++)
            {
                targetPosition = helicopterTarget.transform.position;

                damageables.Clear();

                //Play fire sound
                MasterAudio.PlaySound3DAtTransformAndForget(s_MinigunShot, transform);

                firePosition = targetPosition + new Vector3(Random.Range(-volleyRange20mm, volleyRange20mm), 0f, Random.Range(-volleyRange20mm, volleyRange20mm));

                hits = Physics.SphereCastAll(helicopterOrigin.transform.position, 1.5f, (firePosition - helicopterOrigin.transform.position).normalized, 150f, mask);

                if (i % tracerFreq20mm == 0)
                {
                    tracer = p_20mmTracers.Request();

                    if (tracer != null)
                    {
                        tracer.Place(helicopterOrigin.transform.position, Quaternion.LookRotation((firePosition - helicopterOrigin.transform.position).normalized), null);
                    }
                }

                if (hits != null)
                {
                    IZXDamageable damageable;
                    ZXParticleEffect effect;

                    foreach (RaycastHit hit in hits)
                    {
                        damageable = hit.transform.GetComponentInParent<IZXDamageable>();

                        if (damageable != null)
                        {
                            damageables.Add(damageable);

                            effect = ZXCombatUI.p_LargeBulletImpact.Request();

                            if (effect != null)
                            {
                                effect.Place(hit.point, Quaternion.LookRotation(hit.normal), null);
                            }
                        }
                    }
                }

                if (damageables != null && damageables.Count > 0)
                {
                    damageables.RemoveAll(d_DamageFilter);
                }

                if (damageables != null && damageables.Count > 0)
                {
                    ZXDamage damage;

                    for (int x = 0; x < damageables.Count; x++)
                    {
                        damage = new ZXDamage(600, 0.5f, 0.5f, 0f, ToonRace.Human, ZXDamage.Type.Projectile, new Ray(helicopterOrigin.transform.position, (firePosition - helicopterOrigin.transform.position).normalized), ZXDamage.SpecialEffect.None);

                        if (damageables[x] != null)
                        {
                            damageables[x].TakeDamage(damage);
                        }
                    }
                }

                yield return new WaitForSeconds(rateOfFire20mm);
            }
        }

        if (image != null)
            image.FadeOut();

        yield return new WaitForSeconds(ZXSpellTargetingImage._fadeOutTime);

        Disable();
    }

    public override void AdjustValues(int Index, int Level)
    {

    }
}
