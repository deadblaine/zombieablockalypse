﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXHowitzer : ZXSpell
{
    [SoundGroupAttribute] [SerializeField] protected string s_Shot;

    [SerializeField] private ZXExplosive explosivePrefab;
    private ZXExplosive explosive;

    [SerializeField] private float dropHeight;
    [SerializeField] private float hitRadius;
    [SerializeField] private float timeOnTarget;

    protected override void Awake()
    {
        base.Awake();

        explosive = Instantiate<ZXExplosive>(explosivePrefab);
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        base.Place(location, rotation, args);

        if (args is bool && (bool)args)
        {
            StartCoroutine(PlaceMortar(location));

            MasterAudio.PlaySound3DAtTransformAndForget(s_Spawn, transform);
        }
    }

    private IEnumerator PlaceMortar(Vector3 location)
    {
        Vector3 currentLocation;
        Vector3 origin;

        ZXMapExit exit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit obj) { return obj.Race == ToonRace.Human; });

        if (exit == null)
            exit = GameObject.Find("MapExit").GetComponent<ZXMapExit>();

        origin = exit.transform.position;
        origin.y = dropHeight;

        origin += new Vector3(50, 0, 0);

        currentLocation = location + new Vector3(Random.Range(-hitRadius, hitRadius), 0f, Random.Range(-hitRadius, hitRadius));

        if (image != null)
            image.FadeOutPartial();

        if (explosive != null)
        {
            explosive.Place(origin, Quaternion.identity, null);
            explosive.Arm();
            explosive.Launch(currentLocation, timeOnTarget);
        }

        MasterAudio.PlaySound3DAtTransformAndForget(s_Shot, transform);

        yield return new WaitForSeconds(timeOnTarget);

        if (image != null)
            image.FadeOut();

        yield return new WaitForSeconds(ZXSpellTargetingImage._fadeOutTime);

        Disable();
    }

    public override void AdjustValues(int Index, int Level)
    {
    }
}
