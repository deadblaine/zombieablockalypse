﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using UnityEngine.UI;
using DarkTonic.MasterAudio;

public abstract class ZXSpell : MonoBehaviour, IZXTracked<ZXSpell>
{
    [SoundGroupAttribute][SerializeField] protected string s_Spawn;

    public event System.Action<ZXSpell> e_OnDisable;
    public event System.Action<ZXSpell> e_OnDestroy;

    protected System.Predicate<IZXDamageable> d_DamageFilter;

    protected ZXSpellTargetingImage image;
    protected RectTransform canvasRect;

    protected virtual void Awake()
    {
        image = GetComponentInChildren<ZXSpellTargetingImage>();
        canvasRect = GetComponent<RectTransform>();

        d_DamageFilter = new System.Predicate<IZXDamageable>(DamageablesFilter);
    }

    public virtual void Place (Vector3 location, Quaternion rotation, object args)
	{
		transform.position = location;
        transform.rotation = rotation;

        if (!gameObject.activeSelf)
            gameObject.SetActive(true);

        if (image != null)
            image.FadeIn();

        if (args is bool && (bool)args)
        {
            MasterAudio.PlaySound3DAtTransformAndForget(s_Spawn, transform);

            if (image != null)
                image.Pulse();
        }
    }

    public abstract void AdjustValues(int index, int level);

    protected virtual void Disable()
    {
        MasterAudio.StopAllSoundsOfTransform(transform);

        gameObject.SetActive(false);
    }

    protected bool DamageablesFilter(IZXDamageable Damageable)
    {
        if (Damageable != null)
            return Damageable.Race == ToonRace.Human;
        else
            return false;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        CancelInvoke();

        if (e_OnDisable != null)
            e_OnDisable(this);
    }

    private void OnDestroy()
    {
        if (e_OnDestroy != null)
            e_OnDestroy(this);
    }
}
