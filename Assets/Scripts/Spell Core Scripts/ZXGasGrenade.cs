﻿using UnityEngine;
using System.Collections;
using ZXCore;
using DarkTonic.MasterAudio;


public class ZXGasGrenade : ZXSpell
{
    [SoundGroupAttribute] [SerializeField] protected string s_Shot;

    [SerializeField] private ZXExplosive p_explosivePrefab;
	[SerializeField] private int p_MortarsSize;
	private ZXPoolLite<ZXProjectile> p_Explosives;

	[SerializeField] private float dropHeight;
	[SerializeField] private float hitRadius;
	[SerializeField] private float spawnSpeed;
	[SerializeField] private float timeOnTarget;

    protected override void Awake()
    {
        base.Awake();

        p_Explosives = new ZXPoolLite<ZXProjectile>(ZXCombatUI.ObjectMask, p_explosivePrefab, p_MortarsSize);
	}

	public override void Place(Vector3 location, Quaternion rotation, object args)
	{
		base.Place(location, rotation, args);

		if (args is bool && (bool)args)
		{
			StartCoroutine(PlaceMortars(location));
		}
	}

	private IEnumerator PlaceMortars(Vector3 location)
	{
		Vector3 currentLocation;
		Vector3 origin;
		ZXExplosive currentExplosive;

        ZXMapExit exit = ZXReflectingPool.FindClosest<ZXMapExit>(transform, delegate (ZXMapExit obj) { return obj.Race == ToonRace.Human; });

        if (exit == null)
            exit = GameObject.Find("MapExit").GetComponent<ZXMapExit>();

        origin = exit.transform.position;
        origin.y = dropHeight;

        if (image != null)
            image.FadeOutPartial();

        for (int i = 0; i < p_MortarsSize; i++)
		{
			currentLocation = location + new Vector3(Random.Range(-hitRadius, hitRadius), 0f, Random.Range(-hitRadius, hitRadius));

			currentExplosive = p_Explosives.Request() as ZXExplosive;

			if (currentExplosive != null)
			{
				currentExplosive.Place(origin, Quaternion.identity, null);
				currentExplosive.Arm();
				currentExplosive.Launch(currentLocation, timeOnTarget);
			}

            MasterAudio.PlaySound3DAtTransformAndForget(s_Shot, transform);

            yield return new WaitForSeconds(spawnSpeed);
		}

        yield return new WaitForSeconds(timeOnTarget);

        if (image != null)
            image.FadeOut();

        yield return new WaitForSeconds(ZXSpellTargetingImage._fadeOutTime);

        Disable();
    }

    public override void AdjustValues(int index, int level)
    {
    }
}
