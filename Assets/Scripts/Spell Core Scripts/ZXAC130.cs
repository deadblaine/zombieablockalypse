﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using UnityEngine.UI;
using DarkTonic.MasterAudio;

public class ZXAC130 : ZXSpell
{
    //Pools
    [SerializeField] private float deployTime;
    [SerializeField] private ZXParticleEffect p_20mmTracerPrefab;
    [SerializeField] private ZXExplosive p_40mmPrefab;
    [SerializeField] private ZXExplosive p_105mmPrefab;

    [SoundGroupAttribute] [SerializeField] protected string s_AC130EngineSound;
    [SoundGroupAttribute] [SerializeField] protected string s_20mmRotation;
    [SoundGroupAttribute] [SerializeField] protected string s_20mmShot;
    [SoundGroupAttribute] [SerializeField] protected string s_40mmShot;
    [SoundGroupAttribute] [SerializeField] protected string s_105mmShot;

    private ZXPoolLite<ZXParticleEffect> p_20mmTracers;
    private ZXPoolLite<ZXProjectile> p_40mmExplosives;
    private ZXPoolLite<ZXProjectile> p_105mmExplosives;

    private int p_20mmTracersSize = 10;

    //Firing variables
    [SerializeField] private float rotateSpeed;
    [SerializeField] private float orbitHeight;
    [SerializeField] private float orbitDistance;
    [SerializeField] private float overwatchArea;
    [SerializeField] private float overwatchDuration;
    [SerializeField] private float timeOnTarget;

    [SerializeField] private float rateOfFire20mm;
    [SerializeField] private float rateOfFire40mm;
    [SerializeField] private float rateOfFire105mm;

    [SerializeField] private int volleySize20mm;
    [SerializeField] private int tracerFreq20mm;
    [SerializeField] private int volleyRange20mm;
    [SerializeField] private float volleyDelay20mm;

    [SerializeField] private LayerMask mask;

    //Private variables
    private new Transform transform;
    private bool rotate = false;
    private float overwatchTimer = 0f;
    private ZXAC130Origin ac130Origin;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        transform = GetComponent<Transform>();

        ac130Origin = GetComponentInChildren<ZXAC130Origin>();

        p_20mmTracers = new ZXPoolLite<ZXParticleEffect>(transform, p_20mmTracerPrefab, p_20mmTracersSize);
        p_40mmExplosives = new ZXPoolLite<ZXProjectile>(transform, p_40mmPrefab, Mathf.RoundToInt(overwatchDuration / rateOfFire40mm));
        p_105mmExplosives = new ZXPoolLite<ZXProjectile>(transform, p_105mmPrefab, Mathf.RoundToInt(overwatchDuration / rateOfFire105mm));
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        base.Place(location, Quaternion.LookRotation(new Vector3(-1f, 0f, 0f), Vector3.up), args);

        if (args is bool && (bool)args)
        {
            //Set location and rotation
            ac130Origin.transform.localPosition = new Vector3(0f, orbitHeight, -orbitDistance);

            MasterAudio.PlaySound3DAtTransform(s_AC130EngineSound, ac130Origin.transform);

            StartCoroutine(Disabler());
        }
    }

    private void Update()
    {   
        if (rotate)
        {
            overwatchTimer += Time.deltaTime;

            transform.RotateAround(transform.position, Vector3.up, rotateSpeed * Time.deltaTime);

            if (overwatchTimer > overwatchDuration)
            {
                rotate = false;
            }
        }
    }

    private IEnumerator Disabler()
    {
        yield return new WaitForSeconds(deployTime);

        overwatchTimer = 0f;
        rotate = true;

        if (image != null)
            image.FadeOutPartial();

        StartCoroutine(Fire20mmGun());
        StartCoroutine(Fire40mmGun());
        //StartCoroutine(Fire105mmGun());

        yield return new WaitForSeconds(overwatchDuration);

        if (image != null)
            image.FadeOut();

        MasterAudio.FadeOutSoundGroupOfTransform(ac130Origin.transform, s_AC130EngineSound, ZXSpellTargetingImage._fadeOutTime);

        MasterAudio.StopAllSoundsOfTransform(ac130Origin.transform);

        Disable();
    }

    private IEnumerator Fire20mmGun()
    {
        List<ZXToon> toons;
        ZXToon target;
        Vector3 targetPosition;
        Vector3 firePosition;
        RaycastHit[] hits;
        List<IZXDamageable> damageables;

        damageables = new List<IZXDamageable>(10);

        while (overwatchTimer < overwatchDuration)
        {
            yield return new WaitForSeconds(volleyDelay20mm);

            toons = ZXReflectingPool.FindInSphere(transform.position, overwatchArea, delegate (ZXToon toon) { return toon.Race != ToonRace.Human; });

            if (toons != null)
            {
                ZXParticleEffect tracer;

                target = toons[Random.Range(0, toons.Count)];
                targetPosition = target.LookAtTransform.position;

                MasterAudio.PlaySound3DAtTransformAndForget(s_20mmRotation, ac130Origin.transform);

                for (int i = 0; i < volleySize20mm; i++)
                {
                    MasterAudio.PlaySound3DAtTransformAndForget(s_20mmShot, ac130Origin.transform);

                    damageables.Clear();

                    firePosition = targetPosition + new Vector3(Random.Range(-volleyRange20mm, volleyRange20mm), 0f, Random.Range(-volleyRange20mm, volleyRange20mm));

                    hits = Physics.SphereCastAll(ac130Origin.transform.position, 0.5f, (firePosition - ac130Origin.transform.position).normalized, 75f, mask);

                    if (i % tracerFreq20mm == 0)
                    {
                        tracer = p_20mmTracers.Request();

                        if (tracer != null)
                        {
                            tracer.Place(ac130Origin.transform.position, Quaternion.LookRotation((firePosition - ac130Origin.transform.position).normalized), null);
                        }
                    }

                    if (hits != null)
                    {
                        IZXDamageable damageable;
                        ZXParticleEffect effect;

                        foreach (RaycastHit hit in hits)
                        {
                            damageable = hit.transform.GetComponentInParent<IZXDamageable>();

                            if (damageable != null)
                            {
                                damageables.Add(damageable);

                                effect = ZXCombatUI.p_LargeBulletImpact.Request();

                                if (effect != null)
                                {
                                    effect.Place(hit.point, Quaternion.LookRotation(hit.normal), null);
                                }
                            }
                        }
                    }

                    if (damageables != null && damageables.Count > 0)
                    {
                        damageables.RemoveAll(d_DamageFilter);
                    }

                    if (damageables != null && damageables.Count > 0)
                    {
                        ZXDamage damage;

                        for (int x = 0; x < damageables.Count; x++)
                        {
                            damage = new ZXDamage(600, 0.5f, 1f, 0.5f, ToonRace.Human, ZXDamage.Type.Projectile, new Ray(ac130Origin.transform.position, (firePosition - ac130Origin.transform.position).normalized), ZXDamage.SpecialEffect.None);

                            if (damageables[x] != null)
                            {
                                damageables[x].TakeDamage(damage);
                            }
                        }
                    }

                    yield return new WaitForSeconds(rateOfFire20mm);
                }

                MasterAudio.FadeOutSoundGroupOfTransform(transform, s_20mmRotation, volleyDelay20mm);
            }
        }

        yield return null;
    }

    private IEnumerator Fire40mmGun()
    {
        ZXExplosive current40mm;
        List<ZXToon> toons;
        ZXToon target;

        for (int i = 0; i < p_40mmExplosives.count; i++)
        {
            yield return new WaitForSeconds(rateOfFire40mm);

            toons = ZXReflectingPool.FindInSphere(transform.position, overwatchArea, delegate (ZXToon toon) { return toon.Race != ToonRace.Human; });

            if (toons != null)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_40mmShot, ac130Origin.transform);

                target = toons[Random.Range(0, toons.Count)];

                current40mm = p_40mmExplosives.Request() as ZXExplosive;

                if (current40mm != null)
                {
                    current40mm.Place(ac130Origin.transform.position, ac130Origin.transform.rotation, null);
                    current40mm.Arm();
                    current40mm.Launch(target.transform.position, timeOnTarget);
                }
            }
        }

        yield return null;
    }

    private IEnumerator Fire105mmGun()
    {
        ZXExplosive current105mm;
        List<ZXToon> toons;
        ZXToon target;

        for (int i = 0; i < p_105mmExplosives.count; i++)
        {
            yield return new WaitForSeconds(rateOfFire105mm);

            toons = ZXReflectingPool.FindInSphere(transform.position, overwatchArea, delegate (ZXToon toon) { return toon.Race != ToonRace.Human; });

            if (toons != null)
            {
                MasterAudio.PlaySound3DAtTransformAndForget(s_105mmShot, ac130Origin.transform);

                target = toons[Random.Range(0, toons.Count)];

                current105mm = p_40mmExplosives.Request() as ZXExplosive;

                if (current105mm != null)
                {
                    current105mm.Place(ac130Origin.transform.position, ac130Origin.transform.rotation, null);
                    current105mm.Arm();
                    current105mm.Launch(target.transform.position, timeOnTarget);
                }
            }
        }

        yield return null;
    }

    public override void AdjustValues(int Index, int Level)
    {

    }
}
