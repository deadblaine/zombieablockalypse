﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class ZXDisplayMessage : MonoBehaviour
{
    //Constants
    private const float messageOpenLength = 0.75f;
    private const float messageDisplayLength = 3.5f;
    private const float messageCloseLength = 0.5f;
    private const float messagePulseLength = 2f;
    private const string messagePrimaryID = "primaryMessageDisplayID";
    private const string messageSecondaryID = "secondaryMessageDisplayID";

    //Types
    public enum MessageType
    {
        Flash = 0,
        WaitForUserInput = 1,
        WaitForUserTap = 2
    }
    public enum MessageInstruction
    {
        Open = 0,
        WaitForTime = 1,
        WaitForInput = 2,
        WaitForTap = 3,
        Close = 4,
    }

    public class Message
    {
        //Private variables
        private MessageType msgType;
        private MessageInstruction insType;
        private string message;

        //Accessors
        public MessageType messageType
        {
            get
            {
                return msgType;
            }
        }
        public MessageInstruction instructionType
        {
            get
            {
                return insType;
            }
        }
        public string messageText
        {
            get
            {
                return message;
            }
        }

        //Constructor
        public Message(MessageType messageType, MessageInstruction instructionType, string message)
        {
            this.msgType = messageType;
            this.insType = instructionType;
            this.message = message;
        }
    }

    //Static members
    private static ZXDisplayMessage singleton;
    private System.Action d_completeCallback;
    private System.Action<bool> d_InputListener;
    private static Queue<Message> messageQueue = new Queue<Message>();
    private static bool displayingMessage = false;
    private static bool waitForTouch = false;

    private TextMeshProUGUI primaryDisplay;
    private TextMeshProUGUI secondaryDisplay;

    //Initalization
    private void Awake()
    {
        singleton = this;

        //Get important displays
        primaryDisplay = transform.Find("InstructionsText").GetComponent<TextMeshProUGUI>();
        secondaryDisplay = transform.Find("TimerText").GetComponent<TextMeshProUGUI>();

        //Initalize delegates
        d_completeCallback = new System.Action(MessageComplete);
        d_InputListener = new System.Action<bool>(InputListener);
    }

    private void Update()
    {
        if (waitForTouch && Input.touchCount > 0 || waitForTouch && Input.GetMouseButtonDown(0))
        {
            waitForTouch = false;
            MessageComplete();
        }
    }

    public static void DisplayMessage(MessageType type, string newMessage)
    {
        if (!singleton.gameObject.activeSelf)
            singleton.gameObject.SetActive(true);

        singleton.StartMessage(type, newMessage);
    }

    public void StartMessage(MessageType type, string newMessage)
    {
        switch (type)
        {
            case MessageType.Flash:
                messageQueue.Enqueue(new Message(type, MessageInstruction.Open, newMessage));
                messageQueue.Enqueue(new Message(type, MessageInstruction.WaitForTime, null));
                messageQueue.Enqueue(new Message(type, MessageInstruction.Close, null));
                break;

            case MessageType.WaitForUserInput:
                messageQueue.Enqueue(new Message(type, MessageInstruction.Open, newMessage));
                messageQueue.Enqueue(new Message(type, MessageInstruction.WaitForInput, null));
                messageQueue.Enqueue(new Message(type, MessageInstruction.Close, null));
                break;

            case MessageType.WaitForUserTap:
                messageQueue.Enqueue(new Message(type, MessageInstruction.Open, newMessage));
                messageQueue.Enqueue(new Message(type, MessageInstruction.WaitForTap, null));
                messageQueue.Enqueue(new Message(type, MessageInstruction.Close, null));
                break;
        }

        singleton.CheckForNextMessage();
    }

    private void CheckForNextMessage()
    {
        if (!displayingMessage && messageQueue.Count > 0)
        {
            StartNextMessage(messageQueue.Dequeue());
        }
    }

    private void StartNextMessage(Message nextMessage)
    {
        Sequence primaryDisplayAnimation;
        Sequence secondaryDisplayAnimation;

        //Kill previous tweens
        if (DOTween.IsTweening(messagePrimaryID))
            DOTween.Kill(messagePrimaryID, false);

        //Create new animation sequence
        primaryDisplayAnimation = DOTween.Sequence();
        primaryDisplayAnimation.SetId(messagePrimaryID);
        primaryDisplayAnimation.SetUpdate(true);

        displayingMessage = true;

        //Update text display
        if (nextMessage.messageText != null)
            primaryDisplay.text = nextMessage.messageText;

        //Build tween
        switch (nextMessage.instructionType)
        {
            case MessageInstruction.Open:
                primaryDisplay.rectTransform.localScale = Vector3.zero;

                primaryDisplayAnimation.Append(primaryDisplay.rectTransform.DOScale(Vector3.one, messageOpenLength).SetEase(Ease.OutBack));
                primaryDisplayAnimation.AppendCallback(MessageComplete);
                break;

            case MessageInstruction.WaitForTime:
                primaryDisplayAnimation.AppendInterval(messageDisplayLength);
                primaryDisplayAnimation.AppendCallback(MessageComplete);
                break;

            case MessageInstruction.WaitForTap:
                primaryDisplayAnimation.Append(primaryDisplay.rectTransform.DOScale(new Vector3(1.05f, 1.05f, 1.05f), messagePulseLength));
                primaryDisplayAnimation.Append(primaryDisplay.rectTransform.DOScale(new Vector3(1f, 1f, 1f), messagePulseLength));
                primaryDisplayAnimation.SetLoops(-1);
                WaitForTap();
                break;

            case MessageInstruction.WaitForInput:
                primaryDisplayAnimation.Append(primaryDisplay.rectTransform.DOScale(new Vector3(1.05f, 1.05f, 1.05f), messagePulseLength));
                primaryDisplayAnimation.Append(primaryDisplay.rectTransform.DOScale(new Vector3(1f, 1f, 1f), messagePulseLength));
                primaryDisplayAnimation.SetLoops(-1);
                StartListening();
                break;

            case MessageInstruction.Close:
                primaryDisplayAnimation.Append(primaryDisplay.rectTransform.DOScale(Vector3.zero, messageCloseLength).SetEase(Ease.OutQuad));
                primaryDisplayAnimation.AppendCallback(MessageComplete);
                break;
        }

        if (nextMessage.messageType == MessageType.WaitForUserTap)
        {
            if (nextMessage.instructionType == MessageInstruction.Open || nextMessage.instructionType == MessageInstruction.Close)
            {
                if (DOTween.IsTweening(messageSecondaryID))
                    DOTween.Kill(messageSecondaryID, false);

                secondaryDisplayAnimation = DOTween.Sequence();
                secondaryDisplayAnimation.SetId(messageSecondaryID);
                secondaryDisplayAnimation.SetUpdate(true);

                switch (nextMessage.instructionType)
                {
                    case MessageInstruction.Open:
                        secondaryDisplay.rectTransform.localScale = Vector3.zero;
                        secondaryDisplay.text = ZXLocal.LocalText("genericTapToContinue");

                        secondaryDisplayAnimation.Append(secondaryDisplay.rectTransform.DOScale(Vector3.one, messageOpenLength).SetEase(Ease.OutBack));
                        break;

                    case MessageInstruction.Close:
                        secondaryDisplayAnimation.Append(secondaryDisplay.rectTransform.DOScale(Vector3.zero, messageCloseLength).SetEase(Ease.OutQuad));
                        secondaryDisplayAnimation.AppendCallback(delegate () { secondaryDisplay.text = string.Empty; });
                        break;
                }
            }
        }
    }

    private void StartListening()
    {
        ZXGroundEvents.e_HeroPathEnded += d_InputListener;
        ZXCombatCard.e_DragEnded += d_InputListener;
    }

    private void WaitForTap()
    {
        waitForTouch = true;
    }

    private void InputListener(bool complete)
    {
        if (complete)
        {
            ZXGroundEvents.e_HeroPathEnded -= d_InputListener;
            ZXCombatCard.e_DragEnded -= d_InputListener;

            MessageComplete();
        }
    }

    private void MessageComplete()
    {
        displayingMessage = false;

        CheckForNextMessage();
    }
}
