﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ZXInputListener : MonoBehaviour
{
    private bool AutoRotationOn = false;
    private bool InCombat = false;
    private UnityAction<Scene, LoadSceneMode> d_SceneChanged;

    private void Awake()
    {
        d_SceneChanged = new UnityAction<Scene, LoadSceneMode>(SceneChanged);

        SceneManager.sceneLoaded += d_SceneChanged;
    }

    private void SceneChanged(Scene NewScene, LoadSceneMode Mode)
    {
        InCombat = NewScene.buildIndex > (int)ZXDataManager.Scenes.Main;

        //Configure EventSystems
        EventSystem.current.pixelDragThreshold = Mathf.Max(40, (int)(Screen.dpi / 8f));
    }

    private void OnApplicationFocus(bool Focus)
    {
        if (Focus)
            DetectAutoRotation();
    }

    private void DetectAutoRotation()
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        using (var actClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            var context = actClass.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaClass systemGlobal = new AndroidJavaClass("android.provider.Settings$System");
            var rotationOn = systemGlobal.CallStatic<int>("getInt", context.Call<AndroidJavaObject>("getContentResolver"), "accelerometer_rotation");

            AutoRotationOn = rotationOn == 1;
        }
        #else
        AutoRotationOn = true;
        #endif

        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = AutoRotationOn;
        Screen.autorotateToLandscapeRight = AutoRotationOn;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    private void Update ()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (InCombat)
            {
                if (ZXCombatUI.CurrentState == ZXCombatUI.State.Combat)
                    ZXCombatUI.ChangeMissionState(ZXCombatUI.State.Paused);
            }
            else
            {
                if (ZXCore.ZXPopup.PopupShowing)
                    ZXCore.ZXPopup.BackButtonTap();
                else
                    Application.Quit();
            }
        }
	}

    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= d_SceneChanged;
    }
}
