﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXMainContent : MonoBehaviour, ZXCore.IZXContent
{
    //Private variables
    private RectTransform rect;
    private List<ZXCore.ZXSection> sections;
    [SerializeField] private ZXMainScrollview scrollview;
    private float width;

    protected virtual void Awake()
    {
        rect = GetComponent<RectTransform>();
        sections = new List<ZXCore.ZXSection>(GetComponentsInChildren<ZXCore.ZXSection>());
    }

    private void Start()
    {
        width = scrollview.viewport.rect.width;

        rect.sizeDelta = new Vector2(sections.Count * width, rect.sizeDelta.y);

        foreach (ZXCore.ZXSection section in sections)
        {
            section.Width = width;
            section.Position = (width / 2) + ((int)section.Section * width);
        }
    }

    //Activation functions
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
