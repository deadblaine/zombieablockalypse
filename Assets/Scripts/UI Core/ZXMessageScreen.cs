﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using DG.Tweening;

public class ZXMessageScreen : MonoBehaviour, IPointerClickHandler
{
    //Constants
    private const float defaultDuration = 3f;

    //Static variables
    private static TextMeshProUGUI text; 
    private static ZXMessageScreen singleton;
    private static Image image;

    private static bool handShowing = false;

    [SerializeField] private RectTransform hand;

    private void Awake()
    {
        singleton = this;
        image = GetComponent<Image>();
        text = GetComponentInChildren<TextMeshProUGUI>();

        gameObject.SetActive(false);
	}

    public static void ShowMessage(string Message, float Duration = defaultDuration)
    {
        text.text = Message;

        singleton.gameObject.SetActive(true);
        text.gameObject.SetActive(true);

        singleton.StartCoroutine(singleton.MessageSequence(Duration));
    }

    public static void ShowHandPosition(Vector2 Position)
    {
        singleton.gameObject.SetActive(true);

        singleton.hand.localScale = Vector3.zero;
        singleton.hand.anchoredPosition = Position;
        singleton.hand.DOScale(1f, 0.2f);

        handShowing = true;

        //image.raycastTarget = true;
    }

    public static void HideHand()
    {
        singleton.hand.anchoredPosition = new Vector2(0, -2000);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        HideHand();

        //image.raycastTarget = false;

        gameObject.SetActive(false);
    }

    protected IEnumerator MessageSequence(float Duration)
    {
        yield return new WaitForSeconds(Duration);

        text.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
