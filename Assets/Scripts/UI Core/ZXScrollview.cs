﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using ZXCore;
using DG.Tweening;

[RequireComponent(typeof(ZXSection))]
public class ZXScrollview : ScrollRect
{
    //Constants
    public string scrollTweenID;

    //Types
    public struct Bookmark
    {
        public string Name;
        public Vector2 Location;
    }

    //Accessors
    public ZXSection Section
    {
        get
        {
            return section;
        }
    }
    public bool Tweening
    {
        get
        {
            return DOTween.IsTweening(scrollTweenID) || tweening;
        }
        set
        {
            tweening = value;

            if (e_Tweening != null)
                e_Tweening(value);
        }
    }

    //Events
    public static event System.Action<bool> e_Tweening;

    //Private variables
    protected ZXMainScrollview master;
    private ZXSection section;
    private bool route = false;
    private static bool tweening = false;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        scrollTweenID = "tw_" + gameObject.name + "_scrollview_tween";

        section = GetComponent<ZXSection>();
    }

    protected override void Start()
    {
        base.Start();

        master = GetComponentInParent<ZXMainScrollview>();

        master.Register(section.Section, this);
    }

    //Event handlers
    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
        master.OnInitializePotentialDrag(eventData);
        base.OnInitializePotentialDrag(eventData);
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        //Determines if the drag is horizontal or verticle
        route = Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y);

        if (route)
            master.OnBeginDrag(eventData);
        else
            base.OnBeginDrag(eventData);
    }

    public override void OnDrag(PointerEventData eventData)
    {
        if (route)
            master.OnDrag(eventData);
        else
            base.OnDrag(eventData);
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        if (route)
            master.OnEndDrag(eventData);
        else
            base.OnEndDrag(eventData);
    }

    public void ScrollToStart()
    {
        this.DOVerticalNormalizedPos(1, 0.5f).SetEase(Ease.OutCirc).SetId(scrollTweenID).OnComplete(delegate () { Tweening = false; });
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (master != null)
            master.DeRegister(section.Section);
    }
}
