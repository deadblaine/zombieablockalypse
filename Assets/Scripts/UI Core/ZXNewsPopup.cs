﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXNewsPopup : ZXCore.ZXPopup
{
    private static ZXNewsPopup singleton;

    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
    }

    public static void ShowNews()
    {
        singleton.Show();
    }
}
