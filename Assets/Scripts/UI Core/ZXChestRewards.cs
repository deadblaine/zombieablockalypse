﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;
using DarkTonic.MasterAudio;

public class ZXChestRewards : MonoBehaviour
{
    [SerializeField] private ZXRewardCard rewardDisplay;

    private static ZXChestRewards singleton;
    private static int count = 0;
    private static int maxCount = 3;

    public static bool Loaded
    {
        get
        {
            return singleton != null;
        }
    }

    private void Awake()
    {
        singleton = this;

        gameObject.SetActive(false);
    }

    public static void GrantChestRewards()
    {
        singleton.StartRewards();
    }

    private void StartRewards()
    {
        count = 0;
        maxCount = Random.Range(3, 5);

        ZXCore.ZXPopup.HideActive();

        gameObject.SetActive(true);

        NextReward();

        MasterAudio.PlaySound("UI_Reward_Opening_Chest_Ambient");
    }

    public void NextReward()
    {
        if (count < maxCount)
        {
            List<ZXCard> list = new List<ZXCard>(ZXDataManager.cards.Values);
            ZXCard reward = list[Random.Range(0, list.Count)];
            int amount = Random.Range(1, 6);

            ZXDataManager.cards[reward.Key].Cards += amount;

            rewardDisplay.Set(reward, amount);

            count++;
        }
        else
            Close();
    }

    private void Close()
    {
        gameObject.SetActive(false);

        MasterAudio.FadeOutAllOfSound("UI_Reward_Opening_Chest_Ambient", 1f);
    }

    private void OnDestroy()
    {
        singleton = null;
    }
}
