﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXPausePopup : ZXCore.ZXPopup
{
    private static ZXPausePopup singleton;

    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
    }

    public static void ResumeGame()
    {
        singleton.Resume();
    }

    public void Resume()
    {
        Hide();

        ZXCombatUI.ChangeMissionState(ZXCombatUI.State.Combat);
    }

    public void Abort()
    {
        Hide();

        ZXCombatUI.ChangeMissionState(ZXCombatUI.State.Defeat);
    }
}
