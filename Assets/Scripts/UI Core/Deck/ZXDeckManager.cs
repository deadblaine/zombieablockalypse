﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXDeckManager : MonoBehaviour
{
    private const string Default_Deck = "hero_01#waifu_01#waifu_03#waifu_04#spell_01#spell_07#spell_08";
    private const string Deck_One_ID = "player_deck_one";
    private const string Deck_Two_ID = "player_deck_two";
    private const string Deck_Three_ID = "player_deck_three";
    private const char Parse = '#';
    private const int Deck_Size = 7;

    [SerializeField] private Transform deckContainer;
    [SerializeField] private Transform extraContainer;
    [SerializeField] private Transform useContainer;
    [SerializeField] private TextMeshProUGUI averageCostText;

    public static bool UseActive
    {
        get
        {
            return use;
        }
    }
    public static ZXCard.Card_Type UseCardType
    {
        get
        {
            return useCard.Card.Type;
        }
    }
    public static ZXCardUI UseCardUI
    {
        get
        {
            return useCard; 
        }
    }
        
    private List<Transform> deckSlots;
    private List<ZXCardUI> deckCards;
    private List<Transform> extraSlots;
    private List<ZXCardUI> extraCards;

    private static ZXDeckManager singleton;

    private static string deck_ID;
    private static List<string> deck = null;
    private static bool use = false;
    private static ZXCardUI useCard = null;

	// Use this for initialization
	private void Awake()
    {
        singleton = this;

        //Build deck slots
        deckSlots = new List<Transform>(deckContainer.childCount);

        useContainer.gameObject.SetActive(false);

        for (int i = 0; i < deckContainer.childCount; i++)
        {
            deckSlots.Add(deckContainer.GetChild(i));
        }

        //Build extra slots
        extraSlots = new List<Transform>(extraContainer.childCount * extraContainer.GetChild(0).childCount);

        for (int i = extraContainer.childCount; i > 0; i--)
        {
            for (int x = 0; x < extraContainer.GetChild(i - 1).childCount; x++)
            {
                extraSlots.Add(extraContainer.GetChild(i - 1).GetChild(x));
            }
        }

        deck = LoadDeck(1);
    }

    public void Refresh()
    {
        float totalCost = 0f;
        float averageCost;

        deckCards = new List<ZXCardUI>(Deck_Size);
        extraCards = new List<ZXCardUI>(ZXDeckContent.cards.Count - Deck_Size);

        //Sort cards into deck and extra
        for (int i = 0; i < ZXDeckContent.cards.Count; i++)
        {
            if (deck.Contains(ZXDeckContent.cards[i].Card.Key))
                deckCards.Add(ZXDeckContent.cards[i]);
            else
                extraCards.Add(ZXDeckContent.cards[i]);
        }

        //Place cards in deck
        for (int i = 0; i < deckCards.Count; i++)
        {
            totalCost += deckCards[i].Card.Cost;

            deckCards[i].UpdateParent(deckSlots[i]);
        }

        averageCost = totalCost / (Deck_Size - 1);

        averageCostText.text = ZXLocal.LocalText("AverageCost") + averageCost.ToString("0.0");

        //Place cards in extra
        for (int i = 0; i < extraCards.Count; i++)
        {
            extraCards[i].UpdateParent(extraSlots[i]);
        }
    }

    private void Update()
    {
        string pp_Virus = "pp_virus_clock";
        float rate = 2;
        float total;

        DateTime lastRun;
        TimeSpan time;

        if (ZXDeckContent.cards == null)
            return;

        if (PlayerPrefs.HasKey(pp_Virus))
            lastRun = DateTime.FromBinary(Convert.ToInt64(PlayerPrefs.GetString(pp_Virus)));
        else
        {
            PlayerPrefs.SetString(pp_Virus, DateTime.Now.ToBinary().ToString());
            lastRun = DateTime.Now;
        }

        time = System.DateTime.Now.Subtract(lastRun);

        if (time.TotalMinutes < 2)
            return;

        List<ZXCard> heroes = ZXDataManager.CardsByType(ZXCard.Card_Type.Hero);
        List<ZXCard> waifus = ZXDataManager.CardsByType(ZXCard.Card_Type.Waifu);

        total = (float) time.TotalMinutes * rate;

        heroes[UnityEngine.Random.Range(0, heroes.Count)].Virus += Mathf.RoundToInt(total);
        waifus[UnityEngine.Random.Range(0, waifus.Count)].Virus += Mathf.RoundToInt(total);

        PlayerPrefs.SetString(pp_Virus, DateTime.Now.ToBinary().ToString());
    }

    //Saving and Loading
    private static List<string> LoadDeck(int DeckNumber)
    {
        string deckData;

        switch (DeckNumber)
        {
            case 0:
                deck_ID = Deck_One_ID;
                break;

            case 1:
                deck_ID = Deck_Two_ID;
                break;

            case 2:
                deck_ID = Deck_Three_ID;
                break;

            default:
                deck_ID = Deck_One_ID;
                break;
        }

        if (PlayerPrefs.HasKey(deck_ID))
            deckData = PlayerPrefs.GetString(deck_ID);
        else
            deckData = Default_Deck;

        return new List<string>(deckData.Split(Parse));
    }

    public static List<ZXCard> FetchCards()
    {
        List<ZXCard> cards = new List<ZXCard>(7);

        foreach (string str in deck)
        {
            cards.Add(ZXDataManager.cards[str]);
        }

        return cards;
    }

    private static void SaveDeck()
    {
        string save = deck[0];

        for (int i = 1; i < deck.Count; i++)
        {
            save += Parse + deck[i];
        }

        PlayerPrefs.SetString(deck_ID, save);
    }

    //Static Functions
    public static bool DeckContains(ZXCardUI Card)
    {
        return singleton.deckCards.Contains(Card);
    }

    public static void UseCard(ZXCardUI Card)
    {
        use = true;
        useCard = Card;

        singleton.useContainer.gameObject.SetActive(true);
        Card.UpdateParent(singleton.useContainer);
        singleton.extraContainer.gameObject.SetActive(false);

        ZXBottomBar.ToggleUse(true);

        ZXMainScrollview.GetScrollView(ZXCore.ZXSection.Type.Deck).ScrollToStart();
    }

    public static void SwapCard(ZXCardUI Card)
    {
        int index = deck.IndexOf(Card.Card.Key);

        deck[index] = useCard.Card.Key;

        SaveDeck();

        EndUseCard();
    }

    public static void EndUseCard()
    {
        use = false;
        useCard = null;

        singleton.extraContainer.gameObject.SetActive(true);
        singleton.Refresh();
        singleton.useContainer.gameObject.SetActive(false);

        ZXBottomBar.ToggleUse(false);
    }
}
