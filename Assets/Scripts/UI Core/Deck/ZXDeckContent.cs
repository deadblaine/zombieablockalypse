﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ZXDeckContent : ZXCore.ZXSection, ZXCore.IZXContent
{
    [SerializeField] private ZXDeckManager deck;
    [SerializeField] private GameObject cardPrefab;
    [SerializeField] private TextMeshProUGUI cardsCollectedText;

    public static List<ZXCardUI> cards;

    private static ZXDeckContent singleton;
    private static bool built = false;

    /*Initalization*/
    protected override void Awake()
    {
        base.Awake();

        singleton = this;
    }

    private void Start()
    {
        BuildCards();
    }

    /*Display*/
    private void BuildCards()
    {
        ZXCardUI newCard;

        cards = new List<ZXCardUI>(ZXDataManager.cards.Count);

        foreach (KeyValuePair<string, ZXCard> entry in ZXDataManager.cards)
        {
            newCard = Instantiate(cardPrefab).GetComponentInChildren<ZXCardUI>();
            newCard.Load(entry.Value);
            cards.Add(newCard);
        }

        cardsCollectedText.text = ZXLocal.LocalText("CardsCollected") + ZXDataManager.cards.Count.ToString() + ZXLocal.LocalText("Divisor") + ZXDataManager.cards.Count.ToString();

        built = true;

        deck.Refresh();
    }

    public override void Show()
    {
        if (!built)
            BuildCards();

        base.Show();
    }
}
