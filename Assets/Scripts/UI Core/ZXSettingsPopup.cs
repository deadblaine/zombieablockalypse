﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXSettingsPopup : ZXCore.ZXPopup
{
    protected override void Awake()
    {
        base.Awake();

        gameObject.SetActive(false);
    }

}
