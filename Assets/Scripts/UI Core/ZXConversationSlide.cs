﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ZXConversationSlide : MonoBehaviour
{
    public static float transition = 0.25f;

    public enum Position
    {
        Left = 0,
        Right = 1,
    }

    [SerializeField] private Image image;
    [SerializeField] private GameObject textDisplay;
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI convoText;
    [SerializeField] private Position position;

    private float imageStart;
    private float imageEnd;
    private float convoStart = -250;
    private float convoEnd = 160f;

    private RectTransform imageRect;
    private RectTransform convoRect;

    private bool showing = false;

    private static System.Action<Position> e_SlideChange;

    private System.Action<Position> d_SlideChange;

    private void Awake()
    {
        imageStart = position == Position.Left ? -1600f : 1600;
        imageEnd = position == Position.Left ? -540 : 500;

        imageRect = image.GetComponent<RectTransform>();
        convoRect = textDisplay.GetComponent<RectTransform>();

        d_SlideChange = new System.Action<Position>(SlideChanged);

        e_SlideChange += d_SlideChange;

        ResetDisplay();
    }

    public void ShowText(string Name, string Text, Sprite sprite)
    {
        image.sprite = sprite;
        nameText.text = Name;
        convoText.text = Text;

        if (!showing)
            Show();

        if (e_SlideChange != null)
            e_SlideChange(position);
    }

    public void ResetDisplay()
    {
        imageRect.anchoredPosition = new Vector2(imageStart, imageRect.anchoredPosition.y);
        convoRect.anchoredPosition = new Vector2(convoRect.anchoredPosition.x, convoStart);

        //gameObject.SetActive(false);
    }

    private void SlideChanged(Position NewPosition)
    {
        int index = position == NewPosition ? 2 : 0;

        textDisplay.SetActive(position == NewPosition);

        transform.SetSiblingIndex(index);
    }

    public void Show()
    { 
        imageRect.DOAnchorPos(new Vector2(imageEnd, imageRect.anchoredPosition.y), transition);
        convoRect.DOAnchorPos(new Vector2(convoRect.anchoredPosition.x, convoEnd), transition).OnComplete(delegate () { showing = true; });
    }

    public void Hide()
    {
        if (showing)
        {
            imageRect.DOAnchorPos(new Vector2(imageStart, imageRect.anchoredPosition.y), transition);
            convoRect.DOAnchorPos(new Vector2(convoRect.anchoredPosition.x, convoStart), transition).OnComplete(delegate () { showing = false; });
        }
    }

    private void OnDestroy()
    {
        e_SlideChange -= d_SlideChange;
    }
}
