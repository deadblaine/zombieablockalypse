﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZXBottomBar : MonoBehaviour
{
    [SerializeField] private GameObject NavBar;
    [SerializeField] private GameObject UseText;
    [SerializeField] private GameObject UseCancel;

    private static ZXBottomBar singleton;

    private void Awake()
    {
        singleton = this;
    }

    public static void ToggleUse(bool Active)
    {
        singleton.NavBar.SetActive(!Active);
        singleton.UseText.SetActive(Active);
        singleton.UseCancel.SetActive(Active);
    }

    public static void ToggleCampaign(bool Active)
    {
        singleton.NavBar.SetActive(!Active);
    }

    public void CancelUse()
    {
        ZXDeckManager.EndUseCard();
    }
}
