﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ZXCampaignPopup : ZXCore.ZXPopup
{
    private static ZXCampaignPopup singleton;

    [SerializeField] private List<ZXCampaignMission> missions;
    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI text;

    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        foreach (ZXCampaignMission mission in missions)
        {
            mission.AddToList();
        }

        gameObject.SetActive(false);
    }

    public override void Show()
    {
        base.Show();

        foreach (ZXCampaignMission mission in missions)
        {
            mission.BuildPaths();
        }

        foreach (ZXCampaignMission mission in missions)
        {
            mission.Refresh();
        }

        fill.fillAmount = ZXCampaignMission.MissionProgress;
        text.text = ZXCampaignMission.TotalStars.ToString() + " / " + ZXCampaignMission.MaxStars.ToString();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public static void ShowPopup(bool Active)
    {
        if (Active)
            singleton.Show();
        else
            singleton.Hide();
    }
}
