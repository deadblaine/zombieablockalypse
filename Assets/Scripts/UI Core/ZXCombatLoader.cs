﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using TMPro;

public class ZXCombatLoader : ZXCore.ZXPopup
{ 
    //Inspector variables
    [SerializeField] private Image fill;
    [SerializeField] private TextMeshProUGUI text;

    //Accessors
    private float Fill
    {
        get
        {
            return fill.fillAmount;
        }
        set
        {
            text.text = string.Format("{0:0%}", Mathf.Clamp01(value));
            fill.fillAmount = Mathf.Clamp01(value);
        }
    }

    //Private variables
    private static ZXCombatLoader singleton;

    protected override void Awake()
    {
        base.Awake();

        singleton = this;

        gameObject.SetActive(false);
    }

    public static void LoadCombatScene(int Scene)
    {
        singleton.Show();

        singleton.StartCoroutine(singleton.LoadAsyncScene(Scene));    
    }

    private IEnumerator LoadAsyncScene(int Scene)
    {
        AsyncOperation loading = SceneManager.LoadSceneAsync(Scene);

        loading.allowSceneActivation = true;

        Fill = 0f;

        yield return new WaitUntil(delegate () 
        {
            Fill = loading.progress;
            return loading.isDone;
        });
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        StopAllCoroutines();
    }
}
