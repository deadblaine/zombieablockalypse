﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ZXClickScreen : MonoBehaviour, ZXCore.IZXContent, IPointerClickHandler
{
    private const float transTime = 0.1f;
    private const string tw_ID = "tw_ClickScreen";

    private static ZXClickScreen singleton;
    private Image image;
    private static Color clearBlack;
    private static Color transBlack;

    private void Awake()
    {
        singleton = this;

        image = GetComponent<Image>();

        clearBlack = Color.black;
        clearBlack.a = 0;

        transBlack = Color.black;
        transBlack.a = 0.5f;

        gameObject.SetActive(false);
    }

    public static void ShowClickScreen()
    {
        if (singleton != null)
            singleton.Show();
    }

    public void Show()
    {
        image.color = clearBlack;

        gameObject.SetActive(true);

        if (DOTween.IsTweening(tw_ID))
            DOTween.Kill(tw_ID);

        image.DOColor(transBlack, transTime).SetId(tw_ID);
    }

    public static void HideClickScreen()
    {
        if (singleton != null)
            singleton.Hide();
    }

    public void Hide()
    {
        if (DOTween.IsTweening(tw_ID))
            DOTween.Kill(tw_ID);

        image.DOColor(clearBlack, transTime).SetId(tw_ID).OnComplete(delegate() { gameObject.SetActive(false); });
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ZXCore.ZXPopup.ClickScreenTap();
    }

    private void OnDestroy()
    {
        if (DOTween.IsTweening(tw_ID))
            DOTween.Complete(true);
    }
}
