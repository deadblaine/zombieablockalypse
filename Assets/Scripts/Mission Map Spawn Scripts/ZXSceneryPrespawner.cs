﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXSceneryPrespawner : MonoBehaviour
{
    [SerializeField] private ZXZombie toon;
    private ZXZombie.BehaviorPattern behavior = ZXZombie.BehaviorPattern.Nothing;
    [SerializeField] private ZXZombie.Action action;
    private new Collider collider;

    private void Awake()
    {
        collider = GetComponent<Collider>();

        if (collider != null)
            collider.enabled = false;
    }

    private void Start()
    {
        if (toon != null)
        {
            toon = Instantiate<ZXZombie>(toon);
            toon.Place(transform.position, transform.rotation, null);

            Invoke("BeingAnimation", 1f);
        }
    }

    private void BeingAnimation()
    {
        toon.PhysicsOverride(true);
        //toon.ChangePosture(Posture.Stand);
        toon.SetBehavior(ZXZombie.BehaviorPattern.Nothing);
        toon.AnimatorOverride(ZXToon.AnimState.Override, (int)action);

    }

    public void EndShowcase()
    {
        if (toon != null)
            toon.ExitScene();
    }
}
