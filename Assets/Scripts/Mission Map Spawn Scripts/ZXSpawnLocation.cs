﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using ZXSpawnData;
using DG.Tweening;

public class ZXSpawnLocation : MonoBehaviour
{
    //Private constants
    private const float _clusterRange = 7f;

    //Enums
    public enum ClusterClass
    {
        Single = 0,
        Pair = 1,
        Cluster = 2,
    }

    //Inspector variables
    [SerializeField] private SpawnCondition condition;
    [SerializeField] private bool preSpawner;
    [SerializeField] private bool visible;
    [SerializeField] private bool tankSpawn;

    //Accessors
    public SpawnCondition Condition
    {
        get
        {
            return condition;
        }
    }
    public ClusterClass Cluster
    {
        get
        {
            return cluster;
        }
    }
    public bool isPrespawner
    {
        get
        {
            return preSpawner;
        }
    }
    public bool isTankSpawn
    {
        get
        {
            return tankSpawn;
        }
    }

    //Private tracking
    private ZXSpawnRegion region;
    private ZXNavigationNexus nexus;
    private Quaternion rotationToFaceNexus;
    private Vector3 location;

    private List<ZXSpawnLocation> nearbySpawners;
    private ClusterClass cluster;

    //Initalization
    private void Awake()
    {
        //Register with reflecting pool
        ZXReflectingPool.Register<ZXSpawnLocation>(this, transform);
    }

    private void Start()
    {
        location = transform.position;

        //Find region
        region = ZXReflectingPool.FindClosest<ZXSpawnRegion>(transform);

        if (region != null)
            region.Register(this);
        else
        {
            Debug.Log(gameObject.name + " could not find closest region");
            Destroy(gameObject);
        }

        //Find nearest nexus
        nexus = ZXReflectingPool.FindClosest<ZXNavigationNexus>(transform);

        if (nexus != null)
        {
            Vector3 direction = (nexus.transform.position - transform.position).normalized;

            rotationToFaceNexus = Quaternion.LookRotation(direction);
        }
    }

    public void Initalize()
    {
        nearbySpawners = ZXReflectingPool.FindInSphere<ZXSpawnLocation>(transform.position, _clusterRange, delegate (ZXSpawnLocation Spawner) { return Spawner != this; });

        if (nearbySpawners == null)
            cluster = ClusterClass.Single;
        else if (nearbySpawners.Count == 1)
            cluster = ClusterClass.Pair;
        else
            cluster = ClusterClass.Cluster;
    }

    //Spawn functions
    public void Spawn(ZXToon toon, float delay)
    {
        if (toon is ZXZombie)
            StartCoroutine(Spawn(toon as ZXZombie, delay));
        else if (toon is ZXRaider)
            StartCoroutine(Spawn(toon as ZXRaider, delay));
    }

    private IEnumerator Spawn(ZXZombie zombie, float delay)
    {
        yield return new WaitForSeconds(delay);

        if (visible)
        {
            if (ZXSpawnManager.p_RiseEffect != null)
            {
                ZXParticleEffect riseEffect = ZXSpawnManager.p_RiseEffect.Request();

                if (riseEffect != null)
                    riseEffect.Place(transform.position, Quaternion.identity, null);
            }

            zombie.Place(transform.position, rotationToFaceNexus, ZXZombie.Action.ZombieRise);
        }
        else
            zombie.Place(transform.position, rotationToFaceNexus, null);

        zombie.AdjustValues(ZXSpawnManager.RandomLevel());
        ZXSpawnManager.SpawnedActor(zombie);
    }

    private IEnumerator Spawn(ZXRaider raider, float delay)
    {
        yield return new WaitForSeconds(delay);

        if (visible)
        {
            Vector3 raisedLocation = location + new Vector3(0f, 1.5f, 0f);

            raider.Place(raisedLocation, rotationToFaceNexus, ZXToon.Action.DragFromUI);
            raider.AdjustValues(ZXSpawnManager.RandomLevel());

            yield return new WaitForSeconds(0.5f);

            raider.Place(raisedLocation, rotationToFaceNexus, ZXToon.Action.LandFromFalling);
        }
        else
            raider.Place(location, rotationToFaceNexus, null);

        ZXSpawnManager.SpawnedActor(raider);
    }

    //Destruction
    private void OnDisable()
    {
        StopAllCoroutines();
        CancelInvoke();

        if (region != null)
            region.DeRegister(this);

        ZXReflectingPool.DeRegister<ZXSpawnLocation>(transform);
    }
}
