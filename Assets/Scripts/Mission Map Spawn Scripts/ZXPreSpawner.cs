﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXPreSpawner : MonoBehaviour
{
    //Private constants
    private const float _clusterRange = 7f;

    //Public types
    public enum PreSpawnerClass
    {
        Single = 0,
        Pair = 1,
        Cluster = 2,
    }
    public enum PreSpawnZombie
    {
        ZombieCorpse = 0,
        ZombieLurk = 1,
        ZombieEating = 2,
    }
    public enum PreSpawnRaider
    {
        RaiderMelee = 0,
        RaiderRanged = 1,
    }
    public enum PreSpawnOther
    {
        Civilian = 0,
    }

    //Inspector variables
    [SerializeField] private ZXSpawnData.SpawnCondition condition;

    //Accessors
    public ZXSpawnData.SpawnCondition Condition
    {
        get
        {
            return condition;
        }
    }
    public PreSpawnerClass SpawnerClass
    {
        get
        {
            return spawnerClass;
        }
    }
    public List<ZXPreSpawner> NearbyPreSpawners
    {
        get
        {
            return nearbyPreSpawners;
        }
    }

    //Private variables
    private List<ZXPreSpawner> nearbyPreSpawners;
    private PreSpawnerClass spawnerClass;

    /* Initalization */
	private void Awake()
    {
        ZXReflectingPool.Register<ZXPreSpawner>(this, transform);

        transform.rotation = Quaternion.identity;
    }
    private void Start()
    {
        //ZXCombatUI.Spawner.Register(this);
    }

    public void Initalize()
    {
        nearbyPreSpawners = ZXReflectingPool.FindInSphere<ZXPreSpawner>(transform.position, _clusterRange, delegate (ZXPreSpawner PreSpawner) { return PreSpawner != this; });

        if (nearbyPreSpawners == null)
            spawnerClass = PreSpawnerClass.Single;
        else if (nearbyPreSpawners.Count == 1)
            spawnerClass = PreSpawnerClass.Pair;
        else
            spawnerClass = PreSpawnerClass.Cluster;
    }

    /* Spawn Functions */
    public void Spawn(PreSpawnZombie Type)
    {
        ZXToon newToon = null;

        //Get the appropriate toon
        switch (Type)
        {
            case PreSpawnZombie.ZombieCorpse:
                newToon = ZXSpawnManager.p_Zombies.Request();
                break;

            case PreSpawnZombie.ZombieLurk:
                newToon = ZXSpawnManager.p_Zombies.Request();
                break;

            case PreSpawnZombie.ZombieEating:
                newToon = ZXSpawnManager.p_Zombies.Request();
                break;
        }

        //If no toon was in the pool
        if (newToon == null)
            { Debug.Log("NO ZOMBIE IN THE POOL FOR PRESPAWNER"); }
            return;

        //Place the toon
        switch (Type)
        {
            case PreSpawnZombie.ZombieCorpse:
                newToon.Place(transform.position, Random.rotation, ZXZombie.BehaviorPattern.Corpse);
                break;

            case PreSpawnZombie.ZombieLurk:
                newToon.Place(transform.position, ZXSpawnManager.RandomY(), ZXZombie.BehaviorPattern.Lurk);
                break;

            case PreSpawnZombie.ZombieEating:
                newToon.Place(transform.position, ZXSpawnManager.RandomY(), ZXZombie.BehaviorPattern.Eat);
                break;
        }

        //Report new enemy
        newToon.AdjustValues(ZXSpawnManager.RandomLevel());
        ZXSpawnManager.SpawnedActor(newToon);
    }
    public void Spawn(PreSpawnRaider Type)
    {
        ZXToon newToon = null;

        //Get the appropriate toon
        switch (Type)
        {
            case PreSpawnRaider.RaiderMelee:
                newToon = ZXSpawnManager.p_RaidersMelee.Request();
                break;

            case PreSpawnRaider.RaiderRanged:
                newToon = ZXSpawnManager.p_RaidersRanged.Request();
                break;
        }

        //If no toon was in the pool
        if (newToon == null)
            return;

        //Place the toon
        switch (Type)
        {
            case PreSpawnRaider.RaiderMelee:
                newToon.Place(transform.position, ZXSpawnManager.RandomY(), null);
                break;

            case PreSpawnRaider.RaiderRanged:
                newToon.Place(transform.position, ZXSpawnManager.RandomY(), null);
                break;
        }

        //Report new enemy
        newToon.AdjustValues(ZXSpawnManager.RandomLevel());
        ZXSpawnManager.SpawnedActor(newToon);
    }
    public void Spawn(PreSpawnOther Type)
    {
        ZXToon newToon = null;

        switch (Type)
        {
            //Not currently supported
            case PreSpawnOther.Civilian:
                newToon = null;
                break;
        }

        if (newToon == null)
            return;

        //Place the toon
        switch (Type)
        {
            case PreSpawnOther.Civilian:
                newToon.Place(transform.position, ZXSpawnManager.RandomY(), ZXHuman.BehaviorPattern.Civilian);
                break;
        }
    }

    /* Destruction */
    private void OnDisable()
    {
        //if (ZXCombatUI.Spawner != null)
        //    ZXCombatUI.Spawner.DeRegister(this);

        ZXReflectingPool.DeRegister<ZXPreSpawner>(transform);

        Destroy(gameObject);
    }
}
