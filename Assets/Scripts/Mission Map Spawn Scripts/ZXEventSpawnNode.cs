﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXEventSpawnNode : MonoBehaviour
{
    private Vector3 startingLocalPosition;
    private new Collider collider;

    private void Awake()
    {
        startingLocalPosition = transform.localPosition;

        collider = GetComponent<Collider>();

        if (collider != null)
            collider.enabled = false;
    }

    public void SnapToNavMesh()
    {
        UnityEngine.AI.NavMeshHit hit;

        if (UnityEngine.AI.NavMesh.SamplePosition(transform.position, out hit, 10f, 1 /* Built-in layer 0 - Walkable*/))
        {
            transform.position = hit.position;
        }
        else
        {
            Debug.Log("Error placing: " + gameObject.name);
            gameObject.SetActive(false);
        }
    }

    public void SnapToStart()
    {
        transform.localPosition = startingLocalPosition;
    }

    private void OnDisable()
    {
        SnapToStart();
    }
}
