﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXTankZombie : ZXZombie
{
    //Sounds
    [SoundGroupAttribute] [SerializeField] protected string s_PullRockFromGround;
    [SoundGroupAttribute] [SerializeField] protected string s_ThrowRock;
    [SoundGroupAttribute] [SerializeField] protected string s_MeleeImpactSound;

    //Inspector fields
    [SerializeField] private ZXExplosive rockPrefab;
    [SerializeField] private float rockRange;
    [SerializeField] private float rockFlightSpeed;
    [SerializeField] private float rockSpread;
    [SerializeField] private float rockRateOfFire;

    //Projectile pool
    private ZXPoolLite<ZXProjectile> p_RockPool;
    private int p_Size = 5;

    //Private variables
    private ZXExplosive loadedRock;
    private float rockTimer;
    private Vector3 attackPoint;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        p_RockPool = new ZXPoolLite<ZXProjectile>(t_RightHand.transform, rockPrefab, p_Size);

        canRagDoll = false;

        attackRange = rockRange;

        //Manually change melee range
        meleeRange = 5;

        playDamageAnimations = false;
    }

    protected override void OnEnable()
    {
        base.OnEnable();

        rockTimer = rockRateOfFire;
    }

    public override void AdjustValues(int Level)
    {
        base.AdjustValues(Level);
    }

    //Runtime
    protected override void Update()
    {
        base.Update();

        if (rockTimer > 0)
            rockTimer -= Time.deltaTime;
    }


    //Action functions
    protected override void PerformAction(int ActionCode)
    {
        switch (ActionCode)
        {
            case (int)Action.MeleeGeneric:
                attackTarget = FollowObject;
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;

                MasterAudio.PlaySound3DAtTransform(s_meleeAttackSound, transform);
                break;

            case (int)Action.PickUpRock:
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;
                break;

            case (int)Action.Throw:
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;
                break;

            default:
                base.PerformAction(ActionCode);
                break;
        }
    }

    private void LoadRock()
    {
        if (loadedRock == null)
            loadedRock = p_RockPool.Request() as ZXExplosive;

        if (loadedRock != null)
        {
            loadedRock.transform.SetParent(t_Head.transform);
            loadedRock.transform.localPosition = Vector3.zero;
            loadedRock.transform.localRotation = Quaternion.identity;
            loadedRock.gameObject.SetActive(true);

            loadedRock.SetDamage(Mathf.RoundToInt(loadedRock.ExplosiveDamage * zombieDamageMod));
        }

        MasterAudio.PlaySound3DAtTransform(s_PullRockFromGround, transform);
    }

    private void ThrowRock()
    {
        if (loadedRock != null)
        {
            attackPoint += new Vector3(Random.Range(-rockSpread, rockSpread), 0f, Random.Range(-rockSpread, rockSpread));

            loadedRock.Arm();
            loadedRock.Launch(attackPoint, Vector3.Distance(attackPoint, Center) / rockFlightSpeed);
            loadedRock = null;
            rockTimer = rockRateOfFire;

            MasterAudio.PlaySound3DAtTransform(s_ThrowRock, transform);
        }
    }

    public override void AnimationCallback(int Code)
    {
        if (logDebugging)
            Debug.Log("AnimCallback:" + (CallbackCode)Code);

        switch (Code)
        {
            case (int)CallbackCode.MeleeDamage:
                if (attackTarget != null)
                {
                    Ray ray = new Ray(transform.position, (FollowObject.LookAtTransform.position - LookAtTransform.position).normalized);
                    ZXDamage damage = new ZXDamage(meleeDamage, 0f, 0.2f, 0f, Race, ZXDamage.Type.Impact, ray, ZXDamage.SpecialEffect.None);
                    attackTarget.TakeDamage(damage);
                }
                MasterAudio.PlaySound3DAtTransform(s_MeleeImpactSound, transform);
                break;

            case (int)CallbackCode.PickUpRock:
                LoadRock();
                break;

            case (int)CallbackCode.ThrowRock:
                ThrowRock();
                break;

            default:
                base.AnimationCallback(Code);
                break;
        }
    }

    protected override void TakeDamageEffect(ZXDamage Damage)
    {
        float random = Random.Range(0f, 1f);

        if (!playDamageAnimations)
            return;

        switch (Damage.Effect)
        {
            case ZXDamage.SpecialEffect.ForceRagdoll:

                if (random > 0.5f)
                    EnqueueAction((int)Action.TakeBigDamage);
                else
                    EnqueueAction((int)Action.TakeDamage);

                break;
        }
    }

    //New attack move behavior
    protected override int AttackMove()
    {
        IZXTargetable bestTarget = null;
        float distance;

        //If in ragdoll, stand up
        if (posture == Posture.Ragdoll && health > 0)
            EnqueueAction((int)Action.FromRagdoll);

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];
        else if (tr_NearestBarricade != null)
            bestTarget = tr_NearestBarricade.GetTarget;

        //If no target is found
        if (bestTarget == null)
        {
            SetBehavior(BehaviorPattern.Lurk);
            return (int)defaultAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Compute distance
        distance = FollowObject.GetDistance(Center);

        //Check range
        if (distance <= meleeRange)
        {
            //return (int)Action.PoundGround;
            return (int)Action.MeleeGeneric;
        }
        else if (distance > rockRange || distance < (meleeRange * 2 ) || rockTimer > 0f)
        {
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.Base);

            return (int)defaultMoveAction;
        }
        else if (rockTimer <= 0f)
        {
            ChangeSpeed(Speed.Stop);
            attackPoint = FollowObject.Center;
            return (int)Action.PickUpRock;
        }
        else
        {
            return (int)defaultAction;
        }
    }

    protected void GroundSlam()
    {
        List<IZXDamageable> damageableList;

        damageableList = ZXReflectingPool.FindInSphere<IZXDamageable>(transform.position, attackRange);

        if (damageableList != null && damageableList.Count > 0)
        {
            damageableList.RemoveAll(delegate (IZXDamageable Object) { return Object.Race == Race; });
        }

            if (damageableList != null && damageableList.Count > 0)
        {
            ZXDamage damage;
            Ray trajectory;

            for (int i = 0; i < damageableList.Count; i++)
            {
                //Look on the objects parent
                if (damageableList[i] != null)
                {
                    trajectory = new Ray(transform.position, (damageableList[i].Center - transform.position).normalized);

                    damage = new ZXDamage(meleeDamage, 0f, 0.2f, 0f, Race, ZXDamage.Type.Impact, trajectory, ZXDamage.SpecialEffect.ForceRagdoll);
                    damageableList[i].TakeDamage(damage);
                }
            }
        }
    }

    /* Communication */
    public override void RecieveMessage(ZXInterestPoint Sender, Vector3 Location, ZXInterestPoint.Interest Header, ZXInterestPoint.BroadcastMessage Message)
    {

    }
}
