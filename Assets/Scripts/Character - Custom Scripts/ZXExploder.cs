﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;

public class ZXExploder : ZXZombie
{
    [SerializeField] private ZXExplosive explosionEffect;

    private ZXExplosive explosion;

    //Initalization
    protected override void Awake()
    {
        base.Awake();

        //Load explosion effect
        explosion = Instantiate(explosionEffect);
        explosion.transform.SetParent(t_Body.transform);
        explosion.transform.localPosition = Vector3.zero;
        explosion.transform.localRotation = Quaternion.identity;

        //Manually change melee range
        meleeRange = 5;
    }

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    public override void AdjustValues(int Level)
    {
        base.AdjustValues(Level);

        explosion.SetDamage(Mathf.RoundToInt(explosion.ExplosiveDamage * zombieDamageMod));
    }

    protected override int AttackMove()
    {
        IZXTargetable bestTarget = null;

        //If in ragdoll, stand up
        if (posture == Posture.Ragdoll && health > 0)
            EnqueueAction((int)Action.FromRagdoll);

        //Check visible targets list, default to an objective
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];

        //If no target is found
        if (bestTarget == null)
        {
            SetBehavior(BehaviorPattern.Lurk);
            return (int)defaultAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Check meele distance
        if (FollowObject.GetDistance(Center) > attackRange)
        {
            //If stopped, begin moving toward target
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.Base /*, true*/);

            return (int)defaultMoveAction;
        }
        //If within range perform meele attack
        else
        {
            return (int)Action.MeleeGeneric;
        }
    }

    protected override void PerformAction(int ActionCode)
    {
        switch (ActionCode)
        {
            case (int)Action.MeleeGeneric:
                actionWaitFlag = true;
                break;

            default:
                base.PerformAction(ActionCode);
                break;
        }
    }

    public override void AnimationCallback(int Code)
    {
        if (logDebugging)
            Debug.Log("AnimCallback:" + (CallbackCode)Code);

        switch (Code)
        {
            case (int)CallbackCode.MeleeDamage:
                ChangePosture(Posture.Ragdoll);
                Die();
                r_Shader.Fade(0f, 0.1f, 3f, d_Disable);
                explosion.Detonate();
                break;

            default:
                base.AnimationCallback(Code);
                break;
        }
    }

    protected override void TakeDamageEffect(ZXDamage Damage)
    {
        float random = Random.Range(0f, 1f);

        if (!playDamageAnimations)
            return;

        switch (Damage.Effect)
        {
            case ZXDamage.SpecialEffect.ForceRagdoll:

                if (random > 0.5f)
                    EnqueueAction((int)Action.TakeBigDamage);
                else
                    EnqueueAction((int)Action.TakeDamage);

                break;
        }
    }

    /* Communication */
    public override void RecieveMessage(ZXInterestPoint Sender, Vector3 Location, ZXInterestPoint.Interest Header, ZXInterestPoint.BroadcastMessage Message)
    {

    }
}
