﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public class ZXBodypart : MonoBehaviour
{
    //Constant variables
    private const float _Drag = 0.25f;

    //Type enum
    public enum Bodypart
    {
        NotRecognized = 0,
        Char = 1,
        Root = 2,
        Head = 3,
        Hat = 4,
        Body = 5,
        Spine = 6,
        Hips = 7,
        UpperArm_Right = 8,
        LowerArm_Right = 9,
        Right_Hand = 10,
        UpperArm_Left = 11,
        LowerArm_Left = 12,
        Left_Hand = 13,
        UpperLeg_Right = 14,
        LowerLeg_Right = 15,
        Right_Foot = 16,
        UpperLeg_Left = 17,
        LowerLeg_Left = 18,
        Left_Foot = 19
    }
    public enum Configuration
    {
        Generic = 0,
        TankZombie = 2
    }

    //Inspector variables
    private Bodypart part;
    private ZXToon toon;

    //Accessors
    public Bodypart Part
    {
        get
        {
            return part;
        }
    }
    public float Weight
    {
        get
        {
            if (rigidbody != null)
                return rigidbody.mass;
            else
                return 0f;
        }
    }
    public Vector3 Center
    {
        get
        {
            if (collider != null)
                return collider.bounds.center;
            else
                return transform.position;
        }
    }
    public Collider GetCollider
    {
        get
        {
            return collider;
        }
    }
    public Vector3 Velocity
    {
        get
        {
            if (rigidbody != null && !rigidbody.isKinematic)
                return rigidbody.velocity;
            else
                return Vector3.zero;
        }
    }

    //Private variables
    private new Collider collider;
	[HideInInspector] public new Rigidbody rigidbody;
    private ZXCorpse interestPoint;
    private CharacterJoint joint;
    private FixedJoint fJoint;

    //Initalization
    private void Awake()
    {
        toon = GetComponentInParent<ZXToon>();
    }

    //Main functions
    public Vector3 ClosestPointOnBounds(Vector3 Origin)
    {
        if (collider != null)
            return collider.ClosestPointOnBounds(Origin);
        else
            return transform.position;
    }

    public void SetPhysics(bool State)
    {
        if (rigidbody == null)
            return;

        else
        {
            if (State)
            {
                //if (collider)
                //    collider.isTrigger = false;

                rigidbody.isKinematic = false;
                rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
                rigidbody.useGravity = true;
            }
            else
            {
                //if (collider)
                //    collider.isTrigger = true;

                rigidbody.isKinematic = true;
                rigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;
                rigidbody.useGravity = false;
            }
        }
    }
    public void SetColliders(bool State)
    {
        if (collider != null)
        {
            collider.enabled = State;
        }
    }

    /* Ragdoll generation */
    public void SetPart(Bodypart Bodypart, Configuration Config)
    {
        part = Bodypart;

        switch (Config)
        {
            case Configuration.Generic:
                SetGenericPart();
                break;
            case Configuration.TankZombie:
                SetTankPart();
                break;
        }
    }
    private void SetGenericPart()
    {
        switch (part)
        {
            case Bodypart.Char:
                CreateRigidbody(1f);
                break;

            case Bodypart.Root:
                CreateRigidbody(1f);
                break;

            case Bodypart.Head:
                CreateRigidbody(toon.Weight * 0.06f);
                CreateSphereCollider(new Vector3(-0.4f, 0f, 0f), 0.4f);
                break;

            case Bodypart.Hat:
                CreateRigidbody(1f);
                break;

            case Bodypart.Body:
                CreateRigidbody(toon.Weight * 0.1f);
                CreateBoxCollider(new Vector3(-0.5f, 0f, 0f), new Vector3(1f, 0.88f, 0.6f));
                CreateInterestPoint();
                break;

            case Bodypart.Spine:
                CreateRigidbody(toon.Weight * 0.1f);
                break;

            case Bodypart.Hips:
                CreateRigidbody(toon.Weight * 0.1f);
                CreateBoxCollider(Vector3.zero, new Vector3(0.15f, 0.15f, 0.5f));
                break;

            case Bodypart.UpperArm_Right:
                CreateRigidbody(toon.Weight * 0.05f);
                CreateCapsuleCollider(new Vector3(-0.24f, 0f, 0f), 0.13f, 0.47f);
                break;

            case Bodypart.LowerArm_Right:
                CreateRigidbody(toon.Weight * 0.04f);
                CreateCapsuleCollider(new Vector3(-0.22f, 0f, 0f), 0.13f, 0.44f);
                break;

            case Bodypart.Right_Hand:
                break;

            case Bodypart.UpperArm_Left:
                CreateRigidbody(toon.Weight * 0.05f);
                CreateCapsuleCollider(new Vector3(0.24f, 0f, 0f), 0.13f, 0.47f);
                break;

            case Bodypart.LowerArm_Left:
                CreateRigidbody(toon.Weight * 0.04f);
                CreateCapsuleCollider(new Vector3(0.22f, 0f, 0f), 0.13f, 0.44f);
                break;

            case Bodypart.Left_Hand:
                break;

            case Bodypart.UpperLeg_Right:
                CreateRigidbody(toon.Weight * 0.07f);
                CreateCapsuleCollider(new Vector3(0.19f, 0f, 0f), 0.15f, 0.37f);
                break;

            case Bodypart.LowerLeg_Right:
                CreateRigidbody(toon.Weight * 0.06f);
                CreateCapsuleCollider(new Vector3(0.22f, 0f, 0f), 0.13f, 0.45f);
                break;

            case Bodypart.Right_Foot:
                break;

            case Bodypart.UpperLeg_Left:
                CreateRigidbody(toon.Weight * 0.07f);
                CreateCapsuleCollider(new Vector3(-0.19f, 0f, 0f), 0.15f, 0.37f);
                break;

            case Bodypart.LowerLeg_Left:
                CreateRigidbody(toon.Weight * 0.06f);
                CreateCapsuleCollider(new Vector3(-0.22f, 0f, 0f), 0.13f, 0.45f);
                break;

            case Bodypart.Left_Foot:
                break;
        }
    }
    private void SetTankPart()
    {
        switch (part)
        {
            case Bodypart.Char:
                CreateRigidbody(1f);
                break;

            case Bodypart.Root:
                CreateRigidbody(1f);
                break;

            case Bodypart.Head:
                CreateRigidbody(toon.Weight * 0.06f);
                CreateSphereCollider(new Vector3(-0.51f, 0f, 0f), 0.51f);
                break;

            case Bodypart.Hat:
                CreateRigidbody(1f);
                break;

            case Bodypart.Body:
                CreateRigidbody(toon.Weight * 0.1f);
                CreateBoxCollider(new Vector3(-0.5f, 0f, 0.2f), new Vector3(1.72f, 1.28f, 1.03f));
                CreateInterestPoint();
                break;

            case Bodypart.Spine:
                CreateRigidbody(toon.Weight * 0.1f);
                break;

            case Bodypart.Hips:
                CreateRigidbody(toon.Weight * 0.1f);
                CreateBoxCollider(Vector3.zero, new Vector3(0.24f, 0.22f, 0.3f));
                break;

            case Bodypart.UpperArm_Right:
                CreateRigidbody(toon.Weight * 0.05f);
                CreateCapsuleCollider(new Vector3(-0.576f, 0f, 0f), 0.28f, 1.15f);
                break;

            case Bodypart.LowerArm_Right:
                CreateRigidbody(toon.Weight * 0.04f);
                CreateCapsuleCollider(new Vector3(-1.07f, 0f, 0f), 0.255f, 2f);
                break;

            case Bodypart.Right_Hand:
                break;

            case Bodypart.UpperArm_Left:
                CreateRigidbody(toon.Weight * 0.05f);
                CreateCapsuleCollider(new Vector3(0.559f, 0f, 0f), 0.28f, 1.15f);
                break;

            case Bodypart.LowerArm_Left:
                CreateRigidbody(toon.Weight * 0.04f);
                CreateCapsuleCollider(new Vector3(0.95f, 0f, 0f), 0.262f, 2f);
                break;

            case Bodypart.Left_Hand:
                break;

            case Bodypart.UpperLeg_Right:
                CreateRigidbody(toon.Weight * 0.07f);
                CreateCapsuleCollider(new Vector3(0.352f, 0f, 0f), 0.211f, 0.704f);
                break;

            case Bodypart.LowerLeg_Right:
                CreateRigidbody(toon.Weight * 0.06f);
                CreateCapsuleCollider(new Vector3(0.22f, 0f, 0f), 0.13f, 0.45f);
                break;

            case Bodypart.Right_Foot:
                break;

            case Bodypart.UpperLeg_Left:
                CreateRigidbody(toon.Weight * 0.07f);
                CreateCapsuleCollider(new Vector3(-0.352f, 0f, 0f), 0.21f, 0.704f);
                break;

            case Bodypart.LowerLeg_Left:
                CreateRigidbody(toon.Weight * 0.06f);
                CreateCapsuleCollider(new Vector3(-0.305f, 0f, 0f), .152f, 0.61f);
                break;

            case Bodypart.Left_Foot:
                break;
        }
    }

    public void SetJoint(Configuration Config)
    {
        switch (Config)
        {
            case Configuration.Generic:
                SetGenericJoint();
                break;
            case Configuration.TankZombie:
                SetTankJoint();
                break;
        }
    }
    private void SetGenericJoint()
    {
        switch (part)
        {
            case Bodypart.Char:
                break;

            case Bodypart.Root:
                CreateFixedJoint();
                break;

            case Bodypart.Head:
                CreateJoint(Vector3.up, Vector3.forward, -40f, 25f, 25f);
                break;

            case Bodypart.Hat:
                CreateFixedJoint();
                break;

            case Bodypart.Body:
                CreateFixedJoint();
                break;

            case Bodypart.Spine:
                CreateJoint(Vector3.up, Vector3.forward, -20f, 20f, 10f);
                break;

            case Bodypart.Hips:
                CreateFixedJoint();
                break;

            case Bodypart.UpperArm_Right:
                CreateJoint(Vector3.up, Vector3.back, -70f, 10f, 50f);
                break;

            case Bodypart.LowerArm_Right:
                CreateJoint(Vector3.back, Vector3.up, -90f, 0f, 0f);
                break;

            case Bodypart.Right_Hand:
                break;

            case Bodypart.UpperArm_Left:
                CreateJoint(Vector3.down, Vector3.forward, -70f, 10f, 50f);
                break;

            case Bodypart.LowerArm_Left:
                CreateJoint(Vector3.forward, Vector3.down, -90f, 0f, 0f);
                break;

            case Bodypart.Left_Hand:
                break;

            case Bodypart.UpperLeg_Right:
                CreateJoint(Vector3.down, Vector3.back, -20f, 70f, 30f);
                break;

            case Bodypart.LowerLeg_Right:
                CreateJoint(Vector3.back, Vector3.up, -80f, 0f, 0f);
                break;

            case Bodypart.Right_Foot:
                break;

            case Bodypart.UpperLeg_Left:
                CreateJoint(Vector3.down, Vector3.forward, -20f, 70f, 30f);
                break;

            case Bodypart.LowerLeg_Left:
                CreateJoint(Vector3.back, Vector3.down, -80f, 0f, 0f);
                break;

            case Bodypart.Left_Foot:
                break;
        }
    }
    private void SetTankJoint()
    {
        //Stub for Damian, see SetGenericJoint()
        switch (part)
        {
            case Bodypart.Char:
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                GetComponent<Rigidbody>().isKinematic = true;
                break;

            case Bodypart.Root:
                CreateFixedJoint();
                break;

            case Bodypart.Head:
                CreateJoint(Vector3.up, Vector3.forward, -40f, 25f, 25f);
                break;

            case Bodypart.Hat:
                CreateFixedJoint();
                break;

            case Bodypart.Body:
                CreateFixedJoint();
                break;

            case Bodypart.Spine:
                CreateJoint(Vector3.up, Vector3.forward, -20f, 20f, 10f);
                break;

            case Bodypart.Hips:
                CreateFixedJoint();
                break;

            case Bodypart.UpperArm_Right:
                CreateJoint(Vector3.up, Vector3.back, -70f, 10f, 50f);
                break;

            case Bodypart.LowerArm_Right:
                CreateJoint(Vector3.back, Vector3.up, -90f, 0f, 0f);
                break;

            case Bodypart.Right_Hand:
                break;

            case Bodypart.UpperArm_Left:
                CreateJoint(Vector3.down, Vector3.forward, -70f, 10f, 50f);
                break;

            case Bodypart.LowerArm_Left:
                CreateJoint(Vector3.forward, Vector3.down, -90f, 0f, 0f);
                break;

            case Bodypart.Left_Hand:
                break;

            case Bodypart.UpperLeg_Right:
                CreateJoint(Vector3.down, Vector3.back, -20f, 70f, 30f);
                break;

            case Bodypart.LowerLeg_Right:
                CreateJoint(Vector3.back, Vector3.up, -80f, 0f, 0f);
                break;

            case Bodypart.Right_Foot:
                break;

            case Bodypart.UpperLeg_Left:
                CreateJoint(Vector3.down, Vector3.forward, -20f, 70f, 30f);
                break;

            case Bodypart.LowerLeg_Left:
                CreateJoint(Vector3.back, Vector3.down, -80f, 0f, 0f);
                break;

            case Bodypart.Left_Foot:
                break;
        }
    }

    private void CreateInterestPoint()
    {
        interestPoint = gameObject.AddComponent<ZXCorpse>() as ZXCorpse;
        interestPoint.enabled = false;
    }

    private void CreateRigidbody(float weight)
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();

        if (rigidbody == null)
            rigidbody = gameObject.AddComponent<Rigidbody>() as Rigidbody;

        rigidbody.useGravity = false;
        rigidbody.isKinematic = true;
        rigidbody.mass = weight;
        rigidbody.drag = _Drag;
        rigidbody.maxDepenetrationVelocity = 3f;
    }

    private void CreateBoxCollider (Vector3 Center, Vector3 Size)
    {
        BoxCollider boxCollider = gameObject.GetComponent<BoxCollider>();

        if (boxCollider == null)
            boxCollider = gameObject.AddComponent<BoxCollider>();

        collider = boxCollider as Collider;

        boxCollider.center = Center;
        boxCollider.size = Size;
        //boxCollider.isTrigger = true;
        boxCollider.isTrigger = false;
    }

    private void CreateSphereCollider(Vector3 Center, float Radius)
    {
        SphereCollider sphereCollider = gameObject.GetComponent<SphereCollider>();

        if (sphereCollider == null)
            sphereCollider = gameObject.AddComponent<SphereCollider>();

        collider = sphereCollider as Collider;

        sphereCollider.center = Center;
        sphereCollider.radius = Radius;
        //sphereCollider.isTrigger = true;
        sphereCollider.isTrigger = false;
    }

    private void CreateCapsuleCollider (Vector3 Center, float Radius, float Height)
    {
        CapsuleCollider capsuleCollider = gameObject.GetComponent<CapsuleCollider>();

        if (capsuleCollider == null)
            capsuleCollider = gameObject.AddComponent<CapsuleCollider>();

        collider = capsuleCollider as Collider;

        capsuleCollider.direction = 0; /*X-Axis*/
        capsuleCollider.center = Center;
        capsuleCollider.radius = Radius;
        capsuleCollider.height = Height;
        //capsuleCollider.isTrigger = true;
        capsuleCollider.isTrigger = false;
    }

    private void CreateJoint(Vector3 Axis, Vector3 SwingAxis, float LowTwistLimit, float HighTwistLimit, float SwingLimit)
    {
        SoftJointLimit lowTL = new SoftJointLimit();
        SoftJointLimit highTL = new SoftJointLimit();
        SoftJointLimit swing1TL = new SoftJointLimit();
        SoftJointLimit swing2TL = new SoftJointLimit();

        joint = gameObject.GetComponent<CharacterJoint>() as CharacterJoint;

        if (joint == null)
            joint = gameObject.AddComponent<CharacterJoint>() as CharacterJoint;

        joint.connectedBody = transform.parent.GetComponent<Rigidbody>();

        joint.autoConfigureConnectedAnchor = true;
        joint.axis = Axis;
        joint.swingAxis = SwingAxis;

        lowTL.limit = LowTwistLimit;
        highTL.limit = HighTwistLimit;
        swing1TL.limit = SwingLimit;
        swing2TL.limit = 0f;

        joint.lowTwistLimit = lowTL;
        joint.highTwistLimit = highTL;
        joint.swing1Limit = swing1TL;
        joint.swing2Limit = swing2TL;
        joint.enablePreprocessing = false;
        joint.enableProjection = true;
    }

    private void CreateFixedJoint()
    {
        fJoint = gameObject.GetComponent<FixedJoint>() as FixedJoint;

        if (fJoint == null)
            fJoint = gameObject.AddComponent<FixedJoint>() as FixedJoint;

        fJoint.connectedBody = transform.parent.GetComponent<Rigidbody>();
    }
}
