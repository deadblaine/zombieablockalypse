﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;
using DG.Tweening;
using DarkTonic.MasterAudio;

public class ZXZombie : ZXToon
{
    [SoundGroupAttribute] [SerializeField] protected string s_RiseFromGround;
    [SoundGroupAttribute] [SerializeField] protected string s_LeftStep;
    [SoundGroupAttribute] [SerializeField] protected string s_RightStep;

    //Constants
    private const float _MaxRange = 80;
    private const float _LurkRange = 10f;
    private const float _CorpseRange = 15f;
    private float _EatTime; //Randomized

    /* Support Types */
    public enum BehaviorPattern
    {
        Nothing = 0,
        Idle = 1,
        Lurk = 2,
        Eat = 3,
        Corpse = 4,
        AttackNearest = 5,
    }
	public enum ZombieTypes
	{
		Walker = 0,
		Runner = 1,
		Sprinter = 2,
		Armoured = 3,
		Tank = 4,
        Spitter = 5,
        Exploder = 6
	}
    public enum CallbackCode
    {
        //Default
        None = 0,
        MeleeDamage = 102,
        Spit = 101,
        PickUpRock = 103,
        ThrowRock = 104,
        TankGroundSlam = 105,
        LeftStep = 201,
        RightStep = 202,
    }

    //Inspector variables
    [SerializeField] private BehaviorPattern zombieBehavior;

    //Private variables
    protected System.Func<int> d_AttackMove;
    private System.Func<int> d_Lurk;
    private System.Func<int> d_Eat;
    protected float zombieDamageMod = 1f;
    private float eatTimer = 0f;

    //Tracking variables
    protected ZXBarricade tr_NearestBarricade;

    /* Initalization */

    protected override void Awake()
    {
        base.Awake();

        d_AttackMove = new System.Func<int>(AttackMove);
        d_Lurk = new System.Func<int>(Lurk);
        d_Eat = new System.Func<int>(Eat);

        m_Animator.SetInteger(m_WeaponHashID, (int)ZXWeapon.Type.None);

        zombieBehavior = BehaviorPattern.Nothing;

        _EatTime = Random.Range(2f, 4f);
    }

	public override void AdjustValues(int level, int index)
	{
		AdjustValues(level);
	}

	public override void AdjustValues(int Level)
	{
        /*
		if (level == Level)
			return;

		//level
		level = Level;

        //Set damage mod
        zombieDamageMod = Mathf.Pow(1 + ZXGLOBALS.ZombiesDamageModPerLevel, Level);

        //Health
        startingHealth = original_Health * Mathf.Pow(1 + ZXGLOBALS.ZombiesHealhModPerLevel, Level);

		health = startingHealth;

		meleeDamage = Mathf.RoundToInt(original_Damage * zombieDamageMod);

        ChangedLevel(Level);
        */
    }

    /* AI Functions */

    public override void Analyze()
    {
        base.Analyze();

        switch (zombieBehavior)
        {
            case BehaviorPattern.Corpse:
                if (FollowObject == null)
                {
                    FollowObject = ZXReflectingPool.FindClosest<IZXTargetable>(transform, d_StdCheck);

                    if (FollowObject == null)
                        de_CurrentBehaviorTranslation = null;
                    else
                        EnqueueAction((int)Action.FromRagdoll);
                }

                if (posture == Posture.Stand)
                    SetBehavior(BehaviorPattern.AttackNearest);
                break;
        }

        tr_NearestBarricade = ZXReflectingPool.FindClosest<ZXBarricade>(transform, delegate (ZXBarricade bar) 
        {
            float distance = Vector3.Distance(bar.transform.position, transform.position);

            if (distance <= agroRange && tr_ClosestNexus != null)
            {
                Vector3 displacement = bar.transform.position - transform.position;
                Vector3 segment = tr_ClosestNexus.transform.position - transform.position;

                return Vector3.Dot(displacement, segment) > 0;
            }

            return false;
        } );
    }
    
    private int Lurk()
    {
        if (tr_VisibleTargets.Count > 0 || tr_NearestBarricade != null)
        {
            SetBehavior(BehaviorPattern.AttackNearest);
            return (int)defaultAction;
        }

        if (speed == Speed.Stop)
        {
            Vector3 targetPosition;
            Quaternion targetRotation;

            //Create random point nearby
            targetRotation = transform.rotation;
            targetPosition = new Vector3(Random.Range(-_LurkRange, _LurkRange), 0f, Random.Range(_LurkRange / 2, _LurkRange));

            if (tr_ClosestNexus != null && Vector3.Distance(Base, tr_ClosestNexus.Base) > tr_ClosestNexus.InteractionRadius)
            {
                Vector3 targetDirection = (tr_ClosestNexus.transform.position - transform.position).normalized;

                if (targetDirection != Vector3.zero)
                    targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
                else
                    targetRotation = Quaternion.identity;

                targetRotation = Quaternion.Inverse(transform.rotation) * targetRotation;
                targetPosition = transform.TransformPoint(targetRotation * targetPosition);
            }
            else
            {
                targetPosition = transform.TransformPoint(targetPosition);
            }

            Move(defaultMoveSpeed, targetPosition);
            return (int)defaultMoveAction;
        }

        return (int)defaultAction;
    }

    private int Eat()
    {
        IZXTargetable nearestHuman = ZXReflectingPool.FindClosest<IZXTargetable>(transform, d_StdCheck);

        float distance;
        UnityEngine.AI.NavMeshHit navWaypoint;

        if (nearestHuman != null && eatTimer <= 0f)
        {
            SetBehavior(BehaviorPattern.AttackNearest);
            FollowObject = nearestHuman;
            de_CurrentBehaviorTranslation = d_AttackMove;
            return (int)Action.Idle;
        }

        ZXInterestPoint nearestCorpse = ZXReflectingPool.FindClosest<ZXInterestPoint>(transform, delegate (ZXInterestPoint Object)
        {
            return Object.PointType == ZXInterestPoint.Interest.Corpse;
        });

        if (nearestCorpse != null && currentAction != (int)Action.IdleZombieEat)
        {
            distance = Vector3.Distance(transform.position, nearestCorpse.transform.position);

            if (distance < nearestCorpse.InteractionRadius)
            {
                defaultAction = Action.IdleZombieEat;
                return (int)Action.IdleZombieEat;
            }
            else if (distance < _CorpseRange)
            {
                if (nearestCorpse.GetNearestPosition(out navWaypoint))
                {
                    Move(defaultMoveSpeed, navWaypoint.position);
                }

                defaultAction = Action.Idle;
            }
            else
                nearestCorpse = null;
        }

        if (nearestCorpse == null)
        {
            SetBehavior(BehaviorPattern.Lurk);
            defaultAction = Action.Idle;
        }

        return (int)defaultAction;
    }

    protected virtual int AttackMove()
    {
        IZXTargetable bestTarget = null;

        //Check visible targets list, default to a barricade
        if (tr_VisibleTargets.Count > 0)
            bestTarget = tr_VisibleTargets.Values[0];
        else if (tr_NearestBarricade != null)
            bestTarget = tr_NearestBarricade.GetTarget;

        //If no target is found
        if (bestTarget == null)
        {
            SetBehavior(BehaviorPattern.Lurk);
            return (int)defaultAction;
        }
        //Only replace FollowObject if different
        else if (bestTarget != FollowObject)
            FollowObject = bestTarget;

        //Check meele distance
        if (FollowObject.GetDistance(transform.position) > attackRange)
        {
            //If stopped, begin moving toward target
            if (speed == Speed.Stop)
                Move(defaultMoveSpeed, FollowObject.GetNearestPoint(transform.position) /*, true*/);

            return (int)defaultMoveAction;
        }
        //If within range perform meele attack
        else
        {
            return (int)Action.MeleeGeneric;
        }
    }

    public override void Place(Vector3 location, Quaternion rotation, object args)
    {
        if (args is Action && (Action)args == Action.ZombieRise)
        {
            //Activate game object
			gameObject.SetActive(true);
			transform.position = location + new Vector3(0f, -3.2f, 0f);
            transform.rotation = rotation;

            newRotation = rotation;

            PhysicsOverride(true);
			ChangePosture(Posture.Stand);
			AnimatorOverride(AnimState.Override, (int)Action.ZombieRise);
            SetBehavior(BehaviorPattern.Nothing);
            FindNavMeshTeather();
            actionWaitFlag = true;

            MasterAudio.PlaySound3DAtTransformAndForget(s_RiseFromGround, transform);

            invulnerable = true;
            playDamageAnimations = false;

            transform.DOMove(teather.Value, 3f, false).SetEase(Ease.OutBack).OnComplete(delegate ()
            {

                //if (ZXGroundEvents.instance.activeCube.bounds.Contains(location))
                //{
                    PhysicsOverride(false);
                    SetBehavior(BehaviorPattern.AttackNearest);
                    actionWaitFlag = false;

                    Register(true, true);
                //}

                if (ZXSpawnManager.PlaySpawnSounds)
                    MasterAudio.PlaySound3DAtTransformAndForget(s_SpawnSound, transform);

                invulnerable = false;
                playDamageAnimations = true;
            });

            return;
        }

        base.Place(location, rotation, args);

        if (args is BehaviorPattern)
            SetBehavior((BehaviorPattern)args);
        else
            SetBehavior(BehaviorPattern.Lurk);
    }

    public void SetBehavior(BehaviorPattern Behavior)
    {
        if (zombieBehavior == Behavior)
            return;

        switch (zombieBehavior)
        {
            case BehaviorPattern.Eat:
                defaultAction = Action.Idle;
                break;
        }

        ResetZXAI();
        zombieBehavior = Behavior;

        switch (zombieBehavior)
        {
            case BehaviorPattern.Nothing:
                de_CurrentBehaviorTranslation = d_Idle;

                break;

            case BehaviorPattern.Idle:
                de_CurrentBehaviorTranslation = d_Idle;
                break;

            case BehaviorPattern.Corpse:
                de_CurrentBehaviorTranslation = null;
                ChangePosture(Posture.Ragdoll);
                transform.position = transform.position + new Vector3(0f, 5f, 0f);
                break;

            case BehaviorPattern.Lurk:
                de_CurrentBehaviorTranslation = d_Lurk;
                break;

            case BehaviorPattern.Eat:
                de_CurrentBehaviorTranslation = d_Eat;
                eatTimer = _EatTime;
                break;

            case BehaviorPattern.AttackNearest:
                de_CurrentBehaviorTranslation = d_AttackMove;
                break;
        }

        if (logDebugging)
            Debug.Log("Behavior pattern changed to: " + Behavior);
    }

    /* Runtime Updates */
    protected override void Update()
    {
        base.Update();

        if (eatTimer > 0f)
            eatTimer -= Time.deltaTime;
    }

    /* Action processing */

    protected override void PerformAction(int ActionCode)
    {
        switch (ActionCode)
        {
            case (int)Action.Idle:
                break;

            case (int)Action.IdleZombieEat:
                SetIKState(IKState.Look, true);
                ChangeSpeed(Speed.Stop);
                ChangePosture(Posture.Kneel);
                break;

            case (int)Action.MeleeGeneric:
                attackTarget = FollowObject;
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;
                MasterAudio.PlaySound3DAtTransform(s_meleeAttackSound, transform);
                break;

            case (int)Action.TakeDamage:
                SetIKState(IKState.All, false);
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;
                break;

            case (int)Action.TakeBigDamage:
                SetIKState(IKState.All, false);
                ChangeSpeed(Speed.Stop);
                actionWaitFlag = true;
                break;

            case (int)Action.WallPound:
                break;

            default:
                base.PerformAction(ActionCode);
                break;
        }        
    }

    /* Actions */

    public override void TakeDamage(ZXDamage Damage)
    {
        base.TakeDamage(Damage);

        //Cases that must happen regardless of death
        switch (Damage.Effect)
        {
            case ZXDamage.SpecialEffect.ForceFire:
                Burn();
                break;
        }

        //End function if dead
        if (health <= 0 || invulnerable)
            return;
    }

    protected override void TakeDamageEffect(ZXDamage Damage)
    {
        float random = Random.Range(0f, 1f);

        if (!playDamageAnimations)
            return;

        switch (Damage.Effect)
        {
            case ZXDamage.SpecialEffect.Knockback:

                if (random > 0.5f)
                    EnqueueAction((int)Action.TakeBigDamage);
                else
                    EnqueueAction((int)Action.TakeDamage);

                break;

            case ZXDamage.SpecialEffect.ForceKnockback:

                if (random > 0.8f && canRagDoll && health > 0f)
                {
                    Ragdoll(Damage);
                }
                else if (random > 0.4f)
                    EnqueueAction((int)Action.TakeBigDamage);
                else
                    EnqueueAction((int)Action.TakeDamage);

                break;

            case ZXDamage.SpecialEffect.ForceRagdoll:

                Ragdoll(Damage);

                break;
        }
    }

    /* Destruction */

    protected override void StateChanged(ZXCombatUI.State State)
    {
        if (State == ZXCombatUI.State.Victory)
            Die();
    }

    protected override void Die()
    {
        base.Die();

        SetBehavior(BehaviorPattern.Nothing);

        ZXMissionClock.ReportKilled(this);
    }

    public override void AnimationCallback(int Code)
    {
        if (logDebugging)
            Debug.Log("AnimCallback:" + (CallbackCode)Code);

        switch (Code)
        {
            case (int)CallbackCode.MeleeDamage:

                if (attackTarget != null)
                {
                    Ray ray = new Ray(transform.position, (FollowObject.LookAtTransform.position - LookAtTransform.position).normalized);
                    ZXDamage damage = new ZXDamage(meleeDamage, 0f, 0.2f, 0f, Race, ZXDamage.Type.Impact, ray, ZXDamage.SpecialEffect.None);
                    attackTarget.TakeDamage(damage);
                }
                break;

            case (int)CallbackCode.LeftStep:
                MasterAudio.PlaySound3DAtTransformAndForget(s_LeftStep, transform);
                break;

            case (int)CallbackCode.RightStep:
                MasterAudio.PlaySound3DAtTransformAndForget(s_RightStep, transform);
                break;

            default:
                throw new System.NotImplementedException();
        }
    }

    /* Communication */

    public override void RecieveMessage(ZXInterestPoint Sender, Vector3 Location, ZXInterestPoint.Interest Header, ZXInterestPoint.BroadcastMessage Message)
    {
        if (Message == ZXInterestPoint.BroadcastMessage.NewCorpse)
            SetBehavior(BehaviorPattern.Eat);
    }
}
