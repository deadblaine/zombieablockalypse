﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ZXCore;

public /* Static */ class ZXReflectingPool : MonoBehaviour
{
    /* Constants */
    private const int ReflectingPoolBaseSize = 20;

    /* Types */
    private class ZXReflection
    {
        public System.Type Type;
        public object Self;
        public Transform Transform;
        public int UID;

        public ZXReflection(System.Type Type, object Self, Transform Transform, int UID)
        {
            this.Type = Type;
            this.Self = Self;
            this.Transform = Transform;
            this.UID = UID;
        }
    }

    /* Reflecting Pool */ 
    private static List<List<ZXReflection>> ReflectingPool;
    private static System.Comparison<RaycastHit> DistanceComparison = new System.Comparison<RaycastHit>(DistanceComparer);
    private static System.Predicate<RaycastHit> VisibilityChecker = new System.Predicate<RaycastHit>(VisibilityCheck);

    /* Initalization */
    private void Awake()
    {
        ResetList();
    }

    /* Reflecting Pool Functions */
    #region
    /// <summary>
    /// Registers an object with the reflecting pool
    /// </summary>
    /// <typeparam name="T">Type of the object</typeparam>
    /// <param name="Object">The object to be registered</param>
    public static void Register<T>(T Obj, Transform ObjTrans) where T : class
    {
        //Get the reflection of the object
        ZXReflection Reflection = new ZXReflection(typeof(T), Obj, ObjTrans, ObjTrans.GetInstanceID());

        if (ReflectingPool == null)
        {
            ResetList();
        }


        //Search for target list
        List<ZXReflection> ReflectingList = FindList(Reflection.Type);

        if (ReflectingList == null)
        {
            ReflectingList = new List<ZXReflection>(ReflectingPoolBaseSize);
            ReflectingPool.Add(ReflectingList);
        }

        ReflectingList.Add(Reflection);
    }

    /// <summary>
    /// De-registers an object with the reflecting pool
    /// </summary>
    /// <typeparam name="T">Type of the object</typeparam>
    /// <param name="Object">The object to be de-registered</param>
    public static void DeRegister<T>(Transform ObjTrans) where T : class
    {
        System.Type Type = typeof(T);

        //Search for target list
        List<ZXReflection> ReflectingList = FindList(Type);

        if (ReflectingList == null)
            return;

        //if (ReflectingList == null)
            //throw new System.Exception(ObjTrans.gameObject.name + ": List of <" + Type + ">" + "not found");
        
        for (int i = 0; i < ReflectingList.Count; i++)
        {
            if (ReflectingList[i].UID == ObjTrans.GetInstanceID())
            {
                ReflectingList.RemoveAt(i);
                return;
            }
        }

        //throw new System.Exception(ObjTrans.gameObject.name + " not found in pool.");
    }

    /// <summary>
    /// Attempts to find the object that is closest to the origin, excluding any instance attached to the Origin transform
    /// </summary>
    /// <typeparam name="T">Type of the object you are looking for</typeparam>
    /// <param name="Origin">Origin of the search</param>
    /// <returns>Returns null if nothing found</returns>
    public static T FindClosest<T>(Transform Origin) where T : class
    {
        return FindClosest<T>(Origin, null);
    }

    /// <summary>
    /// Attempts to find the object that is closest to the origin transtorm, excluding any instance attached to the Origin transform
    /// </summary>
    /// <typeparam name="T">Type of the object you are looking for</typeparam>
    /// <param name="Origin">Origin of the search</param>
    /// <param name="Condition">Additional condition(s) for serach</param>
    /// <returns>Returns null if nothing found</returns>
    public static T FindClosest<T>(Transform Origin, System.Predicate<T> Condition) where T : class
    {
        //Variables
        T returnT = null;

        //Search for target list
        List<ZXReflection> ReflectingList = FindList(typeof(T));

        //If the list equals null, return null
        if (ReflectingList == null)
            return null;

        //Search list for closest object
        else
        {
            float clsDist = float.MaxValue;
            float curDist;

            for (int i = 0; i < ReflectingList.Count; i++)
            {
                if (Condition == null || Condition(ReflectingList[i].Self as T))
                {
                    curDist = Vector3.Distance(Origin.position, ReflectingList[i].Transform.position);

                    if (curDist < clsDist && ReflectingList[i].UID != Origin.GetInstanceID())
                    {
                        clsDist = curDist;
                        returnT = ReflectingList[i].Self as T;
                    }
                }
            }
            return returnT;
        }
    }

	public static List<T> FindInSphere<T>(Vector3 Origin, float Radius) where T : class
	{
		return FindInSphere<T>(Origin, Radius, null);
	}
		
    public static List<T> FindInSphere<T>(Vector3 Origin, float Radius, System.Predicate<T> Condition) where T : class
    {
        //Search for target list
        List<ZXReflection> ReflectingList = FindList(typeof(T));
        List<T> returnList = new List<T>(15);

        //If the list equals null, return null
        if (ReflectingList == null)
            return null;

        //Search list for objects within the radius
        else
        {
            float curDist;

            for (int i = 0; i < ReflectingList.Count; i++)
            {
                if (Condition == null || Condition(ReflectingList[i].Self as T))
                {
                    curDist = Vector3.Distance(Origin, ReflectingList[i].Transform.position);

                    if (curDist < Radius)
                        returnList.Add(ReflectingList[i].Self as T);
                }
            }
            if (returnList.Count > 0)
                return returnList;
            else
                return null;
        }
    }

    public static List<T> SphereCast<T>(Ray CastRay, float Radius, float MaxDistance, System.Predicate<T> Condition) where T : class
    {
        //Search for target list
        List<ZXReflection> ReflectingList = FindList(typeof(T));
        List<T> returnList = new List<T>(15);

        //If the list equals null, return null
        if (ReflectingList == null)
            return null;

        //Search list for objects within the radius
        else
        {
            float curDist;
            Vector3 curPoint;
            Vector3 targetPoint;

            for (int i = 0; i < ReflectingList.Count; i++)
            {
                if (Condition == null || Condition(ReflectingList[i].Self as T))
                {
                    curDist = Vector3.Distance(CastRay.origin, ReflectingList[i].Transform.position);

                    if (curDist < MaxDistance)
                    {
                        curPoint = CastRay.GetPoint(curDist);

                        //Adjust
                        curPoint.y = 0;
                        targetPoint = ReflectingList[i].Transform.position;
                        targetPoint.y = 0;

                        if (Vector3.Distance(curPoint, targetPoint) < Radius)
                            returnList.Add(ReflectingList[i].Self as T);
                    }
                }
            }
            if (returnList.Count > 0)
                return returnList;
            else
                return null;
        }
    }

    public static List<T> ConeCast<T>(Ray CastRay, Vector2 Vertex, float MaxDistance, System.Predicate<T> Condition) where T : class
    {
        //Search for target list
        List<ZXReflection> ReflectingList = FindList(typeof(T));
        List<T> returnList = new List<T>(15);

        //If the list equals null, return null
        if (ReflectingList == null)
            return null;

        //Search list for objects within the radius
        else
        {
			Vector3 direction;

            for (int i = 0; i < ReflectingList.Count; i++)
            {
                if (Condition == null || Condition(ReflectingList[i].Self as T))
                {
                    direction = ReflectingList[i].Transform.position - CastRay.origin;

                    if (Vector3.Dot(CastRay.direction, direction.normalized) > Mathf.Cos(Mathf.Deg2Rad * Vertex.x))
                    {
                        if (direction.magnitude < Vertex.y)
                        {
                            returnList.Add(ReflectingList[i].Self as T);
                        }
                    }
                }
            }
            if (returnList.Count > 0)
                return returnList;
            else
                return null;
        }
    }

    /// <summary>
    /// Expensive
    /// </summary>
    /// <param name="Origin"></param>
    /// <param name="Target"></param>
    /// <param name="Hit"></param>
    /// <returns></returns>
    public static bool VisibilityCast(Transform Origin, Transform Target, out RaycastHit Hit)
    {
        float distance = Vector3.Distance(Origin.position, Target.position);
        Ray sightLine = new Ray(Origin.position, (Target.position - Origin.position).normalized);

        return VisibilityCast(sightLine, distance, out Hit);
    }
    /// <summary>
    /// Expensive
    /// </summary>
    /// <param name="SightLine"></param>
    /// <param name="Distance"></param>
    /// <param name="Hit"></param>
    /// <returns></returns>
    public static bool VisibilityCast(Ray SightLine, float Distance, out RaycastHit Hit)
    {
        List<RaycastHit> SortedHits = new List<RaycastHit>(Physics.RaycastAll(SightLine, Distance));

        if (SortedHits == null || SortedHits.Count == 0)
        {
            Hit = new RaycastHit();
            return false;
        }

        //Sort the list
        SortedHits.Sort(DistanceComparer);

        //Find the first visibility blocking instance
        Hit = SortedHits.Find(VisibilityCheck);

        if (Hit.transform != null)
            return true;
        else
            return false;
    }

    //Find the list of type <T>
    private static List<ZXReflection> FindList(System.Type Type)
    {
        if (ReflectingPool == null || ReflectingPool.Count == 0)
            return null; 

        for (int i = 0; i < ReflectingPool.Count; i++)
        {
            if (ReflectingPool[i].Count > 0 && ReflectingPool[i][0].Type == Type)
                return ReflectingPool[i];
        }

        return null;
    }

    private static void ResetList()
    {
        ReflectingPool = new List<List<ZXReflection>>(15);
    }

    //Search & Sort Delegates
    private static int DistanceComparer(RaycastHit A, RaycastHit B)
    {
        int value = A.distance < B.distance ? -1 : 1;

        if (A.distance == B.distance)
            value = 0;
      
        return value;
    }
    private static bool VisibilityCheck(RaycastHit Hit)
    {
        ZXFXable fxObject = Hit.transform.GetComponent<ZXFXable>();

        if (fxObject == null || !fxObject.BlocksVision)
            return false;
        else
            return true;
    }
    #endregion
}
